<?php

require('validation.php');

if (isset($_POST['submit'])) {
    $validate = new validate($_POST);
    $errors = $validate->validateForm();
}

?>
<html>
<head>
<title>flower shop online</title>
<link rel = "stylesheet" type = "text/css" href = "form.css"/>
</head>
<body>
<div class ="border">

<div>
<b>&nbsp &nbsp &nbsp &nbsp FLOWER SHOP ONLINE</b><br><br>
</div>

<div class = "blank" style="background-color: grey">
</div>

<form action = "payment.php" method = "POST">
<fieldset>

<div>
  <b>SECURE PAYMENT PAGE</b>
  <br><br>
  <label>Select Language</label>
  <select id = "value">
    <option value = "English">English</option>
    <option value = "Tamil">Tamil</option>
    <option value = "Hindi">Hindi</option>
  </select>
  <br><br>
  <label>Payement Method</label>
  <input type = "text" value = "visa" disabled>
  <br><br>
  <label>Description</label>
  <input type = "text" value = "Items Ordered" disabled>
  <br><br>
  <label>Amount</label>
  <input type = "text" value = "$ 100" disabled>
  <br><br>
</div>

<div class = "dark">
  <label><h3>Card Details</h3></label>
</div>

<div class = "light">
<p>You must fill in the fields marked with *</p>
<label>*Card Number</label>
<input type = "text" name = "cardNumber" pattern ="[0-9]{16}" value = "<?php echo htmlspecialchars($_POST['cardNumber']?? null)?>"><br><br>
<div class = "error">
    <?php echo $errors['cardNumber'] ?? '' ?>
</div>
<label><u>*Security Code</u></label>
<input type = "password" name = "securityCode" pattern = "[0-9]{3}" value = "<?php echo htmlspecialchars($_POST['securityCode'] ?? null)?>"><br><br>
<div class = "error">
    <?php echo $errors['securityCode'] ?? " " ?>
</div>
<label>*Expiry Date</label>
<input type = "number" name = "expiryYear" max = "2060" min = "2020" value = "<?php echo htmlspecialchars($_POST['expiryYear'])?>"> &nbsp &nbsp
<input type = "number" name = "expiryDate" max = "31" min = "1" value = "<?php echo htmlspecialchars($_POST['expiryDate'])?>"><br><br>
<div class = "error">
    <?php echo $errors['expiryDate'] ?? " " ?>
</div>
<label>*CardHolder's Name</label>
<input type = "text" name = "cardHolder" value = "<?php echo htmlspecialchars($_POST['cardHolder'] ?? null)?>"><br><br>
<div class = "error">
    <?php echo $errors['cardHolder'] ?? " " ?>
</div>
</div>

<div class = "dark">
<label><h3>CardHolder Details</h3></label>
</div>

<div class = "light">
<p>You must fill in the fields marked with *</p>
<label>*Address 1</label>
<input type = "text" name = "address1" value = "<?php echo htmlspecialchars($_POST['address1'] ?? null)?>"><br><br>
<div class = "error">
    <?php echo $errors['address1'] ?? " " ?>
</div>
<label>Address 2</label>
<input type = "text" name = "address2" value = "<?php echo htmlspecialchars($_POST['address2'] ?? null)?>"><br><br>
<label>Address 3</label>
<input type = "text" name = "address3" value = "<?php echo htmlspecialchars($_POST['address3'] ?? null)?>"><br><br>
<label>*Town/City</label>
<input type = "text" name = "town" value = "<?php echo htmlspecialchars($_POST['town'] ?? null)?>"><br><br>
<div class = "error">
    <?php echo $errors['town'] ?? " " ?>
</div>
<label>Region</label>
<input type = "text" name = "region" value = "<?php echo htmlspecialchars($_POST['region'] ?? null)?>"><br><br>
<div class = "error">
    <?php echo $errors['region'] ?? " " ?>
</div>
<label>*Postcode/Zipcode</label>
<input type = "text" name = "zipcode" value = "<?php echo htmlspecialchars($_POST['zipcode'] ?? null)?>"><br><br>
<div class = "error">
    <?php echo $errors['zipcode'] ?? " " ?>
</div>
<label>*country</label>
<select name = "country">
  <option value = "" selected></option>
  <option value = "UK">United Kindom</option>
  <option value = "US">United States</option>
  <option value = "India">India</option>
</select><br><br>
<div class = "error">
    <?php echo $errors['country'] ?? " " ?>
</div>
<label>Telephone</label>
<input type = "text" name = "telephone" value = "<?php echo htmlspecialchars($_POST['telephone'] ?? null)?>"><br><br>
<div class = "error">
    <?php echo $errors['telephone'] ?? " " ?>
</div>
<label>Fax</label>
<input type = "text" name = "fax" value = "<?php echo htmlspecialchars($_POST['fax'] ?? null)?>"><br><br>
<div class = "error">
    <?php echo $errors['fax'] ?? " " ?>
</div>
<label>*Email Address</label>
<input type = "text" name = "email" value = "<?php echo htmlspecialchars($_POST['email'] ?? null)?>"><br><br>
<div class = "error">
    <?php echo $errors['email'] ?? " " ?>
</div>
</div>

<div>
<input type = "submit"/> <b>Start Again</b><br><br>
<input type = "submit"/> <b>Cancel</b>
<div style="display:inline"><b>Make Payement </b>
<input type = "submit"  name = "submit"/></div>
</div>

</form>

<hr>
<div id = "textalign">
<h4 style = "text-align:left">Refunds and Returns</h4>
<h5 style = "text-align:left">For more information visit our <a href="www.google.com">Refund and refer</a> </h5>
<p><image src = "sample.jpeg" hspace = "50">For help with your payment visit the <a href = "worldpay.com"> WorldPay Help</a></p>
</div>
</fieldset>
<hr>

<div class = "blank"><h1>Thank you for shopping at flowershop.Have a nice day</h1></div>
</div>
</body>
</html>
