<?php
session_start();
echo  "You are logged in as ",$_SESSION['username'], "<br>";
class validate
{
    public $data;
    private $error = [];
    const FIELDS = ['cardNumber', 'securityCode', 'expiryDate', 'cardHolder', 'address1', 'address2', 'address3', 'town', 'region', 'country', 'telephone', 'fax', 'email'];

    public function __construct($postData)
    {
        $this->data = $postData;
    }

    public function validateForm()
    {
        $this->validateCardNumber();
        $this->validateSecurityCode();
        $this->validateCardHolder();
        $this->validateExpiryDate();
        $this->validateAddress1();
        $this->validateAddress2();
        $this->validateAddress3();
        $this->validateTown();
        $this->validateRegion();
        $this->validateZipcode();
        $this->validateCountry();
        $this->validateTelephone();
        $this->validateFax();
        $this->validateEmail();

        if (empty($this->error)) {
            $this->display();
        } else {
            return $this->error;
        }
    }

    public function validateCardNumber()
    {
         $cardNumber = trim($this->data['cardNumber']);
         if (empty($cardNumber)) {
             $this->addError('cardNumber', '*card number cannot be empty');
         } else {
             if (!preg_match('/^[0-9]{16}$/', $cardNumber)) {
                 $this->addError('cardNumber', '*card number must be numeric and should contain 16 digits');
             }
         }
    }

    public function validateSecurityCode()
    {
         $securityCode = trim($this->data['securityCode']);

         if (empty($securityCode)) {
             $this->addError('securityCode', '*security Code cannot be empty');
         } else {
             if (!preg_match('/^[0-9]{3}$/', $securityCode)) {
                  $this->addError('securityCode', '*Security code must be numeric and should contain only 3 digits');
             }
         }
    }

    public function validateExpirydate()
    {
         $expiryYear = trim($this->data['expiryYear']);
         $expiryDate = trim($this->data['expiryDate']);
         $currentDate = date();
         if (empty($expiryDate) || empty($expiryYear)) {
             $this->addError('expiryDate', '*expiryDate or Expiry Year cannot be empty');
         }
    }

    public function validateCardHolder()
    {
         $cardHolder = trim($this->data['cardHolder']);

         if (empty($cardHolder)) {
             $this->addError('cardHolder', '*card Holder cannot be empty');
         } else {
             if (!preg_match('/[A-z]/', $cardHolder)) {
                  $this->addError('Card Holder', '*Card Holder must be alphabets');
             }
         }
    }

    public function validateAddress1()
    {
        $address1 = trim($this->data['address1']);

        if (empty($address1)) {
            $this->addError('address1', '*address1 cannot be empty');
        } else {
            if (!preg_match('/[alnum]/i', $address1)) {
                $this->addError('address1', 'address1 must be alphanumeric');
            }
        }
    }

    public function validateAddress2()
    {
        $address2 = trim($this->data['address2']);

    }

    public function validateAddress3()
    {
        $address3 = trim($this->data['address3']);
    }

    public function validateTown()
    {
        $town = trim($this->data['town']);

        if (empty($town)) {
            $this->addError('town', '*Town cannot be empty');
        } else {
             if (!preg_match('/[A-z]/', $town)) {
                 $this->addError('town', '*Town must be alphabets');
             }
        }
    }

    public function validateRegion()
    {
        $region = trim($this->data['region']);

        if (empty($region)) {
            $this->addError('region', '*Region cannot be empty');
        } else {
             if (!preg_match('/[A-z]/', $region)) {
                 $this->addError('region', '*Region must be alphabets');
             }
        }
    }

    public function validateZipcode()
    {
         $zipcode = trim($this->data['zipcode']);
         if (empty($zipcode)) {
             $this->addError('zipcode', '*zipcode cannot be empty');
         } else {
             if (!preg_match('/[0-9]{6}/', $zipcode)) {
                 $this->addError('zipcode', '*zipcode must be numeric and should contain 6 digits');
             }
         }
    }

    public function validateCountry()
    {
         $country = trim($this->data['country']);
         if (empty($country)) {
             $this->addError('country', '*country must be selected');
         }
    }

    public function validateTelephone()
    {
        $telephone = trim($this->data['telephone']);
        if (empty($telephone)) {
            $this->addError('telephone', '*Telephone cannot be empty');
        } else {
            if (!preg_match('/[9,8,7,6][0-9]{9}/', $telephone)) {
                $this->addError('telephone', '*Telephone should contain 10 digits and must be valid phone number');
             }
         }
    }

    public function validateFax()
    {
         $fax = trim($this->data['fax']);
         if (empty($fax)) {
             $this->addError('fax', '*fax cannot be empty');
         } else {
             if (!preg_match('/[1][0-9]{9}/', $fax)) {
                 $this->addError('fax', '*fax should contain 10 digits and must be valid fax number');
             }
         }
    }

    public function validateEmail()
    {
        $email = trim($this->data['email']);
        if (empty($email)) {
            $this->addError('email', '*email cannot be empty');
        } else {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $this->addError('email', '*Enter a vaild email');
            }
        }
    }

    private function addError($key, $value)
    {
        $this->error[$key] = $value;
    }

    public function display()
    {
        foreach ($this->data as $key=>$value) {
            print($key.": ".$value);
            echo "<br>";
        }
    }


}
