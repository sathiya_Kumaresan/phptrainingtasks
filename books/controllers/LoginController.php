<?php
include_once 'controllers/Controller.php';

class LoginController extends Controller
{
    public function index()
    {
        include_once 'views/LoginView.php';
        $this->model = new LoginModel();
        $this->model->__invoke();
    }
}
 ?>
