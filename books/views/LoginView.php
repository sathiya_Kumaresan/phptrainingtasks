<html>
 <head>
   <title>LOGIN PAGE</title>
   <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <link rel = "stylesheet" type = "text/css" href = "stylesheet.css">
 </head>
 <body>
   <div class = 'navigation'>
     <a href = "signup_view.php">SIGNUP</a>
     <a href = "login_view.php">LOGIN</a>
     <a href = "about_view.php">ABOUT</a>
   </div>
   <span> <?php echo $this->error['value']; ?></span>
   <h1> Login to View Your Profile </h1>
    <form action = "models/login_model.php" method = "POST">
      <fieldset>
        <div>
           <div>
           <label for = "email">EMAIL</label>
           <input type = "text" name = "email" placeholder = "Your mail">
           </div>
           <div>
           <label for = "password">PASSWORD  </label>
           <input type = "password" name = "password" placeholder = "Your password">
           </div>
           <div>
           <input type = "submit" name = "submit">
           </div>
        </div>
      </fieldset>
    </form>
 </body>
</html>
