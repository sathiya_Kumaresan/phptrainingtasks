<?php
session_start();
if (isset($_SESSION['email'])) {
    echo "You are logged in as ",$_SESSION['email'];
}

$time = $_SERVER['REQUEST_TIME'];
$timeout = 60;

if (isset($_SESSION['LAST_ACTIVITY']) && ($time - $_SESSION['LAST_ACTIVITY']) > $timeout ) {
    session_unset();
    session_destroy();
    header_location('/views/login.php');
}
