<?php
require_once 'controllers/Controller.php';
require_once 'connection.php';

$url = $_GET['url'];
$url = explode('/', $url);
$end = count($url)-1;
$url = $url[$end];

if (empty($url)) {
  $url = 'login';
}
if (strpos($url, '.php')) {
    $url = str_replace(".php","",$url);
}

$controller = new Controller();
$controller->$url();
