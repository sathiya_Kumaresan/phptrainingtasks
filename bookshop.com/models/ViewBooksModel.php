<?php
include 'Pagination.php';
class ViewBooksModel extends Pagination
{
    public $connect;

    public function __construct($connect)
    {
        $this->connect = $connect;
    }

    public function getTotalRecord()
    {
        $result = mysqli_query($this->connect, "select * from books ");
        $total_record = mysqli_num_rows($result);

        return $total_record;
    }

    public function getResult($starting_record)
    {
        $result = mysqli_query($this->connect, "select * from books limit $starting_record, 5");

        return $result;
    }
}
