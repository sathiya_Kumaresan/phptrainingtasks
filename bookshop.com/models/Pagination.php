<?php
CONST RESULT_PER_PAGE = 5;

class pagination
{
    public function getNumberOfPages($total_record)
    {
        $number_of_page = ceil($total_record/RESULT_PER_PAGE);

        return $number_of_page;
    }

    public function getStartingRecord($page)
    {
        $starting_record = ($page - 1) * RESULT_PER_PAGE;

        return $starting_record;
    }

    public function getPage()
    {
        if (!isset($_GET['page'])) {
            $page = 1;
        } else {
            $page = $_GET['page'];
        }

        return $page;
    }

    static public function pageNumber($number_of_page, $url)
    {
         $getPage = $_GET['page'];
         $next = $getPage + 1;
         $previous = $getPage - 1;
         if ($getPage > 1) {
             echo "<a href = '../../$url/page/$previous'> << </a>";
         }


         for ($page = 1; $page <= $number_of_page; $page++) {
             echo "<a href = '../../$url/page/$page' >$page<a>";
         }

         if ($getPage < $number_of_page)
         echo "<a href = '../../$url/page/$next'> >> </a>";
    }
}
