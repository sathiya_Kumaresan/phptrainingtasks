<?php

include 'SelectInterface.php';
include '../models/SelectModel.php';

class SelectAdaptor implements SelectInterface
{
    public $instance;

    public function __Construct()
    {
        $this->instance = new SelectModel();
    }

    public function getStudentDetails($connect)
    {
         return $this->instance->getStudentDetails($connect);
    }
}
