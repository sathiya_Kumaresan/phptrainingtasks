<?php

class MyProfileModel
{
    public function __construct($connect)
    {
        $this->connect = $connect;
    }

    public function getId()
    {
        $email = $_SESSION['email'];
        $sql = "select * from student_details where email = '$email'";
        $result = mysqli_query($this->connect, $sql);
        $data = mysqli_fetch_assoc($result);
        $id = $data['id'];

        return $id;
    }

    public function getStudentDetails($id)
    {
        $sql = "select * from student_details where id = $id";
        $result = mysqli_query($this->connect, $sql);
        $student = mysqli_fetch_assoc($result);

        return $student;
    }

    public function getHobbies($id)
    {
        $sql = "select * from hobbies where id = $id";
        $result = mysqli_query($this->connect, $sql);
        while ($data = mysqli_fetch_assoc($result)) {
            $value .= $data[hobbies].", ";
        }
        $value = rtrim($value, ', ');

        return $value;
    }

    public function getLocation($id)
    {
         $sql = "select * from images where id = $id";
         $result = mysqli_query($this->connect, $sql);
         $location = mysqli_fetch_assoc($result);

         return $location['image_name'];
    }

}
