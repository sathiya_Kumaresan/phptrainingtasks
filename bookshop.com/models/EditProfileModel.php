<?php

class EditProfileModel
{
 public $data;
 public $error;
 public $firstName;
 public $lastName;
 public $birthDate;
 public $age;
 public $phone;
 public $gender;
 public $email;
 public $city;

 public function __construct($data) {
     $this->data = $data;
     $this->firstName = trim($this->data['firstName']);
     $this->lastName = trim($this->data['lastName']);
     $this->birthDate = $this->data['birthDate'];
     $this->age = trim($this->data['age']);
     $this->phone = trim($this->data['phone']);
     $this->gender = $this->data['gender'];
     $this->email = $this->data['email'];
     $this->city = $this->data['city'];
 }

 public function validateForm() {

     if (empty($this->firstName)){
         $this->error['firstName'] = "First name cannot be empty";
     } else {
         if (!preg_match('/^[a-z]*$/i', $this->firstName)) {
         $this->error['firstName'] = "First name cannot contain numerics or special characters";
         }
     }

     if (empty($this->lastName)) {
         $this->error['lastName'] = "Last name cannot be empty";
     } else {
         if (!preg_match('/^[a-z]*$/i', $this->lastName)) {
             $this->error['lastName'] = "Last name cannot contain numerics or special characters";
         }
     }

     if (empty($this->phone)) {
         $this->error['phone'] = "Phone cannot be empty";
     } else {
         if (!preg_match('/^[9,8,7,6][0-9]{9}$/', $this->phone)) {
             $this->error['phone'] = "Invalid phone number";
         }
     }

     if (empty($this->birthDate)) {
         $this->error['birthDate'] = "Birthdate should be selected";
     }

     if (empty($this->age)) {
         $this->error['age'] = "Age cannot be empty";
     } else if ($this->age < 0) {
         $this->error['age'] = "Age cannot be negative and cannot be greater than 25";
     }


     if (empty($this->data['gender'])) {
        $this->error['gender'] = "Please select the gender";
     }

     if ($this->city == "--selectcity--") {
         $this->error['city'] = "Must select a city";
     }

     return $this->error;
}

public function addStudent($connect) {

       $userEmail = $_SESSION['email'];
       $sql = "select * from student_details where email = '$userEmail'";
       $result = mysqli_query($connect, $sql);
       $data = mysqli_fetch_assoc($result);
       $row = mysqli_num_rows($result);

       if ($row > 0) {
           $update = "update student_details set first_name = '$this->firstName',
           last_name = '$this->lastName', phone = '$this->phone', birth_date = '$this->birthDate',
           age = '$this->age', gender = '$this->gender', city = '$this->city'
           where email = '$userEmail'";
           if (mysqli_query($connect, $update)) {
             echo "<script>alert('Updated!!');
                location.replace('models/EditProfileModel.php');
             </script>";
           } else {
             echo "<script>alert('Error Updation!!');</script>";
           }
       } else {
         $sql = " insert into student_details(first_name, last_name, birth_date, age, phone,
                gender, email, city) values('$this->firstName', '$this->lastName',
                '$this->birthDate', '$this->age', '$this->phone', '$this->gender',
                '$userEmail', '$this->city')";
         if (mysqli_query($connect, $sql)){
             header('location: hobbies.php');
         } else{
             echo "<script>alert('Error!!');</script>";
         }
       }

}

static public function getStudentDetails($connect)
{
    $email = $_SESSION['email'];
    $sql = "select * from student_details where email = '$email'";
    $query = mysqli_query($connect, $sql);
    $studentDetails = mysqli_fetch_assoc($query);

    return $studentDetails;
}

static public function getData($connect)
{
    $email = $_SESSION['email'];
    $sql = "select * from signup where email = '$email'";
    $query = mysqli_query($connect, $sql);
    $data = mysqli_fetch_assoc($query);

    return $data;
}

static public function uploadProfile($connect, $id)
{
    $id = $id['id'];
    $targetDirectory = "uploads/";
    $extension = basename($_FILES["fileToUpload"]["name"]);
    $fileType = strtolower(pathinfo($extension, PATHINFO_EXTENSION));
    $targetFile = $targetDirectory . $id.".".$fileType;
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);

    if ($check !== false) {
        $upload = 1;
    } else {
        $upload = 0;
        echo "<script>alert('File is not Image type');
        location.replace('../models/EditProfileModel.php');
        </script>";
    }

    if (file_exists($targetFile)) {
        $upload = 0;
        echo "<script>alert('File already Exists');
        location.replace('../models/EditProfileModel.php');
        </script>";
    }

    if ($_FILES["fileToUpload"]["size"] > 500000) {
        $upload = 0;
        echo "<script>alert('Sorry, your file is too large.');
        location.replace('../models/EditProfileModel.php');
        </script>";
    }

    if ($fileType != "jpg" && $fileType != "png" && $fileType != "jpeg") {
        $upload = 0;
        echo "<script>alert('Sorry, only JPG, JPEG, PNG & GIF files are allowed.');
        location.replace('../models/EditProfileModel.php');
        </script>";
    }

    if ($upload == 0) {
        $error = "Upload Error";
        echo $error;
    } else {

        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $targetFile)) {
            $sql = "insert into images(id, image_name) values ($id, '$targetFile')";
            mysqli_query($connect, $sql);
            echo "<script>alert('File Uploaded successfully');</script>";

        } else {
            echo "<script>alert('Error Uploading');</script>";
        }
    }

}

}
