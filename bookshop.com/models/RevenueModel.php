<?php
include 'Pagination.php';
class RevenueModel extends Pagination
{
    public $connect;

    public function __construct($connect)
    {
        $this->connect = $connect;
    }

    public function getTotalRecord()
    {
        $result = mysqli_query($this->connect, "select buyer_details.book_id, book_name, count(buyer_details.book_id) as quantity,
                  books.price as price, count(buyer_details.book_id)*books.price as amount from buyer_details join books
                  on buyer_details.book_id = books.book_id group by buyer_details.book_id");
        $total_record = mysqli_num_rows($result);

        return $total_record;
    }

    public function getResult($starting_record)
    {
        $result = mysqli_query($this->connect, "select buyer_details.book_id, book_name, count(buyer_details.book_id) as quantity,
                  books.price as price, count(buyer_details.book_id)*books.price as amount from buyer_details join books
                  on buyer_details.book_id = books.book_id group by buyer_details.book_id limit $starting_record, 5");

        return $result;
    }


    public function getTotalRevenue()
    {
         $query = mysqli_query($this->connect, "select sum(price) as price from books join buyer_details on books.book_id = buyer_details.book_id");
         $value = mysqli_fetch_assoc($query);
        
         return $value;
    }
}
