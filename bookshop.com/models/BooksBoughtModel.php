<?php
include 'Pagination.php';
class BooksBoughtModel extends Pagination
{
    public $connect;

    public function __construct($connect)
    {
        $this->connect = $connect;
    }

    public function getTotalRecord()
    {
        $result = mysqli_query($this->connect, "select email, buyer_details.book_id, book_name,
                author, date_of_purchase from books right join buyer_details
                on books.book_id = buyer_details.book_id");
        $total_record = mysqli_num_rows($result);

        return $total_record;
    }

    public function getResult($starting_record)
    {
        $result = mysqli_query($this->connect, "select email, buyer_details.book_id, book_name,
                author, date_of_purchase from books right join buyer_details
                on books.book_id = buyer_details.book_id limit $starting_record, 5");

        return $result;
    }

    public function getSearch($email)
    {
        $result = mysqli_query($this->connect, "select email, buyer_details.book_id, book_name,
                    author, date_of_purchase from books right join buyer_details
                    on books.book_id = buyer_details.book_id where email = '$email'");

        return $result;
    }

}
