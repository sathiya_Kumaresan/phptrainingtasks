<?php
include '../models/BooksModel.php';
include 'BooksInterface.php';

class BooksAdaptor implements BooksInterface
{
     public $instance;

     public function __construct()
     {
         $this->instance = new BooksModel();
     }

     public function addBooks($connect)
     {
         $this->instance->addBooks($connect);
     }
}
