<?php

class DeleteModel
{
     public function delete($id, $connect)
     {
       try {
          $hobby = "delete from hobbies where id = $id";     //deleting hobby since foreign key has to be deleted  first
          mysqli_query($connect, $hobby);

          $file = "select image_name from images where id = $id";     //to unlink the image in the upload folder
          $imageName = mysqli_query($connect, $file);
          $location = mysqli_fetch_assoc($imageName);
          unlink($location['image_name']);

          $image = "delete from images where id = $id";        //delete the image from database
          mysqli_query($connect, $image);

          $sql = "delete from student_details where id = $id";   //deleting the student details from database
          if (mysqli_query($connect, $sql)) {
              echo "<script>alert('Record Deleted');</script>";
          } else {
              echo "<script>alert('Error Deleting');</script>";
          }
       } catch (Exception $e) {
         echo $e;
       }
     }
}
