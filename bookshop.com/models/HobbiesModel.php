<?php

class HobbiesModel
{
   public function __construct($connect)
   {
       $this->connect = $connect;
   }

   public function addHobby($compare, $id)
   {
       $value = $_POST['hobbies'];
       if (empty($_POST['hobbies'])) {
           echo "<html><script>alert('you haven't checked anything');</script></html>";
       } else {
           if (!empty($compare)) {
               $intersect_array = array_intersect($value, $compare);
               $toInsert = array_diff($value, $compare);
               $toDelete = array_diff($compare, $value);

               if (empty($toInsert) && empty($toDelete)) {
                   echo "<script>alert('already Updated');</script>";
               } else {
                   foreach ($toInsert as $hobby) {
                       $sql = "insert into hobbies(id, hobbies) values ($id, '$hobby')";
                       if (mysqli_query($this->connect, $sql)) {
                           echo "<script>alert('Updated');location.replace('../views/hobbies.php')</script>";
                       } else {
                           echo "Insert error";
                       }
                   }

                   if (isset($toDelete)) {
                       foreach ($toDelete as $hobby) {
                           $sql = "delete from hobbies where hobbies = '$hobby'";
                           if (mysqli_query($this->connect, $sql)) {
                             echo "<script>alert('Updated');location.replace('/views/hobbies.php')</script>";
                           } else {
                               echo "Delete error";
                           }
                       }
                   }
               }
           } else {
               foreach ($value as $hobby) {
                   $sql = "insert into hobbies(id, hobbies) values ($id, '$hobby')";
                   if (mysqli_query($this->connect, $sql)) {
                       echo "<script>alert('Updated');location.replace('/views/hobbies.php')</script>";
                   } else {
                       echo "update error";
                   }
               }
           }
       }
   }

   static public function getCompare($connect, $id)
   {
       $value = $_POST['hobbies'];
       $i = 0;
       $sql = "select hobbies from hobbies where id = $id";
       $result = mysqli_query($connect, $sql);

       while ($data = mysqli_fetch_assoc($result))  {
           $compare[$i++] = $data["hobbies"];
       }

       return $compare;
    }

    static public function getId($connect)
    {
        $email = $_SESSION['email'];
        $sql = mysqli_query($connect, "select * from student_details where email = '$email'");
        $selectId = mysqli_fetch_assoc($sql);
        $id = $selectId['id'];

        return $id;
    }
}
