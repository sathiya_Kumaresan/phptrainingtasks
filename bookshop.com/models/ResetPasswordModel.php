<?php

class ResetPasswordModel
{
    public $connect;
    public $selector;
    public $validator;
    public $password;
    public $rePassword;

    public function __construct($connect, $selector, $validator, $password, $rePassword)
    {
        $this->connect = $connect;
        $this->selector = $selector;
        $this->validator = $validator;
        $this->password = $password;
        $this->rePassword = $rePassword;
    }

    public function validate()
    {
        if (empty($this->password) || empty($this->rePassword)) {
            $error = "Password cannot be Empty";
            return $error;
        } else if ($this->password != $this->rePassword) {
            $error = "Password Doesn't Match";

            return $error;
        }
    }

    public function resetPassword()
    {
         $currentDate = date("U");
         $sql = "select * from reset_Password where password_selector = '$this->selector' and password_expires >= '$currentDate'";
         $result = mysqli_query($this->connect, $sql);
         $check = mysqli_fetch_assoc($result);
    
         if (empty($check)) {
             return "Something Went Wrong Please Request For Reset Password";
         } else {
             echo "<br>validator",$this->validator,"<br>";
             $tokenBin = hex2bin($this->validator);
             $tokenBin = md5($tokenBin);
             $tokenCheck = strcmp($tokenBin, $check['password_token']);

             if ($tokenCheck === false) {
                 return "You Need To Request For Reset Password";
             } else {
                 $updateEmail = $check['email'];
                 $sql = "select * from signup where email = '$updateEmail'";
                 if (!mysqli_query($this->connect, $sql)) {
                     return "You Need to signin";
                 } else {
                      $password = md5($this->password);
                      $sql = "update signup set password = '$password' where email = '$updateEmail'";
                      mysqli_query($this->connect, $sql);
                      echo "<script>alert('Password Updated');
                      location.replace('../views/login.php')</script>";
                 }
             }
         }
     }
}
