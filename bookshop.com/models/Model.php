<?php
include '../connection.php';

class Model
{
      public $connect;

      public function __construct()
      {
          $db = Database::getConnect();
          $this->connect = $db->getConnection();
      }

      public function getQuery($sql)
      {
          return mysqli_query($this->connect, $sql);
      }

      public function getExecuteQuery($result)
      {
          return mysqli_fetch_assoc($result);
      }

      public function getNumberOfRows($result)
      {
          return mysqli_num_rows($result);
      }
}

$value = new Model();
$result = $value->getQuery('select * from books');
$query = $value->getExecuteQuery($result);
$rows = $value->getNumberOfRows($result);
var_dump($result);
var_dump($query);
var_dump($rows);
echo $rows;
