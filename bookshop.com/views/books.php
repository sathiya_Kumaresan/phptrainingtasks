<html>
<head>
  <title>ADDBOOKS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
  <h1>ADD BOOKS OR UPDATE BOOKS</h1>
  <form action = "../models/BooksModel" method = "POST">
    <fieldset>
   <div>
    <div>
      <label for = "bookName">BOOK NAME</label>
      <input type = "text" name ="books[bookName]" placeholder="BOOKNAME" required/>
    </div>
    <div>
      <label for = "author">AUTHOR</label>
      <input type = "text" name ="books[author]" placeholder="AUTHOR" required/>
    </div>
    <div>
        <label for = "category">CATEGORY</label>
         <select name = "books[category]">
           <option value = "--selectcategory--">--selectcategory--</option>
           <option value = "Computer Programming" <?php if ($result[city] == "Computer Programming") {echo "selected";
       } ?>>Computer Programming</option>
           <option value = "Poems" <?php if ($result[city] == "Poems") {echo "selected";
       } ?>>Poems</option>
           <option value = "Novel" <?php if ($result[city] == "Novel") {echo "selected";
       } ?>>Novel</option>
           <option value = "Genreal Knowledge" <?php if ($result[city] == "Genreal Knowledge") {echo "selected";
       } ?>>Genreal Knowledge</option>
         </select>
    </div>
    <div>
      <label for = "quantity">QUANTITY</label>
      <input type = "number" name ="books[quantity]" placeholder="QUANTITY" required/>
    </div>
    <div>
      <label for = "price">PRICE</label>
      <input type = "number" name ="books[price]" placeholder="PRICE" required/>
    </div>
    <div>
      <input type = "submit" name ="submit" value = "ADD / UPDATE"/>
    </div>
  </div>
  </fieldset>
  </form>
</body>
</html>
