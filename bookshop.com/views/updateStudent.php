<html>
   <head>
     <title> update </title>
       <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   </head>
   <body>
     <h1> UPDATE THE DETAILS </h1>
     <form name = "updateForm" action = "/editDetail" method = "POST" onsubmit = "return validateForm()">
       <fieldset>
     <div>
       <div>
       <label for = "firstName">FIRST NAME</label>
        <input type = "text" name = "updateData[firstName]" value = <?php echo $data[first_name] ?> id = "firstName">
        <span> <?php echo $error['firstName']; ?></span>
      </div>

       <div>
        <label for = "lastName">LAST NAME</label>
        <input type = "text" name = "updateData[lastName]" value = <?php echo $data[last_name] ?> id = "lastName">
        <span> <?php echo $error['lastName']; ?></span>
      </div>

       <div>
        <label for = "birthDate">DATE OF BIRTH</label>
        <input type = "date" name = "updateData[birthDate]" value = <?php echo $data[birth_date] ?> id = "birthDate">
        <span> <?php echo $error['birthDate']; ?></span>
      </div>

      <div>
        <label for = "age">AGE</label>
        <input type = "number" name = "updateData[age]" value = <?php echo $data[age] ?> id = "age">
        <span> <?php echo $error['age']; ?></span>
      </div>

      <div>
        <label for = "phone">PHONE NUMBER</label>
        <input type = "phone" name = "updateData[phone]" value = <?php echo $data[phone] ?> id = "phone">
        <span> <?php echo $error['phone']; ?></span>
      </div>

      <div>
       <div><label for = "gender">GENDER</label></div>
       <div><input id = "male" type = "radio" name = "updateData[gender]" value = "female"
       <?php if ($data[gender] == female) {
                  echo "checked";
       } ?>>  <label for = "female">FEMALE</label><br></div>
       <div><input id = "female" type = "radio" name = "updateData[gender]" value = "male"
       <?php if ($data[gender] == "male") { echo "checked";
       } ?> >  <label for = "gender">MALE</label></div>
       <span> <?php echo $error['gender']; ?></span>
     </div>


       <div>
        <label for = "email">EMAIL</label>
        <input type = "email" name = "updateData[email]" value = <?php echo $data[email] ?> disabled>
      </div>

      <div>
       <label for = "city">CITY</label>
        <select name = "updateData[city]" id = "city">
          <option id = "Coimbatore" value = "Coimbatore" <?php if ($data[city] == "Coimbatore") {echo "selected";
        } ?>>Coimbatore</option>
          <option id = "Chennai" value = "Chennai" <?php if ($data[city] == "Chennai") {echo "selected";
        } ?>>Chennai</option>
          <option id = "Salem" value = "Salem" <?php if ($data[city] == "Salem") {echo "selected";
        } ?>>Salem</option>
          <option id = "Hosur" value = "Hosur" <?php if ($data[city] == "Hosur") {echo "selected";
        } ?>>Hosur</option>
      </select>
      <span> <?php echo $error['city']; ?></span>
     </div>

     <div>
        <button name = "updateData[update]" value = <?php echo $_GET['id']; ?>>UPDATE</button>
     </div>

   </div>
      </fieldset>
     </form>
   </body>
</html>
<script>
function validateForm() {
  var firstName = document.getElementById('firstName').value;
  var lastName = document.getElementById('lastName').value;
  var birthDate = document.getElementById('birthDate').value;
  var age = document.getElementById('age').value;
  var phone = document.getElementById('phone').value;

  if (firstName == "") {
    alert("First Name must be filled out");
    return false;
  }

  if (lastName == "") {
    alert("Last Name must be filled out");
    return false;
  }
  if (birthDate == "") {
    alert("Date of Birth must be filled out");
    return false;
  }
  if (age == "") {
    alert("Age must be filled out");
    return false;
  }
  if (phone == "") {
    alert("Phone must be filled out");
    return false;
  }
}
</script>
