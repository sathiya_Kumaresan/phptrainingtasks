 <html>
 <head>
   <title>BUYER DETAILS</title>
     <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 </head>
 <body>
   <div>
     <h1> BOOKS PURCHASED </h1>
   </div>
   <div>
    <table>
      <tr>
        <th>EMAIL</th>
        <th>BOOK_ID</th>
        <th>BOOK NAME</th>
        <th>AUTHOR</th>
        <th>DATE OF PURCHASE</th>
      </tr>
      <?php
      if (mysqli_num_rows($result) < 1 )  {
          echo "<script>alert('Search Not Found');
          location.replace('booksBought');
          </script>";

      } else {
          while ($details = mysqli_fetch_assoc($result)) {
              echo "<tr>",
              "<td>",$details['email'],"</td>",
              "<td>",$details['book_id'],"</td>",
              "<td>",$details['book_name'],"</td>",
              "<td>",$details['author'],"</td>",
              "<td>",$details['date_of_purchase'],"</td></tr>";
          }
      }
      ?>
    </table>
   </div>
 </body>
</html>
