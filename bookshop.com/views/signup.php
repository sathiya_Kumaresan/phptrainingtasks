<html>
     <head>
       <title> Form validation</title>
       <meta name="viewport" content="width=device-width, initial-scale=1.0" />
     </head>
     <body>
       <h1>REGISTER TO LOGIN</h1>
       <form method = "POST" action = "../models/SignupModel.php">
         <fieldset>
          <div>
          <div>
          <label for = "firstName">FIRST NAME</label>
          <input type = "text" name = "data[firstName]" placeholder="FirstName" value = <?php echo $data['firstName']?>>
          <span> <?php echo $error['firstName']; ?></span>
          </div>

          <div>
          <label for = "lastName">LAST NAME</label>
          <input type = "text" name = "data[lastName]" placeholder="LastName" value = <?php echo $data['lastName']?>>
          <span> <?php echo $error['lastName']; ?></span>
          </div>

          <div>
          <label for = "phone">PHONE NUMBER</label>
          <input type = "phone" name = "data[phone]" placeholder="Phone Number" value = <?php echo $data['phone']?>>
          <span> <?php echo $error['phone']; ?></span>
          </div>

          <div>
          <label for = "email">EMAIL</label>
          <input type = "email" name = "data[email]" placeholder="Email" value = <?php echo $data['email']?>>
          <span> <?php echo $error['email']; ?></span>
          </div>

          <div>
          <label for = "password">PASSWORD</label>
          <input type = "password" placeholder="Password" name = "data[password]">
          <span> <?php echo $error['password']; ?></span>
         </div>

          <div>
          <label for = "password">CONFIRM PASSWORD</label>
          <input type = "Password" placeholder="Confirm Password" name = "data[confirmPassword]">
          <span> <?php echo $error['confirmPassword']; ?></span>
          </div>

          <div>
          <input type = "submit" value = "SIGN UP" name = "data[submit]">
          </div>
        </div>
        </fieldset>
        </form>
      </body>
</html>
