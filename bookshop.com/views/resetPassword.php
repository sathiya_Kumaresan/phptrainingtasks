<html>
<head>
  <title>RESETPASSWORD</title>
</head>
<body>
       <?php
         $selector = $_GET['selector'];
         $validator = $_GET['validator'];
         if (empty($selector) && empty($validator)) {
            echo "Could not validate your request";
         } else {
            if (ctype_xdigit($selector) !== false && ctype_xdigit($validator) !== false) {
        ?>
        <h1>RESET PASSWORD</h1>
        <div>
        <form action = "../models/ResetPasswordModel.php" method = "POST">
          <fieldset>

          <input type = "hidden" name = "selector" value = "<?php echo $selector ?>">
          <input type = "hidden" name = "validator" value = "<?php echo $validator ?>">
          <div>
          <label for = "password">PASSWORD</label>
        </div>
        <div>
          <input type = "password" placeholder="Reset Password" name = "password" required>
        </div>
        <div>
          <label for = "repassword">REPEAT PASSWORD</label>
        </div>
        <div>
          <input type = "password" placeholder="Repeat Password" name = "repassword" required>
        </div>
        <div>
          <span><?php echo $_GET['error']; ?></span>
        </div>
        <div>
          <input type = "submit" name = "submit" value = "RESET">
        </div>
        </fieldset>
        </form>
      </div>
        <?php
            }
          }
         ?>
</body>
</html>
