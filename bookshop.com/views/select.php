 <html>
  <head>
    <title>view Details</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <h1> STUDENT DETAILS </h1>
    <div>
    <table>
      <tr>
        <th>ID</th>
        <th>FIRST NAME</th>
        <th>LAST NAME</th>
        <th>DATE OF BIRTH</th>
        <th>AGE</th>
        <th>PHONE</th>
        <th>GENDER</th>
        <th>EMAIL</th>
        <th>CITY</th>
        <th>HOBBIES</th>
        <th>DELETE</th>
        <th>UPDATE</th>
      </tr>
      <?php
      while ($data = mysqli_fetch_assoc($result)) {
        $id = $data['id'];
        $value = SelectModel::getHobbies($id, $this->connect);
        echo "<tr><td>",
        $data['id'],"</td><td>",$data['first_name'],"</td><td>",$data['last_name'],"</td><td>",$data['birth_date'],"</td>",
        "<td>",$data['age'],"</td><td>",$data['phone'],"</td><td>",$data['gender'],"</td>",
        "<td>",$data['email'],"</td>",
        "<td>",$data['city'],"</td>",
        "<td>",$value,"</td>",
        "<td><a href='/delete/id/$id'>Delete</a></td>",
        "<td><a href='/updateStudent/id/$id'>Update</a></td>",
        "</tr>";

      } ?>
    </table>
  </div>
  </body>
</html>
