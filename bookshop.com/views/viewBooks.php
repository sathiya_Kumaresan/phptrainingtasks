 <html>
  <head>
    <title>VIEW BOOK DETAILS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>
  <body>
    <h1> BOOK DETAILS  </h1>
    <div>
    <table>
      <tr>
        <th>BOOK ID</th>
        <th>BOOK NAME</th>
        <th>AUTHOR</th>
        <th>PRICE</th>
        <th>QUANTITY</th>
      </tr>

      <?php
      try {
          while ($data = mysqli_fetch_assoc($result)) {
            $id = $data['id']; ?>
            <tr>
            <td><?php echo $data['book_id']; ?></td>
            <td><?php echo $data['book_name']; ?></td>
            <td><?php echo $data['author']; ?></td>
            <td><?php echo $data['price']; ?></td>
            <td><?php echo $data['quantity']; ?></td>
          </tr>
    <?php
          }
      } catch (Exception $e) {
          echo $e;
      }
     ?>

    </table>
  </div>
  </body>
</html>
