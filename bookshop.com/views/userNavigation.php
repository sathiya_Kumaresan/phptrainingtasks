<html>
<head>
   <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <link rel = "stylesheet" type = "text/css" href = "../../stylesheet.css">
</head>
<body>
<div class = 'navigation'>
  <a href = "../logout">LOGOUT</a>
  <a href = "../myOrders">MY ORDERS</a>
  <div class="dropdown">
      <button class="dropbtn" id = "buyBook">VIEW BOOKS
      </button>
      <div class="dropdown-content">
        <a href = "/byCategory/id/1">COMPUTER PROGRAMMING</a>
        <a href="/byCategory/id/2">POEM</a>
        <a href="/byCategory/id/3">NOVEL</a>
        <a href="/byCategory/id/4">GENERAL KNOWLEDGE</a>
      </div>
    </div>
  <a href = "../hobbies">HOBBIES</a>
  <a href = "../editProfileModel">EDIT PROFILE</a>
  <a href = "../myProfile">MY PROFILE</a>
</div>
</body>
</html>

<script type="text/javascript">
    document.getElementById("buyBook").onclick = function () {
        location.href = "/buyBooks";
    };
</script>
