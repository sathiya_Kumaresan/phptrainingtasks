<html>
     <head>
       <title> Form validation</title>
       <meta name="viewport" content="width=device-width, initial-scale=1.0" />
     </head>
     <body>
       <h1> STUDENT PROFILE </h1>
       <fieldset>
       <div>
         <form action = "EditProfileModel" method = "POST" enctype = "multipart/form-data">
           <div>
         <label for = "profileImage">PROFILE IMAGE</label>
         <input type="file" name = "fileToUpload" value = "hi">
         <input type = "submit" name = "submit" value = "Upload">
       </div>
       </form>
       </div>
       <hr>
       <form action = "EditProfileModel" method = "POST">
        <div>
         <div>
         <label for = "firstName">FIRST NAME</label>
          <input type = "text" name = "data[firstName]" value = <?php echo $studentDetails['first_name'] ?: $data['firstName']?>>
          <span> <?php echo $error['firstName']; ?></span>
         </div>

          <div>
          <label for = "lastName">LAST NAME</label>
          <input type = "text" name = "data[lastName]" value = <?php echo $studentDetails['last_name'] ?: $data['lastName']?> >
          <span> <?php echo $error['lastName']; ?></span>
          </div>

          <div>
          <label for = "birthDate">DATE OF BIRTH</label>
          <input type = "date" name = "data[birthDate]" value = <?php echo $studentDetails['birth_date'] ?: $data['birthDate']?>>
          <span> <?php echo $error['birthDate']; ?></span>
          </div>

          <div>
          <label for = "age">AGE</label>
          <input type = "number" name = "data[age]" value = <?php echo $studentDetails['age'] ?: $data['age']?>>
          <span> <?php echo $error['age']; ?></span>
         </div>

          <div>
          <label for = "phone">PHONE NUMBER</label>
          <input type = "phone" name = "data[phone]" value = <?php echo $studentDetails['phone'] ?: $data['phone']?>>
          <span> <?php echo $error['phone']; ?></span>
         </div>

          <div>
            <label for = "gender">GENDER</label>
            <div>
              <input type = "radio" name = "data[gender]" value = "female"
              <?php if ($studentDetails[gender] == "female") { echo "checked";
              }?>>FEMALE
           </div>
           <div>
              <input type = "radio" name = "data[gender]" value = "male"
              <?php if ($studentDetails[gender] == "male") { echo "checked";
              } ?>>MALE
            </div>
              <span> <?php echo $error['gender']; ?></span>
         </div>

          <div>
          <label for = "email">EMAIL</label>
          <input type = "email" name = "data[email]" value = <?php echo $_SESSION['email']?> disabled>
          <span> <?php echo $error['email']; ?></span>
          </div>

         <div>
         <label for = "city">CITY</label>
          <select name = "data[city]">
            <option value = "--selectcity--">--selectcity--</option>
            <option value = "Coimbatore" <?php if ($studentDetails[city] == "Coimbatore") {echo "selected";
        } ?>>Coimbatore</option>
            <option value = "Chennai" <?php if ($studentDetails[city] == "Chennai") {echo "selected";
        } ?>>Chennai</option>
            <option value = "Salem" <?php if ($studentDetails[city] == "Salem") {echo "selected";
        } ?>>Salem</option>
            <option value = "Hosur" <?php if ($studentDetails[city] == "Hosur") {echo "selected";
        } ?>>Hosur</option>
          </select>
          <span> <?php echo $error['city']; ?></span>
         </div>

          <div>
          <input type = "submit" name = "data[submit]">
          </div>
        </div>
        </fieldset>
       </form>
     </body>
</html>
