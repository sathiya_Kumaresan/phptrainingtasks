<html>
<head>
   <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <link rel = "stylesheet" type = "text/css" href = "/stylesheet.css">
</head>
<body>
  <div class = 'navigation'>
    <a href = "/logout">LOGOUT</a>
    <div class="dropdown">
        <button class="dropbtn">ORDER DETAILS
        </button>
        <div class="dropdown-content">
          <a href = "/booksBought">ORDERS</a>
          <a href="/revenue">REVENUE</a>
          <a href="">SUMMARY</a>
        </div>
      </div>
    <a href = "/viewBooks">VIEW BOOKS</a>
    <a href = "/books">ADD/UPDATE BOOKS</a>
    <a href = "/select">STUDENT DETAILS</a>
  </div>
</body>
</html>
