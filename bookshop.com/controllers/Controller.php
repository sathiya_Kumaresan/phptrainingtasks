<?php
require_once 'session.php';
require_once 'connection.php';
require_once 'autoload.php';

class Controller
{
     public $connect;

     public function __construct()
     {
         $db = Database::getConnect();
         $this->connect = $db->getConnection();
     }

     public function about()
     {
         require_once 'views/about.php';
     }

     public function signup()
     {
         include_once 'views/about.php';
         require_once 'views/signup.php';
     }

     public function SignupModel()
     {
         if (isset($_POST['data'])) {
             $data = $_POST['data'];
             $validation = new SignupModel($_POST['data']);
             $error = $validation->validateForm();
             if (empty($error)) {
                 $error['email'] = $validation->addStudent($this->connect);
             }
             require_once 'views/about.php';
             require_once 'views/signup.php';
         }
     }

     public function LoginModel()
     {

         include_once 'views/about.php';
         $login = new LoginModel();
         $error = $login->login($this->connect);
         include_once 'views/login.php';
     }

     public function login()
     {
         include_once 'views/about.php';
         include_once 'views/login.php';
     }

     public function select()
     {
         $object = new SelectAdaptor();
         $result = $object->getStudentDetails($this->connect);
         require_once 'views/adminNavigation.php';
         require_once 'views/select.php';
     }

     public function logout()
     {
         require_once 'views/logout.php';
     }

     public function books()
     {
         require_once 'views/adminNavigation.php';
         include_once 'views/books.php';
     }

     public function BooksModel()
     {
         $addBook = new BooksAdaptor();
         $addBook->addBooks($this->connect);
         require_once 'views/adminNavigation.php';
         include_once 'views/books.php';
     }

     public function viewBooks()
     {
         $url = 'viewBooks';
         $book = new ViewBooksModel($this->connect);
         $total_record = $book->getTotalRecord();
         $number_of_page = $book->getNumberOfPages($total_record);
         $page = $book->getPage();
         $starting_record = $book->getStartingRecord($page);
         $result = $book->getResult($starting_record);
         include_once 'views/adminNavigation.php';
         include_once 'views/viewBooks.php';
         include_once 'views/pagination.php';
     }

     public function booksBought()
     {
         $searchPlaceholder = "Search By Email";
         $actionPage = 'search';

         $url = 'booksBought';
         $book = new BooksBoughtModel($this->connect);
         $total_record = $book->getTotalRecord();
         $number_of_page = $book->getNumberOfPages($total_record);
         $page = $book->getPage();
         $starting_record = $book->getStartingRecord($page);
         $result = $book->getResult($starting_record);
         require_once 'views/adminNavigation.php';
         Controller::searchTemplate($searchPlaceholder, $actionPage);
         include_once 'views/booksBought.php';
         include_once 'views/pagination.php';
     }

     public function DeleteModel()
     {
         $id = $_GET['id'];
         $delete = new DeleteModel();
         $delete->delete($id, $this->connect);
         header('location: select.php');
     }

     public function updateStudent()
     {
         $student  = new UpdateStudentModel();
         $data = $student->getEachUser($this->connect);
         require_once 'views/adminNavigation.php';
         include_once 'views/updateStudent.php';
     }

     public function editDetail()
     {
         $student  = new UpdateStudentModel();
         if (isset($_POST['updateData']) ) {
           $student->updateStudent($this->connect);
         }
     }

     public function search()
     {
         $search = $_POST['searchText'];
         $book = new BooksBoughtModel($this->connect);
         $result = $book->getSearch($search);
         require_once 'views/adminNavigation.php';
         include_once 'views/booksBought.php';
     }

     public function EditProfileModel()
     {
         $studentDetails = EditProfileModel::getStudentDetails($this->connect);
         $data = EditProfileModel::getData($this->connect);

         if (isset($_POST['data'])) {
             $validation = new EditProfileModel($_POST['data']);
             $error = $validation->validateForm();
             if (empty($error)) {
                 $error['email'] = $validation->addStudent($this->connect);
             }
          }

          if (isset($_POST['submit'])) {
              EditProfileModel::uploadProfile($this->connect, $studentDetails);
          }
          require_once 'views/userNavigation.php';
          require_once 'views/editProfile.php';
     }

     public function Hobbies()
     {
         $id = HobbiesModel::getId($this->connect);
         $compare = HobbiesModel::getCompare($this->connect, $id);
         if (isset($_POST['submit'])) {
             $hobby = new HobbiesModel($this->connect);
             $alertMessage = $hobby->addHobby($compare, $id);
         }
         require_once 'views/userNavigation.php';
         require_once 'views/hobbies.php';

     }

     public function buyBooks()
     {
         $url = 'buyBooks';
         $book = new buyBooksModel($this->connect);
         $total_record = $book->getTotalRecord();
         $number_of_page = $book->getNumberOfPages($total_record);
         $page = $book->getPage();
         $starting_record = $book->getStartingRecord($page);
         $result = $book->getResult($starting_record);
         include_once 'views/userNavigation.php';
         Controller::searchTemplate('Search By Book', 'byBook');
         include_once 'views/buyBooks.php';
         include_once 'views/pagination.php';
     }

     public function BuyModel()
     {
         $buy = new BuyModel();
         $buy->getBooks($this->connect);
     }

     public function myOrders()
     {
         $boughtBooks = new MyOrdersModel();
         $result = $boughtBooks->getData($this->connect);
         include_once 'views/userNavigation.php';
         include_once 'views/myOrders.php';
     }

     public function myProfile()
     {
          $profileDetails = new MyProfileModel($this->connect);
          $id = $profileDetails->getId();
          $row = $profileDetails->getStudentDetails($id);
          $value = $profileDetails->getHobbies($id);
          $imageLocation = $profileDetails->getLocation($id);
          include_once 'views/userNavigation.php';
          include_once 'views/myProfile.php';
     }

     public function forgotPassword()
     {
         include_once 'views/about.php';
         include_once 'views/forgotPassword.php';
     }

     public function ForgotPasswordModel()
     {
         $reset = new ForgotPasswordModel();

         if (isset($_POST['email'])) {
            $userCount = $reset->isUser($this->connect);
            if ($userCount == 1) {
                $reset->getLink($this->connect);
            } else {
                echo "<script>alert('Not a Registered User');
                location.replace('../views/signup.php')</script>";
            }
         }
     }

     static public function searchTemplate($searchPlaceholder, $actionPage)
     {
         $searchPlaceholder = $searchPlaceholder;
         $actionPage = $actionPage;
         include 'views/searchTemplate.php';
     }

     public function byCategory()
     {

       if (!empty($_GET['id'])) {
           $id = $_GET['id'];
           if ($id == 1) {
               $searchText = 'Computer Programming';
           } else if ($id == 2) {
               $searchText = 'Poems';
           } else if ($id == 3) {
               $searchText = 'Novel';
           } else if ($id == 4) {
               $searchText = 'Genreal Knowledge';
           }
           $book = new UserSearchModel();
           $query = $book->getBookCategory($this->connect, $searchText);
           include_once 'views/userNavigation.php';
           include_once 'views/searchBook.php';
       } else {
           echo "<script>alert('Empty Search Text');
           location.replace('/views/buyBooks.php');</script>";
       }

     }

     public function byBook()
     {

         if (!empty($_POST['searchText'])) {
            if (isset($_POST['search'])) {
                $searchText = $_POST['searchText'];
                $book = new UserSearchModel();
                $query = $book->getBook($this->connect, $searchText);
            }
            include_once 'views/userNavigation.php';
            include_once 'views/searchBook.php';
         } else {
             echo "<script>alert('Empty Search Text');
             location.replace('/views/buyBooks.php');</script>";
         }

     }

    public function resetPassword()
    {
        include_once 'views/about.php';
        include_once 'views/resetPassword.php';
    }

    public function ResetPasswordModel()
    {
         if (isset($_POST['submit']) ) {
             $selector = $_POST["selector"];
             $validator = $_POST["validator"];
             $password = $_POST["password"];
             $rePassword = $_POST["repassword"];
             $reset = new ResetPasswordModel($this->connect, $selector, $validator, $password, $rePassword);
             $error = $reset->validate();
             if (isset($error)) {
                 header('location: /views/resetPassword.php?selector='.$selector.'&validator='.$validator.'&error='.$error);
             } else {
                 $reset->resetPassword();
             }
         }
    }

    public function delete()
    {
        include 'views/delete.php';
    }

    public function revenue()
    {
        $url = 'revenue';
        $book = new RevenueModel($this->connect);
        $total_record = $book->getTotalRecord();
        $number_of_page = $book->getNumberOfPages($total_record);
        $page = $book->getPage();
        $starting_record = $book->getStartingRecord($page);
        $result = $book->getResult($starting_record);
        $value = $book->getTotalRevenue($connect);

        require_once 'views/adminNavigation.php';
        Controller::searchTemplate('Search By Book', '/bookRevenue');

        include_once 'views/revenue.php';
        include_once 'views/pagination.php';
    }

    public function bookRevenue()
    {
        if (!empty($_POST['searchText'])) {
           if (isset($_POST['search'])) {
               $searchText = $_POST['searchText'];
               $book = new UserSearchModel();
               $result = $book->getRevenue($this->connect, $searchText);
               $getTotalRevenue = new RevenueModel($this->connect);
               $value = $getTotalRevenue->getTotalRevenue($connect);
           }
           include_once 'views/userNavigation.php';
           include_once 'views/revenue.php';
        } else {
            echo "<script>alert('Empty Search Text');
            location.replace('/views/revenue');</script>";
        }

    }

}
