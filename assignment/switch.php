<?php

function switchcase($value)
{
    switch ($value) {
        case 0: 
            echo "This is case 0";
            break;
        case 1: 
            echo "This is case 1";
            //no break
        case 2:
        case 3:
        case 5:
            echo "This is case 2, 3, 5";
            break;
        Default:
            echo "Not valid";
            break;
     }
}

switchcase(0);echo "<br>";
switchcase(1);echo "<br>";
switchcase(2);echo "<br>";
switchcase(3);echo "<br>";
switchcase(5);echo "<br>";
switchcase(8);echo "<br>";


