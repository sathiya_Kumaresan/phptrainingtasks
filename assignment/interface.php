<?php

interface value
{
    function add();
    function multiply();
}

interface operation 
{
    function subtract();
    function divide();
}

interface inherit extends value, operation
{
    function add();
}
class Calculator implements inherit
{
    public function __construct($value, $number)
    {
        $this->value = $value;
        $this->number = $number;
    }
    
    function add()
    {
        return $this->value + $this->number;
    }
    
    function multiply()
    {
        return $this->value * $this->number;
    }
    
    function divide()
    {
        return $this->value / $this->number;
    }
    
    function subtract()
    {
        return $this->value - $this->number;
    }
}

$object = new Calculator(12, 3);
echo "Addition : ",$object->add(),"<br>";
echo "Multiplication : ",$object->multiply(),"<br>";
echo "Divide : ",$object->divide(),"<br>";
echo "Subtraction : ",$object->subtract(),"<br>";