<?php
namespace first {

const value = 1234;
const divisor = 2;
class Naming 
{
     public function add()
     {
          $number = value / divisor;
           return $number;
      }
}
}

namespace second {

const value =  4322;
const divisor = 2;
class Naming 
{
      public function add()
      {
           $number = value/divisor;
           return $number;
      }
}
}

namespace {

$object  = new first\Naming;
echo "First namespace : ", $object->add();
echo "<br>";
$object  = new second\Naming;
echo "Second namespace ",$object->add();
echo "<br>";
}