<?php

class Animal 
{
     public function eat() 
     {
         echo "Animal is eating";
     }
     
     protected function habitat()
     {
         echo "Animal is living in the forest";
     }
     
     private function work()
     {
         echo "Animal is sleeping";
     }
     
     public function protectedImplements()
     {
         $this->habitat();
     }
}

class Cat extends Animal
{
     
}

class kitty extends Cat
{

}

$object = new Kitty;
$object->eat(); 
echo "<br>";
//$object->habitat(); //protected method cannot be accessed
$object->protectedImplements(); //to access protected method
echo "<br>";
//$object->work(); // private method cannot be accessed
echo "<br>";


$catObject = new Cat;
$catObject->eat();
echo "<br>";
$catObject->protectedImplements();
echo "<br>";
//$catObject->work();//private method cannot be accessed
echo "<br>";


$Animalobject = new Animal;
$Animalobject->eat();
echo "<br>";
$Animalobject->protectedImplements();
echo "<br>";
$Animalobject->work();//private method cannot be accessed
echo "<br>";
