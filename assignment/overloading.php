<?php

class overloading
{
    public function __call($name, $arguments)
    {
       foreach ($arguments as $value)
       {
             $value += $value ;
        }
        echo $value;
     }

     public static function __callstatic($name, $arguments)
     {
          $value = 1; 
          foreach($arguments as $number)
          {
               $value *= $number; 
           }
           return $value;
      }
}

$object = new overloading;
echo "Addition of two numbers: ";
$object->add('add', 1, 2);
echo "<br>Addition of three numbers: ";
$object->add(1, 2, 3);
echo "<br>Multiplication of  numbers: ";
echo overloading::multiply(1, 2, 3, 4, 5);