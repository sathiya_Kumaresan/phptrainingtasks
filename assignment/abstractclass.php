<?php

abstract class Employee
{
    public $name = "sathiya";
    public $id = "412";
    
    public function employeeDetails()
    {
        echo $this->name," ",$this->id,"<br>";
    }
    
    abstract protected function designation();//protected function cannot be directly accessed
    
    public function implementsProtected()
    {
        $this->designation();
    }
}

abstract class Work extends Employee     //abstract class extended
{
    protected function doWork() 
    {
        echo "<br>Working<br>";
    }
    
    function doing()
    {
         $this->doWork();
    }
}

class Values extends Work
{
     protected function designation()
     {
         echo "Designation is Trainee";
     }
}

$object = new Values;
$object->employeeDetails();
$object->implementsProtected();
$object->doing();

