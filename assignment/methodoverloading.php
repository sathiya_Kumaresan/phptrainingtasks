<?php

class overloading
{
    public function add($value)
    {
        return $value;
    }
}

class nextclass extends overloading
{
    public function add($value, $number)
    {
         return $value + $number;
    }
}

$object = new nextclass();
echo "Addition of numbers: ",$object->add(12, 4);
echo "<br>";
echo "1st class value: ",$object->add(13);
