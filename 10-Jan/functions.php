<?php
//function default value
animal("tiger", "forest", "yellowishbrown");

function animal($name, $habitat, $color, $family = 'cat') 
{
    echo "The ".$name." lives in ".$habitat." and its color is ${color} "."and it is ${family} family <br>";
}
animal("cat", "house", "yellow");
animal("tiger", "forest", "yellowishbrown");



//conditional function
$name = "sathiya";

if ($name != null) {
function emptyof($name) 
{
    echo "your name is ".$name."<br>";
}    
}
emptyof($name);



//function within function
function factorial($value): int 
{
    if ($value > 1) {
    $value *= factorial($value-1);
    } 
    return $value;
   
}

echo "Factorial of ${value}: ".factorial(6)."<br>";


function name($string, $value) 
{
    if (strcmp($string, $value) == 0) { 
    return "equal";
    } else {
    return null;
    }
}

echo name("sathiya", "sathiya")."<br>";

$compare = 'name';
echo "variable call : ". $compare('ah', 'ah');


//closures
$value = "echo";
echo name(function() use ($value) {
 return $value;
}, "echo");


