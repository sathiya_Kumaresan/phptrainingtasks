<?php
trait value 
{
    function call() 
    {
        echo "This is trait";
    }
}

class Person
{
    use value;
    public $name;
    public $amount;
    public function __construct($name, $amount) 
    {
        $this->name = $name;
        $this->amount = $amount;
    }
    public function write() 
    {
        echo "The person ".$this->name." can get ".$this->amount."<br>";
    }
}

$object = new Person("sathiya", 2000);
$object->write();
$object->call();
