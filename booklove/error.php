<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Error Page Not Found</title>
    <style>
    div#error-page {
      margin-left: 30%;
      width: 60%;
      padding: 5%;
    }
    img {
        margin-left: unset;
        border-radius: unset;
    }
    </style>
  </head>
  <body>
    <div id = "error-page">
      <h1>PAGE NOT FOUND</h1>
      <img src="/404page.jpg" width = "30%" >
      <h4>The page you are searching is not found on the server. </h4>
      <h4>Try navigating the Pages!!</h4>
      <h4> OR </h4>
      <h4>Try Loging In!!</h4>
    </div>
  </body>
</html>
