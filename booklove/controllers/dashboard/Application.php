<?php
require_once 'booklove/../session.php';
require_once 'booklove/../autoload.php';
require_once 'DashboardInterface.php';

class Application implements DashboardInterface
{
    public function dashboard()
    {
        include_once 'booklove/../views/dashboardNavigation.php';
        $table = new DashboardModel;
        $data = $table->getTopSoldBooks();
        $user = $table->getTopUser();
        include_once 'booklove/../views/dashboard.php';
    }

    public function bookFile()
    {
        require_once 'booklove/../views/dashboardNavigation.php';
        $addBook = new BooksModel();
        $category = $addBook->getCategory();

        $file = new BookFileModel();
        if (isset($_POST['books'])) {
             $errorBook = $file->getCsv();
        }
        if (isset($_POST['sold'])) {
             $errorSold = $file->getsoldBooks();
        }
        if (isset($_POST['category'])) {
            if (isset($_POST['bookCategory'])) {
               $errorCategory = $file->bookSoldByCategory($_POST['bookCategory']);
            }
        }
        include_once 'booklove/../views/bookFile.php';
    }

    public function summary()
    {
        require_once 'booklove/../views/dashboardNavigation.php';
        if (isset($_POST['startDate']) && isset($_POST['endDate'])) {
            $file = new SummaryModel();
            $file->getSummaryBooks($_POST['startDate'], $_POST['endDate']);
        }
        require_once 'booklove/../views/summary.php';
    }


    public function revenue()
    {
        require_once 'booklove/../views/dashboardNavigation.php';
        $book = new RevenueModel();
        $result = $book->getResult();
        $totalRevenue = $book->getTotalRevenue();
        if (isset($_POST['submit'])) {
            $book->getFile();
        }

        include_once 'views/revenue.php';
    }


    public function error()
    {
        include_once 'booklove/../error.php';
    }
}
