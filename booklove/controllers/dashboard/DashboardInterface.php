<?php

interface DashboardInterface
{
     public function dashboard();
     public function bookFile();
     public function summary();
     public function revenue();
}
