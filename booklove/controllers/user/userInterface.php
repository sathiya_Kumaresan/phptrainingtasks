<?php

/**
 *
 */
interface userInterface
{
    public function myProfile();
    public function editProfile();
    public function myOrders();
    public function buyBooks();
}
