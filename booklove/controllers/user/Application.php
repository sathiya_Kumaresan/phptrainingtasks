<?php
require_once 'booklove/../session.php';
require_once 'booklove/../autoload.php';
require_once 'booklove/../views/userNavigation.php';
require_once 'userInterface.php';
include_once 'models/EditProfileModel.php';
use models\EditProfileModel as EditProfileModel;

class Application implements userInterface
{

     public function myProfile()
     {
         $getProfileDetails = new MyProfileModel();
         $id = $getProfileDetails->getId();
         $row = $getProfileDetails->getStudentDetails($id);
         $value = $getProfileDetails->getHobbies($id);
         include_once 'booklove/../views/myProfile.php';
     }

     public function editProfile()
     {

         $profile = new EditProfileModel();
         $studentDetails = $profile->getData();
         $hobby = $profile->getHobbies($studentDetails['id']);

         include_once 'booklove/../views/editProfile.php';
     }

     public function edit()
     {

                
     }

     public function myOrders()
     {
         $booksBought = new MyOrdersModel();
         $data = $booksBought->getData();
         include_once 'booklove/../views/myOrders.php';
     }

     public function buyBooks()
     {
            $addBook = new BooksModel();
            $category = $addBook->getCategory();

            if (isset($_GET['id'])) {
             $id = $_GET['id'];
             $book = new BuyBooksModel();
             $message = $book->placeOrder($id);
            }

            if (isset($_POST['categoryName'])) {
              $book = new BuyBooksModel();
              $categoryId = $book->getCategoryId($_POST['categoryName']);
              $data = $book->getBookByCategory($categoryId);

            } else {
              $book = new BuyBooksModel();
              $data = $book->getResult();

            }


         include_once 'booklove/../views/buyBooks.php';

     }

     public function error()
     {
         include_once 'booklove.com/../error.php';
     }
}
