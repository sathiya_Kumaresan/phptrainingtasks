<?php
require_once 'booklove/../autoload.php';
require_once 'AuthenticationInterface.php';

class Application implements AuthenticationInterface
{
    public function login()
    {
       if (isset($_POST['submit'])) {
           $login = new LoginModelAdaptor();
           $error = $login->autheticateUser();
       }
       include_once 'views/about.php';
       include_once 'views/login.php';
    }

    public function signup()
    {
       if (isset($_POST['data'])) {
           $data = $_POST['data'];
       }

       if (isset($data['submit'])) {
           $user = new SignupModel();
           $user->setFirstName($data['firstName']);
           $user->setLastName($data['lastName']);
           $user->setEmail($data['email']);
           $user->setPassword($data['password']);
           $user->setConfirmPassword($data['confirmPassword']);
           $user->validateForm();
           $error = $user->getError();
           $checkUser = $user->checkUser();
           if (!isset($error)) {
               if ($checkUser == "true") {
                   $message = $user->addStudent();
                   echo "<script>alert('$message');
                   location.replace('/authentication/login');
                   </script>";
               } else {
                   echo "<script>alert('$checkUser');
                   location.replace('/authentication/login');
                   </script>";
               }
           }
       }
       include_once 'views/about.php';
       require_once 'views/signup.php';
    }

    public function forgotPassword()
    {
        include_once 'views/about.php';
        include_once 'views/forgotPassword.php';

         if (isset($_POST['email'])) {
            $reset = new ForgotPasswordModel();
            $userCount = $reset->isUser();
            if ($userCount == 1) {
                $reset->getLink();
            } else {
                echo "<script>alert('Not a Registered User');
                location.replace('/authentication/signup')</script>";
            }
         }
    }

    public function resetPassword()
    {
        include_once 'views/about.php';
        include_once 'views/resetPassword.php';

        if (isset($_POST['submit'])) {
             $reset = new ResetPasswordModel();
             $reset->setSelector($_POST['selector']);
             $reset->setPassword($_POST['password']);
             $reset->setConfirmPassword($_POST['repassword']);
             $error = $reset->validate();
             if (isset($error)) {
                 echo "Error";
             } else {
                $message = $reset->resetPassword();
                echo "<script>alert('$message');
                location.replace('../authentication/login')</script>";
             }
        }
    }

    public function logout()
    {
         session_start();
         session_unset();
         session_destroy();
         header('location: /authentication/login');
    }


    public function error()
    {
        include_once 'booklove.com/../error.php';
    }
}
