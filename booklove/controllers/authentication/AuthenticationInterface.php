<?php

/**
 *
 */
interface AuthenticationInterface
{
    public function login();
    public function signup();
    public function logout();
}
