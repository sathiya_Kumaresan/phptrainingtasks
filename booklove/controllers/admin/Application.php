<?php
require_once 'booklove/../session.php';
require_once 'booklove/../autoload.php';
require_once 'booklove/../views/adminNavigation.php';
require_once 'AdminInterface.php';

class Application implements AdminInterface
{
    public $id;

    public function showUser()
    {
        $object = new ShowUserModel();
        $data = $object->getStudentDetails();
        $hobby = $object->getHobbies($data);

        require_once 'booklove/../views/showUser.php';
    }

    public function addBooks()
    {
        $addBook = new BooksModel();
        $category = $addBook->getCategory();
        if (isset($_POST['submit'])) {
            $message = $addBook->addBooks();
        }
        if (isset($_POST['add'])) {
            $addCategory = new BooksModel();
            $message = $addCategory->addCategory();
        }

        if (isset($message)) {
            echo "<script>alert('$message');
            location.replace('/admin/addBooks');
            </script>";
        }

        require_once 'booklove/../views/addBooks.php';
    }

    public function showBooks()
    {
        $book = new BuyBooksModel();
        $data = $book->getResult();

       require_once 'booklove/../views/showBooks.php';
    }

    public function activateUser() {
        if (isset($_GET['id'])) {
           $book = new ShowUserModel();
           $data = $book->activateUser($_GET['id']);

        } else {
            echo "request error";
        }

    }

    public function delete()
    {
        if (isset($_GET['id'])) {
             $this->id = $_GET['id'];
             $object = new ShowUserModel();
             $message = $object->deleteUser($this->id);
        }
    }

    public function error()
    {
        include_once 'booklove.com/../error.php';
    }

}
