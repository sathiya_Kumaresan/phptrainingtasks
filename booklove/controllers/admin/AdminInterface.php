<?php

interface AdminInterface
{
     public function showUser();
     public function addBooks();
     public function showBooks();
     public function delete();
}
