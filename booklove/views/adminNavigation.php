<html>
<head>
   <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <link rel = "stylesheet" type = "text/css" href = "/stylesheet.css">
</head>
<body>
  <div class = 'navigation' id = "navigation">
    <a href="javascript:void(0);" class="icon" onclick="responsiveNav()">
       <i class="fa fa-bars"></i>
    </a>
    <a href = "/authentication/logout">LOGOUT</a>
    <a href = "/dashboard/dashboard">DASHBOARD</a>
    <a href = "/admin/showBooks">VIEW BOOKS</a>
    <a href = "/admin/addbooks">ADD/UPDATE BOOKS</a>
    <a href = "/admin/showUser">USER DETAILS</a>
    <p id = "welcome">WELCOME ADMIN</p>
  </div>
</body>


<script type="text/javascript">
    function responsiveNav() {
        var nav = document.getElementById("navigation");
        if (nav.className === "navigation") {
           nav.className += " responsive";
        } else {
           nav.className = "navigation";
        }
    }
</script>
</html>
