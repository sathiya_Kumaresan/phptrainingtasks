<html>
     <head>
       <title> Form Validation</title>
       <meta name="viewport" content="width=device-width, initial-scale=1.0" />
     </head>
     <body>
       <div class = "login-image">
        <img src = "../library.jpg" class = "login-image">
       </div>
       <div class = "login-form">
       <h1 class = "heading">REGISTER TO LOGIN</h1>
       <form method = "POST" action = "../authentication/signup" onsubmit="return validateForm()">
         <fieldset class = "login-container">
          <div class = "main-content">
          <div class = "sub-content">
          <label class = "label-title" for = "firstName">FIRST NAME</label>
          <input type = "text" id = "firstName" name = "data[firstName]" placeholder="FirstName" value = <?php if (isset($data['firstName'])) {echo $data['firstName']; }?>>
          <span> <?php if (isset($error['firstName'])) {echo $error['firstName']; }?></span>
          </div>

          <div class = "sub-content">
          <label class = "label-title" for = "lastName">LAST NAME</label>
          <input type = "text" id = "lastName" name = "data[lastName]" placeholder="LastName" value = <?php if (isset($data['lastName'])) {echo $data['lastName']; }?>>
          <span> <?php if (isset($error['lastName'])) {echo $error['lastName']; }?></span>
          </div>

          <div class = "sub-content">
          <label class = "label-title" for = "email">EMAIL</label>
          <input type = "email" id = "email" name = "data[email]" placeholder="Email" value = <?php if (isset($data['email'])) {echo $data['email']; }?>>
          <span> <?php if (isset($error['email'])) {echo $error['email']; }?></span>
          </div>

          <div class = "sub-content">
          <label class = "label-title"  for = "password">PASSWORD</label>
          <input type = "password" id = "password" placeholder="Password" name = "data[password]">
          <span> <?php if (isset($error['password'])) {echo $error['password']; }?></span>
          </div>

          <div class = "sub-content">
          <label class = "label-title"  for = "password">CONFIRM PASSWORD</label>
          <input type = "Password" id = "confirmPassword" placeholder="Confirm Password" name = "data[confirmPassword]">
          <span> <?php if (isset($error['confirmPassword'])) {echo $error['confirmPassword']; }?></span>
          </div>

          <div class = "submit-form">
          <input type = "submit" value = "SIGN UP" name = "data[submit]">
          </div>
        </div>

        <div class = "login-link">
           <a href = "/authentication/login"> Already have an account? Login</a>
        </div>
        </fieldset>
        </form>
        </div>
      </body>
</html>

<script>
function validateForm() {
  var firstName = document.getElementById('firstName').value;
  if (firstName == "") {
    alert("First Name must be filled out");
    return false;
  }

  var lastName = document.getElementById('lastName').value;
  if (lastName == "") {
    alert("Last Name must be filled out");
    return false;
  }

  var email = document.getElementById('email').value;
  if (email == "") {
    alert("Email must be filled out");
    return false;
  }

  var password = document.getElementById('password').value;
  if (password == "") {
    alert("Password must be filled out");
    return false;
  }

  var rePassword = document.getElementById('confirmPassword').value;
  if (rePassword == "") {
    alert("Confirm Password cannot be empty");
    return false;
  }
}
</script>
