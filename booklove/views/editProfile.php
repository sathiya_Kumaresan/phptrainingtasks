<html>
  <head>
    <title> Form validation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  </head>
  <body>
      <fieldset class = "image-content">
       <div class = "main-content">
          <h1 class = "heading">PROFILE IMAGE</h1><hr>
          <form method = "POST" enctype = "multipart/form-data">
          <div class = "sub-content">
             <label class = "label-title" for = "profileImage">PROFILE IMAGE</label>
             <input type="file" name = "fileToUpload" value = "hi">
             <input type = "submit" name = "submit" value = "Upload">
          </div>
          </form>
       </div>
     </fieldset>

      <div class = "main-content">

        <fieldset class = "profile-content">
          <h1 class = "heading">UPDATE PROFILE</h1><hr>
         <div class = "sub-content">
            <label class = "label-title" for = "firstName">FIRST NAME</label>
            <input type = "text" placeholder = "First Name" name = "data[firstName]" id = "firstName" value = <?php
            if (isset($studentDetails['first_name'])) {
                echo "${studentDetails['first_name']}";
            } else if (isset($data['firstName'])) {
                echo "${data['firstName']}";
            }?> disabled>
            <button class = "edit" id = "edit-firstName"><i class="fa fa-edit"></i></button>
            <button class = "update" id = "update-firstName">UPDATE</button>
            <span> <?php if (isset($error['firstName'])) {echo $error['firstName'];} ?></span>
         </div>

         <div class = "sub-content">
            <label class = "label-title" for = "lastName">LAST NAME</label>
            <input type = "text" placeholder = "Last Name" name = "data[lastName]" id = "lastName" value = <?php
            if (isset($studentDetails['last_name'])) {
               echo "'${studentDetails['last_name']}'";
            } else if (isset($data['lastName'])) {
               echo "'${data['lastName']}'";
            }?>>
            <button class = "edit" id = "edit-lastName"><i class="fa fa-edit"></i></button>
            <button class = "update" id = "update-lastName">UPDATE</i></button>
            <span> <?php if (isset($error['lastName'])) {echo $error['lastName'];} ?></span>
         </div>

          <div class = "sub-content">
             <label class = "label-title" for = "birthDate">DATE OF BIRTH</label>
             <input type = "date" placeholder = "Birth Date" name = "data[birthDate]" id = "birthDate" value = <?php
             if (isset($studentDetails['birth_date'])) {
                echo $studentDetails['birth_date'];
             } else if (isset($data['birthDate'])) {
                echo $data['birthDate'];
             }?> >
             <button class = "edit" id = "edit-birthDate"><i class="fa fa-edit"></i></button>
             <button class = "update" id = "update-birthDate">UPDATE</i></button>
             <span> <?php if (isset($error['birthDate'])) {echo $error['birthDate'];} ?></span>
          </div>

          <div class = "sub-content">
             <label class = "label-title" for = "age">AGE</label>
             <input type = "number" placeholder = "Age" name = "data[age]" id = "age" value = <?php
             if (isset($studentDetails['age'])) {
                echo $studentDetails['age'];
             } else if (isset($data['age'])) {
                echo $data['age'];
             }?> >
             <button class = "edit" id = "edit-age"><i class="fa fa-edit"></i></button>
             <button class = "update" id = "update-age">UPDATE</i></button>
             <span><?php if (isset($error['age'])) {echo $error['age'];} ?></span>
         </div>

          <div class = "sub-content">
             <label class = "label-title" for = "phone">PHONE NUMBER</label>
             <input type = "phone" placeholder = "Phone Number" id = "phone" name = "data[phone]" value = <?php
             if (isset($studentDetails['phone'])) {
                echo $studentDetails['phone'];
             } else if (isset($data['phone'])) {
                echo $data['phone'];
             }?> >
             <button class = "edit" id = "edit-phone"><i class="fa fa-edit"></i></button>
             <button class = "update" id = "update-phone">UPDATE</i></button>
             <span><?php if (isset($error['phone'])) {echo $error['phone'];} ?></span>
         </div>

          <div class = "sub-content">
             <label class = "label-title" for = "gender">GENDER</label>
             <div class = "sub-content">
                <input type = "radio" name = "data[gender]" value = "female" disabled
                <?php if((isset($studentDetails['gender']) && $studentDetails['gender'] == "female") ||
                 (isset($data['gender']) && $data['gender'] == "female")
                ) { echo "checked";
                }?>  > <label for = "female" class = "radio-button"><tr>FEMALE</tr></label>
             </div>
             <div class = "sub-content">
                <input type = "radio" name = "data[gender]" value = "male"  disabled
                <?php if((isset($studentDetails['gender']) && $studentDetails['gender'] == "male") ||
                (isset($data['gender']) && $data['gender'] == "male")
                ) { echo "checked";
                }?>> <label for = "male" class = "radio-button"><tr>MALE</tr></label>
             </div>
                <span> <?php if (isset($error['gender'])) {echo $error['gender'];} ?></span>
          </div>

          <div class = "sub-content">
             <label class = "label-title" for = "email">EMAIL</label>
             <input type = "email" placeholder = "Email" name = "data[email]" value = <?php if(isset($_SESSION['email'])) {echo $_SESSION['email']; }?> disabled>
             <span> <?php if (isset($error['email'])) {echo $error['email'];} ?></span>
          </div>

         <div class = "sub-content">
            <label class = "label-title" for = "city">CITY</label>
            <select name = "data[city]" class = "select-city" id = "firstName">
               <option value = "--selectcity--">--selectcity--</option>
               <option value = "Coimbatore"
                 <?php if ((isset($studentDetails['city']) && $studentDetails['city'] == "Coimbatore") ||
                  isset($data['city']) && $data['city'] == "Coimbatore") {
                           echo "selected";
                        } ?>>Coimbatore</option>
              <option value = "Chennai"
                 <?php if ((isset($studentDetails['city']) && $studentDetails['city'] == "Chennai") ||
                 (isset($data['city']) && $data['city'] == "Chennai")) {
                           echo "selected";
                       } ?>>Chennai</option>
              <option value = "Salem"
                 <?php if ((isset($studentDetails['city']) && $studentDetails['city'] == "Salem") ||
                   (isset($data['city']) && $data['city'] == "Salem")) {
                           echo "selected";
                       } ?>>Salem</option>
             <option value = "Hosur"
                 <?php if ((isset($studentDetails['city']) && $studentDetails['city'] == "Hosur") ||
                 (isset($data['city']) && $data['city'] == "Hosur")) {
                      echo "selected";
                       } ?>>Hosur</option>
            </select>
            <button class = "edit" id = "edit-city"><i class="fa fa-edit"></i></button>
            <button class = "update" id = "update-city">UPDATE</i></button>
            <span> <?php if (isset($error['city'])) {echo $error['city'];} ?></span>
         </div>

     </div>
     </fieldset>


     <form method = "POST">
     <fieldset class = "hobbies-content">
        <h1>HOBBIES</h1><hr>
        <div class = "sub-content">
          <input type = "checkbox" id = "cricket" name = hobbies[cricket]
          value = "Cricket" <?php if (isset($hobby['Cricket'])) {echo "checked";} ?>>
          <label for = "cricket" class = "radio-button">Cricket</label>
        </div>
        <div class = "sub-content">
          <input type = "checkbox" id = "volleyball" name = hobbies[volleyball]
          value = "Volleyball" <?php if (isset($hobby['Volleyball'])) {echo "checked";} ?>>
          <label for = "Volleyball" class = "radio-button">Volleyball</label>
        </div>
        <div class = "sub-content">
          <input type = "checkbox" id = "basketball" name = hobbies[basketball]
          value = "Basketball" <?php if (isset($hobby['Basketball'])) {echo "checked";} ?>>
          <label for = "Basketball" class = "radio-button">Basketball</label>
        </div>
        <div class = "sub-content">
          <input type = "checkbox" id = "Shuttle" name = hobbies[shuttle]
          value = "Shuttle" <?php if (isset($hobby['Shuttle'])) {echo "checked";} ?>>
          <label for = "shuttle" class = "radio-button">Shuttle</label>
        </div>
        <div class = "sub-content">
          <input type = "checkbox" id = "hockey" name = hobbies[hockey]
          value = "Hockey" <?php if (isset($hobby['Hockey'])) {echo "checked";} ?>>
          <label for = "Hockey" class = "radio-button">Hockey</label>
        </div>
        <div class = "submit-form">
          <input type = "submit" id = "hobbies-submit" name = 'hobbiesSubmit' value = "Submit">
        </div>
     </fieldset>
     </form>
</body>
</html>
<script type="text/javascript">
$(document).ready(function(){

  $("#edit-firstName").click(function(){
       $('#firstName').prop("disabled", false);
  });

  $("#update-firstName").click(function(){
    $('#firstName').prop("disabled", true);

    name = $("#firstName").val();
    $.ajax({
        url: 'query/edit',
        type: 'POST',
        data: {
            'firstName': name,
        },
        success: function (response){
            alert(response);
        },
        error: function (response) {
           alert("error");
        }


    });
  });

});
</script>
