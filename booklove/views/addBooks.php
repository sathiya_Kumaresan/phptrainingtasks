<html>
<head>
  <title>ADDBOOKS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
  <h1 class = "heading">ADD BOOKS OR UPDATE BOOKS</h1>
  <form action = "/admin/addbooks" method = "POST">
  <fieldset class = "container">
   <div class = "main-content">
    <div class = "sub-content">
      <label class = "label-title" for = "bookName">BOOK NAME</label>
      <input type = "text" name ="books[bookName]" placeholder="BOOKNAME" required/>
    </div>
    <div class = "sub-content">
      <label class = "label-title" for = "author">AUTHOR</label>
      <input type = "text" name ="books[author]" placeholder="AUTHOR" required/>
    </div>
    <div class = "sub-content">
        <label class = "label-title" for = "category">CATEGORY</label>
         <select name = "books[category]" class = "select-category">
           <option value = "--selectcategory--">--selectcategory--</option>
          <?php foreach ($category as $key=>$value) { ?>
               <option value = "<?php echo $category[$key]['category_name']; ?>"><?php echo $category[$key]['category_name']; ?></option>
        <?php   } ?>
         </select>
    </div>
    <div class = "sub-content">
      <label class = "label-title" for = "quantity">QUANTITY</label>
      <input type = "number" name ="books[quantity]" placeholder="QUANTITY" required/>
    </div>
    <div class = "sub-content">
      <label class = "label-title" for = "price">PRICE</label>
      <input type = "number" name ="books[price]" placeholder="PRICE" required/>
    </div>
    <div class = "submit-form">
      <input type = "submit" name ="submit" value = "ADD / UPDATE"/>
    </div>
  </div>
  </fieldset>
  </form>

  <form action = "/admin/addbooks" method = "POST">
     <fieldset class = "container">
     <div class = "main-content">
       <div class = "sub-content">
         <label class = "label-title" for = "category">CATEGORY</label>
         <input type = "text" name ="category" placeholder="Add Category" required/>
       </div>
       <div class = "submit-form">
         <input type = "submit" name ="add" value = "ADD"/>
       </div>
     </div>
    </fieldset>
  </form>
</body>
</html>
