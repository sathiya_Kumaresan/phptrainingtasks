<html>
 <head>
   <title>VIEW BOOK DETAILS</title>
   <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
   <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
   <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
 </head>
 <body>
   <h1 class = "heading"> BOOK DETAILS  </h1>
   <div>
   <table class = "books" id = "book-content">
     <thead>
     <tr>
       <th>BOOK ID</th>
       <th>BOOK NAME</th>
       <th>AUTHOR</th>
       <th>PRICE</th>
       <th>QUANTITY</th>
     </tr>
   </thead>
   <tbody>
     <?php
     try {
          foreach ($data as $key => $value) {
     ?>
           <tr>
           <td><?php echo $data[$key]['book_id']; ?></td>
           <td><?php echo $data[$key]['book_name']; ?></td>
           <td><?php echo $data[$key]['author']; ?></td>
           <td><?php echo $data[$key]['price']; ?></td>
           <td><?php echo $data[$key]['quantity']; ?></td>
         </tr>
     <?php
         }
     } catch (Exception $e) {
         echo $e;
     }
     ?>
   </tbody>
   </table>
 </div>
 </body>
</html>
<script>
$(document).ready(function() {
    $("#book-content").DataTable();
});
</script>
