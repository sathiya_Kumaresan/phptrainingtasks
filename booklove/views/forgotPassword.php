<html>
<head>
  <title>RESETPASSWORD</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
     <h1 class = "heading">RESET PASSWORD</h1>
       <form action = "/authentication/forgotPassword" method = "POST">
         <fieldset class = "container">
         <div class = "main-content">
        <div class = "sub-content">
          <label class = "label-title" for = "email">EMAIL</label>
        </div>
        <div class = "sub-content">
           <input type = "email" name = "email" placeholder="Enter Your Email" required>
        </div>
         <div class = "submit-form">
           <input type = "submit" name = "submit">
         </div>
         <div>
           <span><?php if (isset($error)) {echo $error; }?></span>
         </div>
       </div>
       <?php
         if (isset($_GET['reset']) == "success") {
             echo '<span> Check your Email! To reset your Password</span>';
         }
        ?>
     </fieldset>
       </form>
</body>
</html>
