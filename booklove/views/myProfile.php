<html>
<head>
  <title>VIEW PROFILE</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
  <h1 class = "heading">VIEW DETAILS</h1>
  <div class = "profile-image">
   <img src = "<?php echo '../'.$row['image_name'] ?>">
  </div>
  <table class = "profile">
    <tr>
      <td>ID</td>
      <td><?php if (isset($row['id'])) {echo $row['id']; }?></td>
    </tr>
    <tr>
      <td>FIRST NAME</td>
      <td><?php if (isset($row['first_name'])) {echo $row['first_name']; }?></td>
    </tr>
    <tr>
      <td>LAST NAME</td>
      <td><?php if (isset($row['last_name'])) {echo $row['last_name']; }?></td>
    </tr>
    <tr>
      <td>DATE OF BIRTH</td>
      <td><?php if (isset($row['birth_date'])) {echo $row['birth_date']; }?></td>
    </tr>
    <tr>
      <td>AGE</td>
      <td><?php if (isset($row['age'])) {echo $row['age']; }?></td>
    </tr>
    <tr>
      <td>PHONE</td>
      <td><?php if (isset($row['phone'])) {echo $row['phone']; }?></td>
    </tr>
    <tr>
      <td>GENDER</td>
      <td><?php if (isset($row['gender'])) {echo $row['gender']; }?></td>
    </tr>
    <tr>
      <td>MAIL</td>
      <td><?php if (isset($row['email'])) {echo $row['email']; }?></td>
    </tr>
    <tr>
      <td>CITY</td>
      <td><?php if (isset($row['city'])) {echo $row['city']; }?></td>
    </tr>
    <tr>
      <td>HOBBIES</td>
      <td><?php if (isset($value)) {echo $value;} ?></td>
    </tr>
  </table>
</body>
</html>
