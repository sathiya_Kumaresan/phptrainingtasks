<html>
   <head>
     <title>My orders</title>
     <meta name="viewport" content="width=device-width, initial-scale=1.0" />
     <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
     <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
   </head>
 <body>
   <h1 class = "heading"> ORDER DETAILS  </h1>

   <table class = "order-details" id = "book-orders">
     <thead>
     <tr>
       <th>BOOK NAME</th>
       <th>NUMBER OF BOOKS</th>
       <th>COST PER BOOK</th>
       <th>TOTAL COST</th>
     </tr>
   </thead>
   <tbody>
     <?php
         foreach ($data as $key=> $value) {
               echo "<tr>",
               "<td>",$data[$key]['book_name'],"</td>",
               "<td>",$data[$key]['count'],"</td>",
               "<td>",$data[$key]['price'],"</td>",
               "<td>",$data[$key]['total_cost'],"</td></tr>";
           }
    ?>
  </tbody>
   </table>
 </body>
</html>
<script>
$(document).ready(function() {
    $("#book-orders").DataTable();
});
</script>
