<html>
<head>
   <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <link rel = "stylesheet" type = "text/css" href = "/stylesheet.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <div class = "navigation-list">
      <a class = "nav-list" href = "/dashboard/dashboard"><i class="fa fa-download"></i> DASHBOARD</a>
      <a class = "nav-list" href = "/dashboard/bookFile"><i class="fa fa-download"></i> REPORT</a>
      <a class = "nav-list" href = "/dashboard/summary"><i class="fa fa-book"></i> SUMMARY</a>
      <a class = "nav-list" href = "/dashboard/revenue"><i class="fa fa-rupee"></i>  REVENUE</a>
      <a class = "nav-list" href = "/admin/showUser"><i class="fa fa-close"></i> QUIT DASHBOARD</a>
      <a class = "nav-list" href = "/authentication/logout"><i class="fa fa-close"></i>LOG OUT</a>
  </div>
</body>
</html>
