<html>
 <head>
   <title>View Details</title>
   <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <link rel = "stylesheet" type = "text/css" href = "booklove/../../stylesheet.css">
   <script src='https://kit.fontawesome.com/a076d05399.js'></script>
   <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
   <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
   <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">

 </head>
 <body>
   <h1 class = "heading"> STUDENT DETAILS </h1>
   <div class = "main-content">
   <table class = "user-details" id = "user-profile-contents">
     <thead>
     <tr>
       <th>ID</th>
       <th>FIRST NAME</th>
       <th>LAST NAME</th>
       <th>DATE OF BIRTH</th>
       <th>AGE</th>
       <th>PHONE</th>
       <th>GENDER</th>
       <th>EMAIL</th>
       <th>CITY</th>
       <th>HOBBIES</th>
       <th>Activae/Deactivate</th>
     </tr>
   </thead>
   <tbody>
     <?php
       foreach ($data as $key => $value) {
           $id = $data[$key]['id'];
           echo
           "<tr><td>",$data[$key]['id'],
           "</td><td>",$data[$key]['first_name'],
           "</td><td>",$data[$key]['last_name'],
           "</td><td>",$data[$key]['birth_date'],
           "</td><td>",$data[$key]['age'],
           "</td><td>",$data[$key]['phone'],
           "</td><td>",$data[$key]['gender'],
           "</td><td>",$data[$key]['email'],
           "</td><td>",$data[$key]['city'],
           "</td><td>",$hobby[$key],"</td>";
           if ($data[$key]['status'] == 0 ){
             echo "<td><button class = 'button-activation' value='$id'> Activate </button></td></tr>";
           } else {
             echo "<td><button class = 'button-deactivation' value='$id'>Deactivate</button></td></tr>";
           }

       }
    ?>
  </tbody>
   </table>
 </div>
 </body>
</html>
<script type="text/javascript">
$(document).ready(function() {
    $(".button-activation").on('click', function() {
       var id = $(this).val();
        $.confirm({
        boxWidth: '20%',
        title: 'Confirm!',
        useBootstrap: false,
        content: 'Activate User',
        buttons: {
            confirm: function () {
                $.ajax({
                    url: '/admin/activateUser/id',
                    type: 'GET',
                    data: {
                        'id': id,
                    },
                    success: function (response){
                        $.alert({
                            boxWidth: '20%',
                            title: 'success!',
                            content: 'Account Activated!',
                            useBootstrap: false,
                        });
                        location.reload(true);
                    }
                });
             },

             cancel: function () {
                 $.alert({
                     boxWidth: '20%',
                     title: 'Failed!',
                     content: 'Cancelled!',
                     useBootstrap: false
                 });
             }
        }
        });
    });

    $(".button-deactivation").on('click', function() {
       var id = $(this).val();
        $.confirm({
        boxWidth: '20%',
        title: 'Confirm!',
        useBootstrap: false,
        content: 'Deactivate User',
        buttons: {
            confirm: function () {
                $.ajax({
                    url: '/admin/delete/id',
                    type: 'GET',
                    data: {
                        'id': id,
                    },
                    success: function (response){
                        $.alert({
                            boxWidth: '20%',
                            title: 'success!',
                            content: 'Account Deactivated !',
                            useBootstrap: false,
                        });
                        location.reload(true);
                    }
                });
             },

             cancel: function () {
                 $.alert({
                     boxWidth: '20%',
                     title: 'Failed!',
                     content: 'Cancelled!',
                     useBootstrap: false
                 });
             }
        }
        });
    });
});
$(document).ready(function() {
    $("#user-profile-contents").DataTable();
});

</script>
