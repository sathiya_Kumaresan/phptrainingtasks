<html>
<head>
   <title>LOGIN PAGE</title>
   <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 </head>
 <body>
   <div class = "login-image">
   <img src = "../library.jpg" class = "login-image">
   </div>
   <div class = "login-form">
   <h1 class = "heading"> LOGIN </h1>
   <form action = "../authentication/login" method = "POST">
   <fieldset class = "login-container">
      <div class = "main-content">
          <div class = "sub-content">
             <label class = "label-title" for = "email">EMAIL</label>
             <input type = "text" name = "email" placeholder = "Your mail">
          </div>
          <div class = "sub-content">
             <label class = "label-title" for = "password">PASSWORD  </label>
             <input type = "password" name = "password" placeholder = "Your password">
          </div>
          <div class = "submit-form">
             <input type = "submit" name = "submit" value = "LOGIN">
          </div>
           <span> <?php if (isset($error)) { echo $error;} ?></span>
      </div>
         <div class = "forgot-password">
           <a href = '../authentication/forgotPassword'>Forgot Password??</a>
         </div>
   </fieldset>
   </form>
   </div>
 </body>
</html>
