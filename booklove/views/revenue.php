<html>
<head>
  <title>BUYER DETAILS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
</head>
<body>
  <div class = "revenue-submit">
    <h1 class = "heading"> REVENUE PER BOOK </h1>
    <form method = "POST" action = "/dashboard/revenue">
        <div class = "submit-form">
            <input type = 'submit' name = 'submit' value = "DOWNLOAD CSV"/>
        </div>
    </form>

  </div>

   <table class = "dashboard-table" id = "ordered-books" width = "50%">
     <thead>
     <tr>
       <th>BOOK_ID</th>
       <th>BOOK NAME</th>
       <th>QUANTITY</th>
       <th>COST PER BOOK</th>
       <th>AMOUNT</th>
     </tr>
   </thead>
   <tbody>
     <?php
       foreach ($result as $key=>$value) {
           echo "<tr>",
           "<td>",$result[$key]['book_id'],"</td>",
           "<td>",$result[$key]['book_name'],"</td>",
           "<td>",$result[$key]['quantity'],"</td>",
           "<td>",$result[$key]['price'],"</td>",
           "<td>",$result[$key]['amount'],"</td></tr>";
       }
     ?>
   </tbody>
   </table>
</body>
<h1 class = "heading">TOTAL REVENUE: <?php echo $totalRevenue; ?></h1>
<script>
$(document).ready(function() {
    $("#ordered-books").DataTable();
});
</script>

</html>
