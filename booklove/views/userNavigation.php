<html>
<head>
   <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <link rel = "stylesheet" type = "text/css" href = "/stylesheet.css">
</head>
<body>
<div class = 'navigation' id = "navigation">
  <a href="javascript:void(0);" class="icon" onclick="responsiveNav()">
    <i class="fa fa-bars"></i>
  </a>
  <a href = "/authentication/logout">LOGOUT</a>
  <a href = "/user/myOrders">MY ORDERS</a>
  <a href = "/user/buyBooks">BOOKS</a>
  <a href = "/user/editProfile">UPDATE PROFILE</a>
  <a href = "/user/myProfile">MY PROFILE</a>
  <p id = "welcome">WELCOME <?php if (isset($_SESSION['name'])) {echo strtoupper($_SESSION['name']);} ?></p>
</div>
</body>
</html>

<script type="text/javascript">
    function responsiveNav() {
        var nav = document.getElementById("navigation");
        if (nav.className === "navigation") {
           nav.className += " responsive";
        } else {
           nav.className = "navigation";
        }
    }
</script>
