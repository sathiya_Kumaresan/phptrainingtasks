<html>
<head>
  <title>RESETPASSWORD</title>
</head>
<body>
       <?php
         if (isset($_GET['selector']))
             if (empty($_GET['selector'])) {
                echo "Could not validate your request";
             } else {
                if (ctype_xdigit($_GET['selector']) !== false) {
        ?>
        <h1 class = "heading">RESET PASSWORD</h1>
        <div class = "main-content">
            <form action = "/authentication/resetPassword" method = "POST">
            <fieldset class = "container">
            <input type = "hidden" name = "selector" value = "<?php if (isset($_GET['selector'])) {echo $_GET['selector']; } ?>">
            <div class = "sub-content">
                <label class = "label-title" for = "password">PASSWORD</label>
            </div>
            <div class = "sub-content">
                <input type = "password" placeholder="Reset Password" name = "password" required>
            </div>
            <div class = "sub-content">
                <label class = "label-title" for = "repassword">REPEAT PASSWORD</label>
            </div>
            <div class = "sub-content">
                <input type = "password" placeholder="Repeat Password" name = "repassword" required>
            </div>
            <div>
               <span><?php if (isset($_GET['error'])) { echo $_GET['error'];} ?></span>
            </div>
            <div class = "submit-form">
               <input type = "submit" name = "submit" value = "RESET">
            </div>
            <div class = "login-link">
               <a href = "/authentication/login"> Already have an account? Login</a>
            </div>
            </fieldset>
            </form>
      </div>
        <?php
            }
          }
         ?>
</body>
</html>
