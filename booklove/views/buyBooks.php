<html>
 <head>
   <title>BUY BOOKS</title>

   <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <script src='https://kit.fontawesome.com/a076d05399.js'></script>
   <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
   <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
   <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

 </head>
 <body>

   <h1 class = "heading"> BOOK DETAILS  </h1>
      <table id = "books" class = "display">
      <thead>
      <tr>
        <th>BOOK ID</th>
        <th>BOOK NAME</th>
        <th>AUTHOR</th>
        <th>CATEGORY NAME</th>
        <th>PRICE</th>
        <th>BUY BOOK</th>
      </tr>
    </thead>
        <tbody>
       <?php
            foreach ($data as $key=> $value) {
               $id = $data[$key]['book_id']; ?>
               <tr>
                 <td><?php echo $data[$key]['book_id']; ?></td>
                 <td><?php echo $data[$key]['book_name']; ?></td>
                 <td><?php echo $data[$key]['author']; ?></td>
                 <td><?php echo $data[$key]['category_name']; ?></td>
                 <td><?php echo $data[$key]['price']; ?></td>
                 <?php if ($data[$key]['quantity'] > 0) { ?>
                    <td><button class = 'buy-button' value = <?php echo $data[$key]['book_id']; ?> onClick='setId(this.value)'><i class='fas fa-cart-plus'></i></button></td>
                 <?php } else { ?>
                    <td><button class = 'stock'>NOT IN STOCK</a></button></td>
                 <?php } ?>
               </tr><?php } ?>
             </tbody>
    </table>
    <script>
    function setId(value) {
       buttonid = value;
    }

    $(document).ready(function() {
        var dataTable = $("#books").DataTable();
        $(document).on("click", ".buy-button", function() {
            $.confirm({
            boxWidth: '20%',
            title: 'Confirm!',
            useBootstrap: false,
            content: 'Place Order',
            buttons: {
                confirm: function () {
                    $.ajax({
                        url: '/user/buyBooks/id/',
                        type: 'GET',
                        data: {
                            'id': buttonid,
                        },
                        success: function (response){
                            $.alert({
                                boxWidth: '20%',
                                title: 'success!',
                                content: 'Order Confirmed!',
                                useBootstrap: false,
                            });
                        }
                    });
                 },

                 cancel: function () {
                     $.alert({
                         boxWidth: '20%',
                         title: 'Failed!',
                         content: 'Order Cancelled!',
                         useBootstrap: false
                     });
                 }
            }
            });
        });
    });
    </script>
</body>

</html>
