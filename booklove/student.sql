-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: localhost    Database: student
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES ('sathiyakumaresan128@gmail.com','827ccb0eea8a706c4c34a16891f84e7b');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_name` varchar(50) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `category_id` int(1) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`book_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `books_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES (1,'Core python programming','R. Nageswara Rao',1,0,50.00),(2,'Data Structures and Algorithms','Narasimha Karumanchi ',1,22,70.00),(3,'The Lean Startup ','Eric Ries',1,16,80.00),(4,'Java - The Complete Reference','Herbert Schildt',1,7,45.00),(5,'C in Depth  ','S.K.Srivastava/Deepali Srivastava',1,2,100.00),(6,'Excel 2019 All-in-One','Lokesh Lalwani',1,0,70.00),(7,'Java - The Complete Reference','Lokesh Lalwani',1,20,90.00),(8,'.NET','Shivprasad Koirala',1,12,60.00),(9,'Machine Learning using python','U Dinesh Kumar Manaranjan Pradhan',1,11,90.00),(10,'BRIGHT DEAD THINGS','ADA LIMON',2,12,40.00),(11,'The History of Tom Jones, a Foundling ','Henry Fielding',3,1,45.00),(12,'War and Peace ','Tolstoy',3,5,65.00),(13,'General Knowledge','Prabhat Prakashan',4,4,50.00),(15,'Mysql','satiya',1,90,45.00),(16,'jkjj','nmnvn',2,21,87.00),(17,'asn','sma,m',3,21,45.00),(18,'jkghj','hkjkj',2,1,123.00),(19,'Design pattern','Kanaga',1,0,45.00),(20,'Design pattern','satiya',1,22,45.00),(21,'I too had a love story','Ravinder Sighn',3,21,45.00),(22,'Mysql','satiya',1,90,45.00),(23,'Objective General Knowledge.','Ajay',4,2,65.00),(29,'A Complete History of American Comic Books','Shirrel Rhoades',6,0,45.00),(31,'A Little Princess','Frances Hodgson Burnett',6,2,45.00),(32,'Alice in Wonderland',' Lewis Carroll',3,1,45.00),(33,'In Search of Lost Time','Marcel Proust',3,5,45.00),(34,'A Little Princess 2','Shirrel Rhoades',3,5,45.00),(35,'Harry Potter','J K Rowling',9,13,78.00),(36,'Beauty and the Beast','Gabrielle-Suzanne de Villeneuve',9,1,80.00);
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Computer Programming'),(2,'Poems'),(3,'Novels'),(4,'General Knowledge'),(5,'others'),(6,'Comic book'),(7,'Historical Frictions'),(8,'Comics'),(9,'story books');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hobbies`
--

DROP TABLE IF EXISTS `hobbies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hobbies` (
  `hobbies_id` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) DEFAULT NULL,
  `hobbies` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`hobbies_id`),
  KEY `id` (`id`),
  CONSTRAINT `hobbies_ibfk_1` FOREIGN KEY (`id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hobbies`
--

LOCK TABLES `hobbies` WRITE;
/*!40000 ALTER TABLE `hobbies` DISABLE KEYS */;
INSERT INTO `hobbies` VALUES (110,210,'Volleyball'),(111,210,'Shuttle'),(166,209,'Basketball'),(167,209,'Shuttle');
/*!40000 ALTER TABLE `hobbies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) DEFAULT NULL,
  `image_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`image_id`),
  UNIQUE KEY `id` (`id`),
  CONSTRAINT `images_ibfk_1` FOREIGN KEY (`id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (2,209,'uploads/209.jpeg'),(3,238,'uploads/238.png');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reset_password`
--

DROP TABLE IF EXISTS `reset_password`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reset_password` (
  `pswdId` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(250) NOT NULL,
  `password_selector` text NOT NULL,
  `password_expires` text NOT NULL,
  PRIMARY KEY (`pswdId`),
  KEY `email` (`email`),
  CONSTRAINT `reset_password_ibfk_1` FOREIGN KEY (`email`) REFERENCES `users` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reset_password`
--

LOCK TABLES `reset_password` WRITE;
/*!40000 ALTER TABLE `reset_password` DISABLE KEYS */;
INSERT INTO `reset_password` VALUES (31,'dineshkumar.19cs@kct.ac.in','39e6525e9ef95621','1598784638');
/*!40000 ALTER TABLE `reset_password` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales`
--

DROP TABLE IF EXISTS `sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `book_id` int(11) DEFAULT NULL,
  `date_of_purchase` datetime DEFAULT NULL,
  `price` decimal(6,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `book_id` (`book_id`),
  CONSTRAINT `sales_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `books` (`book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales`
--

LOCK TABLES `sales` WRITE;
/*!40000 ALTER TABLE `sales` DISABLE KEYS */;
INSERT INTO `sales` VALUES (1,'candy@gmail.com',1,'2020-02-26 14:47:18',50.00),(2,'candy@gmail.com',5,'2020-02-26 14:47:20',100.00),(3,'candy@gmail.com',2,'2020-02-26 15:04:21',70.00),(4,'candy@gmail.com',6,'2020-02-26 15:04:26',70.00),(5,'candy@gmail.com',1,'2020-02-26 15:05:39',50.00),(6,'candy@gmail.com',5,'2020-02-26 15:06:48',100.00),(7,'candy@gmail.com',4,'2020-02-26 15:09:16',45.00),(8,'candy@gmail.com',2,'2020-02-26 15:14:47',70.00),(9,'varshni@gmail.com',3,'2020-02-26 15:15:24',80.00),(10,'varshni@gmail.com',4,'2020-02-26 15:15:26',45.00),(11,'varshni@gmail.com',2,'2020-02-26 15:16:36',70.00),(12,'varshni@gmail.com',2,'2020-02-26 15:16:42',70.00),(13,'varshni@gmail.com',5,'2020-02-27 09:55:33',100.00),(14,'varshni@gmail.com',1,'2020-02-27 14:23:20',50.00),(15,'varshni@gmail.com',1,'2020-02-27 14:23:30',50.00),(16,'varshni@gmail.com',3,'2020-02-27 14:25:10',80.00),(17,'varshni@gmail.com',9,'2020-02-28 09:21:00',90.00),(18,'ruban@gmail.com',1,'2020-03-06 14:43:11',50.00),(19,'babu@gmail.com',1,'2020-03-26 17:07:21',50.00),(20,'babu@gmail.com',1,'2020-03-26 17:07:23',50.00),(21,'babu@gmail.com',1,'2020-03-26 17:07:28',50.00),(22,'bala@gmail.com',1,'2020-03-28 17:58:41',50.00),(23,'bala@gmail.com',1,'2020-03-28 17:59:12',50.00),(24,'bala@gmail.com',5,'2020-03-28 17:59:17',100.00),(25,'bala@gmail.com',1,'2020-03-28 18:45:15',50.00),(26,'bala@gmail.com',11,'2020-03-28 22:02:05',45.00),(27,'sath@gmail.com',1,'2020-03-31 19:50:30',50.00),(28,'bala@gmail.com',1,'2020-04-01 22:35:35',50.00),(29,'bala@gmail.com',1,'2020-04-01 22:35:39',50.00),(30,'bala@gmail.com',5,'2020-04-01 22:36:53',100.00),(31,'bala@gmail.com',3,'2020-04-01 22:37:25',80.00),(32,'bala@gmail.com',1,'2020-04-02 16:33:57',50.00),(33,'bala@gmail.com',4,'2020-04-02 16:40:56',45.00),(34,'dineshkumar.19cs@kct.ac.in',1,'2020-04-09 00:43:07',50.00),(35,'dineshkumar.19cs@kct.ac.in',4,'2020-04-09 00:43:19',45.00),(36,'babu@gmail.com',1,'2020-04-09 00:47:11',50.00),(37,'bala@gmail.com',5,'2020-04-15 01:49:23',100.00),(38,'bala@gmail.com',21,'2020-04-15 01:49:30',45.00),(39,'bala@gmail.com',22,'2020-04-15 01:49:38',65.00),(40,'dineshkumar.19cs@kct.ac.in',1,'2020-04-15 01:50:29',50.00),(41,'dineshkumar.19cs@kct.ac.in',22,'2020-04-15 01:50:48',65.00),(42,'dineshkumar.19cs@kct.ac.in',5,'2020-04-15 01:51:07',100.00),(43,'dineshkumar.19cs@kct.ac.in',3,'2020-04-15 01:51:11',80.00),(44,'dineshkumar.19cs@kct.ac.in',19,'2020-04-15 01:51:17',45.00),(45,'bala@gmail.com',1,'2020-04-16 20:05:20',50.00),(46,'bala@gmail.com',22,'2020-04-16 20:05:54',65.00),(47,'bala@gmail.com',20,'2020-04-16 20:06:04',45.00),(48,'bala@gmail.com',5,'2020-04-16 20:06:31',100.00),(49,'dineshkumar.19cs@kct.ac.in',2,'2020-04-18 14:14:32',70.00),(50,'bala@gmail.com',3,'2020-04-22 18:03:51',80.00),(51,'bala@gmail.com',4,'2020-04-22 18:05:03',45.00),(52,'dineshkumar.19cs@kct.ac.in',2,'2020-04-26 23:50:29',70.00),(53,'dineshkumar.19cs@kct.ac.in',5,'2020-04-26 23:51:15',100.00),(54,'dineshkumar.19cs@kct.ac.in',29,'2020-04-26 23:51:32',45.00),(55,'reshma@gmail.com',2,'2020-04-27 01:22:23',70.00),(56,'dineshkumar.19cs@kct.ac.in',11,'2020-04-27 19:24:06',45.00),(57,'dineshkumar.19cs@kct.ac.in',13,'2020-05-03 19:24:45',50.00),(58,'dineshkumar.19cs@kct.ac.in',5,'2020-06-17 19:02:53',100.00),(59,'dineshkumar.19cs@kct.ac.in',2,'2020-08-28 23:50:31',70.00),(60,'dineshkumar.19cs@kct.ac.in',2,'2020-08-30 01:04:15',70.00),(61,'dineshkumar.19cs@kct.ac.in',29,'2020-08-30 01:04:25',45.00);
/*!40000 ALTER TABLE `sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `city` enum('Chennai','Coimbatore','Hosur','Salem') DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `image_name` varchar(50) DEFAULT 'uploads/default.png',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'sathiya','Kumaresan','1998-12-08',21,'9887766544','female','sathiya.16it@kct.ac.in','Hosur','827ccb0eea8a706c4c34a16891f84e7b','uploads/2.jpeg'),(9,'kishore','Kumaresan','1998-12-08',21,'9887766544','female','kishore@gmail.com','Hosur','1234','uploads/2.jpeg'),(15,'sayeesha','Kumaresan','1998-12-08',21,'9887766544','female','sayeesha@gmail.com','Hosur','1234','uploads/2.jpeg'),(209,'Dinesh','Kumaresan','1998-04-01',22,'9632587410','male','dineshkumar.19cs@kct.ac.in','Chennai','827ccb0eea8a706c4c34a16891f84e7b','uploads/209.jpeg'),(210,'Bala','kishore','2020-04-17',21,'9632587419','male','bala@gmail.com','Chennai',NULL,'uploads/default.png '),(230,'Ramya','Ravi',NULL,NULL,'9632587234',NULL,'ramya@gmail.com',NULL,'81dc9bdb52d04dc20036dbd8313ed055','uploads/230.jpeg '),(237,'Ramani','Ranbeer',NULL,NULL,'9632587410',NULL,'ramani@gmail.com',NULL,'827ccb0eea8a706c4c34a16891f84e7b','uploads/default.png '),(238,'sandhiya','venkat','1998-04-23',22,'9632587410','female','sandhiya@gmail.com','Coimbatore','827ccb0eea8a706c4c34a16891f84e7b','uploads/238.jpeg'),(239,'sakshi','santhosh','2020-04-10',21,'9632587410','male','sakshi@gmail.com','Chennai','827ccb0eea8a706c4c34a16891f84e7b','uploads/239.jpeg');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-30 23:40:15
