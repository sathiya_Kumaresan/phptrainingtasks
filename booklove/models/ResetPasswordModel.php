<?php
require_once 'booklove/../connection.php';

class ResetPasswordModel
{
    private $connect;
    private $selector;
    private $password;
    private $rePassword;

    public function __construct()
    {
      $db = models\Database::getConnect();
      $this->connect = $db->getConnection();
    }

    public function setSelector($selector) {
       $this->selector = $selector;
    }

    public function setPassword($password) {
       $this->password = $password;
    }

    public function setConfirmPassword($rePassword) {
      $this->rePassword = $rePassword;
    }

    public function getSelector()
    {
        return $this->selector;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getConfirmPassword()
    {
        return $this->confirmPassword;
    }

    public function validate()
    {
        if (empty($this->password) || empty($this->rePassword)) {
            $error = "Password cannot be Empty";

            return $error;
        } else if ($this->password != $this->rePassword) {
            $error = "Password Doesn't Match";

            return $error;
        }
    }

    public function resetPassword()
    {
         $currentDate = date("U");
         $sql = "select * from reset_password where password_selector = '$this->selector' and password_expires >= '$currentDate'";
         $result = mysqli_query($this->connect, $sql);
         $check = mysqli_fetch_assoc($result);

         if (empty($check)) {
             return "Something Went Wrong Please Request For Reset Password";
         } else {
                 $updateEmail = $check['email'];
                 $sql = "select * from users where email = '$updateEmail'";
                 if (!mysqli_query($this->connect, $sql)) {
                     return "You Need to signin";
                 } else {
                     $password = md5($this->password);
                     $sql = "update users set password = '$password' where email = '$updateEmail'";
                     mysqli_query($this->connect, $sql);

                     return "Password Updated";
                 }
        }
     }
}
