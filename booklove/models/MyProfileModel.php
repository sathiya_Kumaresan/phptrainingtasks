<?php
require_once 'booklove/../connection.php';

class MyProfileModel
{
    public $value;

    public function __construct()
    {
        $db = models\Database::getConnect();
        $this->connect = $db->getConnection();
    }

    public function getId()
    {
        if (isset($_SESSION['email'])) {
            $email = $_SESSION['email'];
        }

        $sql = "select * from users where email = '$email'";
        $result = mysqli_query($this->connect, $sql);
        $data = mysqli_fetch_assoc($result);
        $id = $data['id'];

        return $id;
    }

    public function getStudentDetails($id)
    {
        $sql = "select * from users where id = $id";
        $result = mysqli_query($this->connect, $sql);
        $student = mysqli_fetch_assoc($result);

        return $student;
    }

    public function getHobbies($id)
    {
        $sql = "select * from hobbies where id = $id";
        $result = mysqli_query($this->connect, $sql);
        while ($data = mysqli_fetch_assoc($result)) {
            $this->value .= $data['hobbies'].", ";
        }
        $this->value = rtrim($this->value, ', ');

        return $this->value;
    }
}
