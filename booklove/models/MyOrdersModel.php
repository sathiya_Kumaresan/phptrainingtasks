<?php
require_once 'booklove/../connection.php';

class MyOrdersModel
{
    public $connect;
    public $data;

    public function __construct()
    {
        $db = models\Database::getConnect();
        $this->connect = $db->getConnection();
    }

    public function getData()
    {
        $index = 0;
        $email = $_SESSION['email'];
        $result = mysqli_query($this->connect, "select book_name, count(book_name) as count,
                  sales.price, count(book_name) * sales.price as total_cost
                  from books join sales on books.book_id = sales.book_id
                  where email = '$email' group by book_name, sales.price");
        while ($value = mysqli_fetch_assoc($result)) {
          foreach($value as $key=>$value) {
               $this->data[$index][$key] = $value;
          }
          $index++;
        }

        return $this->data;
     }
}
