<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require_once 'booklove/../connection.php';

class ForgotPasswordModel
{
    public $connect;

    public function __construct()
    {
        $db = models\Database::getConnect();
        $this->connect = $db->getConnection();
    }

    public function isUser()
    {
        $email = $_POST['email'];
        $sql = "select * from users where email = '$email'";
        $result = mysqli_query($this->connect, $sql);
        $userCount = mysqli_num_rows($result);

        return $userCount;
    }

    public function getLink()
    {
        $email = $_POST['email'];
        $selector = bin2hex(random_bytes(8));

        $url = "booklove.com/authentication/resetPassword?selector=".$selector;
        $expires = date("U") + 1800;

        $sql = "delete from reset_password where email = '$email'";
        $result = mysqli_query($this->connect, $sql);

        $sql = "insert into reset_password (email, password_selector, password_expires) values ('$email', '$selector', '$expires')";
        $result = mysqli_query($this->connect, $sql);

        ForgotPasswordModel::sendMail($url, $email);
     }

     static public function sendMail($url, $email)
     {

        require_once "vendor/autoload.php";

        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->Host = "smtp.gmail.com";
        $mail->SMTPAuth = true;
        $mail->Username = "sathiyakumaresan128@gmail.com";
        $mail->Password = "Saranghae@123";
        $mail->SMTPSecure = "tls";
        $mail->Port = 587;

        $mail->From = "sathiyakumaresan128@gmail.com";
        $mail->FromName = "sathiya kumaresan";

        $mail->addAddress($email);

        $mail->isHTML(true);

        $mail->Subject = "Link to change your password";
        $mail->Body = "<i><p>You have requested to reset your password. Here is a link to reset your password</p><a href = '$url'>click here</a></i>";

        if(!$mail->send())
        {
            echo "Mailer Error: " . $mail->ErrorInfo;
        }
        else
        {
            header ("location: /authentication/forgotPassword?reset=success");
        }
     }

}
