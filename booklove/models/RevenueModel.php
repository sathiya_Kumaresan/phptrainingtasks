<?php
require_once 'booklove/../connection.php';

class RevenueModel
{
    public $connect;

    public function __construct()
    {
      $db = models\Database::getConnect();
      $this->connect = $db->getConnection();
    }

    public function getResult()
    {
        $index = 0;
        $result = mysqli_query($this->connect, "select sales.book_id, book_name, count(sales.book_id) as quantity,
                  books.price as price, count(sales.book_id)*books.price as amount from sales join books
                  on sales.book_id = books.book_id group by sales.book_id");

        while ($value = mysqli_fetch_assoc($result)) {
          foreach($value as $key=>$value) {
               $data[$index][$key] = $value;
          }
          $index++;
        }

        return $data;
    }


    public function getTotalRevenue()
    {
         $index = 0;
         $query = mysqli_query($this->connect, "select sum(price) as price from sales");
         $value = mysqli_fetch_assoc($query);

         return $value['price'];
    }

    public function getFile()
    {
        $output = fopen("php://output", "w");
        header('content-Type: text/csv; charset=utf-8');
        header('content-Disposition: attachment; filename=data.csv');
        ob_end_clean();
        fputcsv($output, array('Book Id', 'Book Name', 'Quantity', 'Cost Per Book', 'Amount'));
        $sql = mysqli_query($this->connect, "select sales.book_id, book_name, count(sales.book_id) as quantity,
                  books.price as price, count(sales.book_id)*books.price as amount from sales join books
                  on sales.book_id = books.book_id group by sales.book_id");
        while ($value = mysqli_fetch_assoc($sql) ) {
          fputcsv($output, $value);
        }
        $query = mysqli_query($this->connect, "select sum(price) as price from sales");
        $value = mysqli_fetch_assoc($query);
        fputcsv($output, array('Total Revenue', $value['price']));
        exit();
    }
}
