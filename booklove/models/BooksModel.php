<?php
require_once 'booklove/../connection.php';

class BooksModel
{
    public $connect;

    public function __construct()
    {
      $db = models\Database::getConnect();
      $this->connect = $db->getConnection();
    }

    public function getCategory()
    {
        try {
            $index = 0;
            $result = mysqli_query($this->connect, "select * from category");
            if (!$result) {
                error_log("Error Fetching Category");
                throw new Exception("Error getting the category values");
            }
            while ($value = mysqli_fetch_assoc($result)) {
              foreach($value as $key=>$value) {
                   $data[$index][$key] = $value;
              }
              $index++;
            }

            return $data;
        } catch (Exception $e) {
            error_log("[".date("F j,Y,g:i")."]  ".$e->getMessage()."\n", 3, "models/error.php");
        }
    }

    public function addCategory()
    {
       try {
         $categoryName = $_POST['category'];
         $query = mysqli_query($this->connect, "select * from category where category_name = '$categoryName'");
         $result = mysqli_fetch_assoc($query);
         if (empty($result)) {
             $sql = mysqli_query($this->connect, "insert into category(categorys_name) values ('$categoryName')");
             if ($sql) {
                return "Category added Successfully";
             } else {
                error_log("Error Adding Category");
                throw new Exception("Error Adding");
             }
         } else {
             return "Category Already Exists";
         }


       } catch(Exception $e) {
           echo $e;
       }
    }

    public function addBooks()
    {
          $books = $_POST['books'];
          $bookName = $books['bookName'];
          $author = $books['author'];
          $quantity = $books['quantity'];
          $category = $books['category'];
          $price = $books['price'];

          $category = mysqli_query($this->connect, "select category_id from
                      category where category_name = '$category'");
          $result = mysqli_fetch_assoc($category);
          $category_id = $result['category_id'];

          $compare = "select * from books where book_name = '$bookName' and author = '$author' and category_id = $category_id";
          $result = mysqli_query($this->connect, $compare);
          $value = mysqli_fetch_assoc($result);
          $numofrows = mysqli_num_rows($result);

          if (mysqli_num_rows($result) > 0) {
              $updateQuantity = $value['quantity'] + $quantity;
              $sql = "update books set quantity = $updateQuantity, price = '$price'
                      where book_name = '$bookName' and author = '$author' and category_id = $category_id";
              if (mysqli_query($this->connect, $sql)) {

                  return "Updated";
              } else {

                  return "Cannot update the books";
              }
          } else {
              $sql = " insert into books(book_name, author, category_id, quantity, price) values
                       ('$bookName', '$author', $category_id, $quantity, $price)";
              if (mysqli_query($this->connect, $sql)){

                   return "Book Added";
              }
           }
    }

}
