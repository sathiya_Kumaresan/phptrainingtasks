<?php
require_once 'booklove/../connection.php';

class BuyBooksModel
{
    public $connect;
    public $data;

    public function __construct()
    {
        $db = models\Database::getConnect();
        $this->connect = $db->getConnection();
    }

    public function getResult()
    {
        try {
            $index = 0;
            $result = mysqli_query($this->connect, "select book_id, book_name, author, category_name, quantity, price
                             from books join category on books.category_id = category.category_id;");
            while ($value = mysqli_fetch_assoc($result)) {
              foreach($value as $key=>$value) {
                   $this->data[$index][$key] = $value;
              }
              $index++;
            }

            return $this->data;
        } catch (Exception $e) {
            echo $e;
        }
    }

    public function placeOrder($id)
    {
         $userEmail = $_SESSION['email'];
         $query = mysqli_query($this->connect, "update books set quantity = quantity-1 where book_id = $id");
         if ($query) {
            $sql = mysqli_query($this->connect, "select price from books where book_id = $id");
            $row = mysqli_fetch_assoc($sql);
            $price = $row['price'];
            $order = mysqli_query($this->connect, "insert into sales
            (email, book_id, date_of_purchase, price) values ('$userEmail', '$id', now(), $price)");

            if ($order) {
                return "Order Placed";
            } else {
                return "Error Ordering Try Again";
            }
         }
    }

    public function getQuantity($id) {
       $query = mysqli_query($this->connect, "select quantity from books where book_id = $id");
       if ($query) {
           $value = mysqli_fetch_assoc($query);
           $quantity = $value['quantity'];
           return $quantity;
       }

    }

    public function getCategoryId($searchText)
    {
        $query = mysqli_query($this->connect, "select * from category where category_name like '%$searchText%'");
        $value = mysqli_fetch_assoc($query);
        $categoryId = $value['category_id'];

        return $categoryId;
    }

    public function getBookByCategory($categoryId) {
      $index = 0;
        $result = mysqli_query($this->connect, "select * from books where category_id = $categoryId");
        $count = mysqli_num_rows($result);
        if ($count > 0 ){
          while ($value = mysqli_fetch_assoc($result)) {
            foreach($value as $key=>$value) {
                 $this->data[$index][$key] = $value;
            }
            $index++;
          }

          return json_encode($this->data);
        }
    }

}
