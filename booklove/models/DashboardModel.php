<?php
require_once 'booklove/../connection.php';

class DashboardModel
{
    public $connect;

    public function __construct()
    {
        $db = models\Database::getConnect();
        $this->connect = $db->getConnection();
    }

    public function getTopSoldBooks()
    {
        $index = 0;
        $result = mysqli_query($this->connect, "select book_name, count(sales.book_id) as count from sales join books
         on books.book_id = sales.book_id group by sales.book_id order by count(sales.book_id) desc limit 5");
        while ($value = mysqli_fetch_assoc($result)) {
          foreach($value as $key=>$value) {
               $data[$index][$key] = $value;
          }
          $index++;
        }

        return $data;
    }

    public function getTopUser()
    {
        $index = 0;
        $result = mysqli_query($this->connect, "select email, count(book_id) as count from sales group by email order by count(book_id) desc limit 5");
        while ($value = mysqli_fetch_assoc($result)) {
          foreach($value as $key=>$value) {
               $data[$index][$key] = $value;
          }
          $index++;
        }

        return $data;
    }
}
