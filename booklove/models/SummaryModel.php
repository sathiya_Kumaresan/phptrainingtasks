<?php
require_once 'booklove/../connection.php';

class SummaryModel
{
    public $connect;

    public function __construct()
    {
        $db = models\Database::getConnect();
        $this->connect = $db->getConnection();
    }

    public function getSummaryBooks($startDate, $endDate)
    {
        $output = fopen("php://output", "w");
        header('content-Type: text/csv; charset=utf-8');
        header('content-Disposition: attachment; filename=data.csv');
        ob_end_clean();
        fputcsv($output, array('Id', 'Email', 'Book Id', 'Book Name', 'Author', 'Price', 'Date of Purchase'));
        $sql = mysqli_query($this->connect, "select id, email, sales.book_id, book_name,
                            author, sales.price, date_of_purchase
                            from sales join books on books.book_id = sales.book_id
                            where date_of_purchase between '$startDate' and  '$endDate'");
        while ($value = mysqli_fetch_assoc($sql) ) {
          fputcsv($output, $value);
        }
        exit();
    }
}
