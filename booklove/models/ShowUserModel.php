<?php
require_once 'booklove/../connection.php';

class ShowUserModel
{
     public $connect;
     public $value;
     public $hobby;

     public function __construct()
     {
         $db = models\Database::getConnect();
         $this->connect = $db->getConnection();
     }

     public function getStudentDetails()
     {
         try {
             $index = 0;
             $result = mysqli_query($this->connect, "select * from users");
             while ($value = mysqli_fetch_assoc($result)) {
               foreach($value as $key=>$value) {
                    $data[$index][$key] = $value;
               }
               $index++;
             }

             return $data;
         } catch (Exception $e) {
             echo $e;
         }
     }

     public function getHobbies($data)
     {
          $index = 0;
          foreach ($data as $key=>$value) {
             $id = $data[$key]['id'];
             $sql = "select * from hobbies where id = $id";
             $result = mysqli_query($this->connect, $sql);
             while ($hobbyData = mysqli_fetch_assoc($result)) {
               $this->value .= $hobbyData['hobbies'].",";
             }
             $this->hobby[$index++] = rtrim($this->value, ', ');
             $this->value = '';
          }

          return $this->hobby;
     }

     public function activateUser($id) {
         try {
             $sql = "update users set status = 1 where id = $id";   //making active
             if (mysqli_query($this->connect, $sql)) {

                   echo "Account Activated";
             } else {

                   echo "Error Activating";
             }
        } catch (Exception $e) {
            echo $e;
        }
     }

     public function deleteUser($id)
     {
         try {
             $sql = "update users set status = 0 where id = $id";   //making inactive
             if (mysqli_query($this->connect, $sql)) {

                   return "Record Deleted";
             } else {

                   return "Error Deleting";
             }
        } catch (Exception $e) {
            echo $e;
        }

     }
}
