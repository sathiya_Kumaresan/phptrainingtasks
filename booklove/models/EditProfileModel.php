<?php
namespace models;
require_once 'booklove/../connection.php';

class EditProfileModel
{
     private $error;
     private $firstName;
     private $lastName;
     private $birthDate;
     private $age;
     private $phone;
     private $gender;
     private $email;
     private $city;
     public $connect;
     public $data;

     public function __construct() {
       $db = Database::getConnect();
       $this->connect = $db->getConnection();
     }

     public function getFirstName()
     {
         return $this->firstName;
     }

     public function getLastName()
     {
          return $this->lastName;
     }

     public function getBirthDate()
     {
          return $this->birthDate;
     }

     public function getAge()
     {
          return $this->age;
     }

     public function getPhone()
     {
          return $this->phone;
     }

     public function getGender()
     {
          return $this->gender;
     }

     public function getEmail()
     {
          return $this->email;
     }

     public function getCity()
     {
          return $this->city;
     }

     public function setFirstName($firstName)
     {
         $this->firstName = $firstName;
     }

     public function setLastName($lastName)
     {
          $this->lastName = $lastName;
     }

     public function setBirthDate($birthDate)
     {
          $this->birthDate = $birthDate;
     }

     public function setAge($age)
     {
          $this->age = $age;
     }

     public function setPhone($phone)
     {
          $this->phone = $phone;
     }

     public function setGender($gender)
     {
          $this->gender = $gender;
     }

     public function setEmail($email)
     {
          $this->email = $email;
     }

     public function setCity($city)
     {
          $this->city = $city;
     }

     public function validateFirstName() {
       if (empty($this->firstName)){
           $this->error['firstName'] = "First name cannot be empty";
       } else {
           if (!preg_match('/^[a-z]*$/i', $this->firstName)) {
           $this->error['firstName'] = "First name cannot contain numerics or special characters";
           }
       }

       echo $this->error;
     }

     public function updateFirstName() {

       $userEmail = $_SESSION['email'];
       $sql = "select * from users where email = '$userEmail'";
       $result = mysqli_query($this->connect, $sql);
       $data = mysqli_fetch_assoc($result);
       $row = mysqli_num_rows($result);

       if ($row > 0) {
           if (isset($this->firstName)) {
              $update = "update users set first_name = '$this->firstName' where email = '$userEmail'";
              if (mysqli_query($this->connect, $update)) {
                  $message = true;
              } else {
                  $message = false;
              }
           }
       }

        return $message;
     }

     public function validateForm() {

         if (empty($this->firstName)){
             $this->error['firstName'] = "First name cannot be empty";
         } else {
             if (!preg_match('/^[a-z]*$/i', $this->firstName)) {
             $this->error['firstName'] = "First name cannot contain numerics or special characters";
             }
         }

         if (empty($this->lastName)) {
             $this->error['lastName'] = "Last name cannot be empty";
         } else {
             if (!preg_match('/^[a-z \s]*$/i', $this->lastName)) {
                 $this->error['lastName'] = "Last name cannot contain numerics or special characters";
             }
         }

         if (empty($this->phone)) {
             $this->error['phone'] = "Phone Number cannot be empty";
         } else {
             if (!preg_match('/^[9,8,7,6][0-9]{9}$/', $this->phone)) {
                 $this->error['phone'] = "Invalid phone number";
             }
         }

         if ($this->age < 0) {
             $this->error['age'] = "Age cannot be negative and cannot be greater than 25";
         }

         if ($this->city == "--selectcity--") {
             $this->error['city'] = "Must select a city";
         }

         return $this->error;
    }

    public function updateStudent() {

           $userEmail = $_SESSION['email'];
           $sql = "select * from users where email = '$userEmail'";
           $result = mysqli_query($this->connect, $sql);
           $data = mysqli_fetch_assoc($result);
           $row = mysqli_num_rows($result);

           if ($row > 0) {
               if (isset($this->firstName)) {
                  $update = "update users set first_name = '$this->firstName' where email = '$userEmail'";
                  if (mysqli_query($this->connect, $update)) {
                      $message = true;
                  } else {
                      $message = false;
                  }
               }
               if (isset($this->lastName)) {
                  $update = "update users set last_name = '$this->lastName' where email = '$userEmail'";
                  if (mysqli_query($this->connect, $update)) {
                      $message = true;
                  } else {
                      $message = false;
                  }
               }
               if (isset($this->birthDate)) {
                  $update = "update users set birth_date = '$this->birthDate' where email = '$userEmail'";
                  if (mysqli_query($this->connect, $update)) {
                      $message = true;
                  } else {
                      $message = false;
                  }
               }
               if (isset($this->phone)) {
                  $update = "update users set phone = '$this->phone' where email = '$userEmail'";
                  if (mysqli_query($this->connect, $update)) {
                      $message = true;
                  } else {
                      $message = false;
                  }
               }
               if (isset($this->age)) {
                  $update = "update users set age = '$this->age' where email = '$userEmail'";
                  if (mysqli_query($this->connect, $update)) {
                      $message = true;
                  } else {
                      $message = false;
                  }
               }
               if (isset($this->gender)) {
                  $update = "update users set gender = '$this->gender' where email = '$userEmail'";
                  if (mysqli_query($this->connect, $update)) {
                      $message = true;
                  } else {
                      $message = false;
                  }
               }
               if (isset($this->firstName)) {
                  $update = "update users set city = '$this->city' where email = '$userEmail'";
                  if (mysqli_query($this->connect, $update)) {
                      $message = true;
                  } else {
                      $message = false;
                  }
               }

               return $message;
           }

    }

    public function getData()
    {
        $email = $_SESSION['email'];
        $sql = "select * from users where email = '$email'";
        $query = mysqli_query($this->connect, $sql);
        $data = mysqli_fetch_assoc($query);

        return $data;
    }

    public function uploadProfile($id)
    {
        $id = $id['id'];
        $targetDirectory = "uploads/";
        $extension = basename($_FILES["fileToUpload"]["name"]);
        $fileType = strtolower(pathinfo($extension, PATHINFO_EXTENSION));
        $targetFile = $targetDirectory . $id.".".$fileType;
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);

        if ($check !== false) {
            $upload = 1;
        } else {
            $upload = 0;

            return  'File is not Image type.';
        }

        if ($_FILES["fileToUpload"]["size"] > 500000) {
            $upload = 0;

             return 'Sorry, your file is too large.';
        }

        if ($fileType != "jpg" && $fileType != "png" && $fileType != "jpeg") {
            $upload = 0;

            return 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.';
        }

        if ($upload == 0) {
            $error = "Upload Error";
            echo $error;
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $targetFile)) {
                $sql = "update users set image_name = '$targetFile' where id = $id";
                mysqli_query($this->connect, $sql);

                return 'File Uploaded successfully';

            } else {

                return 'Error Uploading';
            }
        }

    }

    public function getHobbies($id)
    {
        try {
            $result = mysqli_query($this->connect, "select hobbies from hobbies where id = $id");
            while ($value = mysqli_fetch_assoc($result)) {
              foreach($value as $key=>$value) {
                   $this->data[$value] = $value;
              }
            }

            return $this->data;
        } catch (Exception $e) {
            echo $e;
        }
    }

    public function addHobbies($id, $hobby)
    {
         if (isset($hobby)) {
             $sql = "delete from hobbies where id = $id";
             mysqli_query($this->connect, $sql);
             foreach ($hobby as $key => $value) {
                $sql = "insert into hobbies(id, hobbies) values ($id, '$value')";
                mysqli_query($this->connect, $sql);
             }

             return true;
         } else {
             return "Please select your hobbies";
         }
    }
}
