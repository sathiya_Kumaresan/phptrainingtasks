<?php

require_once 'booklove/../connection.php';

class SignupModel
{
    private $error;
    private $firstName;
    private $lastName;
    private $phone;
    private $email;
    private $password;
    private $confirmPassword;

    public function __construct()
    {
      $db = models\Database::getConnect();
      $this->connect = $db->getConnection();
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getPhone()
    {
         return $this->phone;
    }

    public function getEmail()
    {
         return $this->email;
    }

    public function getPassword()
    {
         return $this->password;
    }

    public function getConfirmPassword()
    {
         return $this->confirmPassword;
    }

    public function getError()
    {
         return $this->error;
    }

    public function setFirstName($firstName)
    {
         $this->firstName = $firstName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    public function setPhone($phone)
    {
         $this->phone = $phone;
    }

    public function setEmail($email)
    {
         $this->email = $email;
    }

    public function setPassword($password)
    {
         $this->password = $password;
    }

    public function setConfirmPassword($confirmPassword)
    {
         $this->confirmPassword = $confirmPassword;
    }

    public function setError($error)
    {
        $this->error = $error;
    }

    public function validateForm()
    {
          if (empty($this->firstName)){
              $this->error['firstName'] = "First name cannot be empty";
          } else {
              if (!preg_match('/^[a-z]*$/i', $this->firstName)) {
              $this->error['firstName'] = "First name cannot contain numerics or special characters";
              }
          }

          if (empty($this->lastName)) {
              $this->error['lastName'] = "Last name cannot be empty";
          } else {
              if (!preg_match('/^[a-z \s]*$/i', $this->lastName)) {
                  $this->error['lastName'] = "Last name cannot contain numerics or special characters";
              }
          }

          if (empty($this->email)) {
              $this->error['email'] = "Email cannot be empty";
          } else {
              if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
                  $this->error['email'] = "Invalid Email";
              }
          }

          if (empty($this->password)) {
              $this->error['password'] = "Please set your Password";
          }

          if (empty($this->confirmPassword)) {
              $this->error['confirmPassword'] = "Please repeat your Password";
          } else {
              if ($this->password != $this->confirmPassword) {
                 $this->error['confirmPassword'] = "Password Doesn't match";
              }
          }
      }

      public function checkUser()
      {
          $query = mysqli_query($this->connect, "select * from users where email = '$this->email'");
          $result = mysqli_num_rows($query);

          if ($result == 0) {
              return "true";
          } else {
              return "Existing User!! Try Logging In";
          }
      }

      public function addStudent()
      {
          $this->password = md5($this->password);
          $sql = "insert into users(first_name, last_name, email, password)
          values('$this->firstName', '$this->lastName', '$this->email', '$this->password')";
          if (mysqli_query($this->connect, $sql)) {
              return "Registered Successfully!! Try Logging In";
          } else {
              return "Connection Error";
          }
      }
}
