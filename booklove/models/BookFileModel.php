<?php
require_once 'booklove/../connection.php';

class BookFileModel
{
     public $connect;

     public function __construct()
     {
       $db = models\Database::getConnect();
       $this->connect = $db->getConnection();
     }

     public function getCsv()
     {
         try {
           $output = fopen("php://output", "w");
           if (!$output) {
              throw new Exception("Error File Opening");
           }
           header('content-Type: text/csv; charset=utf-8');
           header('content-Disposition: attachment; filename=data.csv');
           ob_end_clean();
           fputcsv($output, array('Book Id', 'Book Name', 'Author', 'Category', 'Quantity', 'Price'));
           $sql = mysqli_query($this->connect, 'select book_id, book_name, author, category_name, quantity,
           price from books join category on category.category_id = books.category_id');
           while ($value = mysqli_fetch_assoc($sql) ) {
             fputcsv($output, $value);
           }
           fclose($output);
           exit();
         } catch(Exception $e) {
              error_log("[".date("F j,Y,g:i")."]  ".$e->getMessage()."\n", 3, "booklove/../models/error.php");
              $error = "Some Internal Error! Try Again Later";
              return $error;
         }
     }

     public function getsoldBooks()
     {
       try {
         $output = fopen("php://output", "w");
         if (!$output) {
            throw new Exception("Error File Opening");
         }
         $output = fopen("php://output", "w");
         header('content-Type: text/csv; charset=utf-8');
         header('content-Disposition: attachment; filename=data.csv');
         ob_end_clean();
         fputcsv($output, array('Email', 'Book Name', 'Author', 'date_of_purchase', 'Price'));
         $sql = mysqli_query($this->connect, ' select email, book_name, author, date_of_purchase, sales.price from sales
         join books on sales.book_id = books.book_id');
         while ($value = mysqli_fetch_assoc($sql) ) {
           fputcsv($output, $value);
         }
         fclose($output);
         exit();
       } catch(Exception $e) {
            error_log("[".date("F j,Y,g:i")."]  ".$e->getMessage()."\n", 3, "booklove/../models/error.php");
            $error = "Some Internal Error! Try Again Later";
            return $error;
       }
     }

     public function bookSoldByCategory($category)
     {
       try {
         $output = fopen("php://output", "w");
         if (!$output) {
            throw new Exception("Error File Opening");
         }
         $output = fopen("php://output", "w");
         header('content-Type: text/csv; charset=utf-8');
         header('content-Disposition: attachment; filename=data.csv');
         ob_end_clean();
         fputcsv($output, array($category));
         fputcsv($output, array('Email', 'Book Name', 'Author', 'Date of Purchase', 'Price'));
         $sql = mysqli_query($this->connect, "select email, book_name, author, date_of_purchase, sales.price from
         sales join books on books.book_id = sales.book_id join category on category.category_id = books.category_id
         where category_name = '$category'");
         while ($value = mysqli_fetch_assoc($sql) ) {
           fputcsv($output, $value);
         }
         fclose($output);
         exit();
       } catch(Exception $e) {
            error_log("[".date("F j,Y,g:i")."]  ".$e->getMessage()."\n", 3, "booklove/../models/error.php");
            $error = "Some Internal Error! Try Again Later";
            return $error;
       }
     }

}
