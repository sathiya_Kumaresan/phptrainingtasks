<?php
require_once 'booklove/../autoload.php';

/**
 *
 */
interface LoginInterface
{
    public function autheticateUser();
}

class LoginModelAdaptor implements LoginInterface
{
     public $login;

     public function __construct()
     {
         $this->login = new LoginModel();
     }

     public function autheticateUser()
     {
         return $this->login->autheticateUser();
     }
}
