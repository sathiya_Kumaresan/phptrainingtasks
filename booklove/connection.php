<?php
namespace models;

class Database
{
    public $connection;
    public static $connect;

    public function __construct()
    {
        $this->connection = mysqli_connect('localhost', 'root', 'root', 'book_shop');
        if (!$this->connection) {
            error_log("Failed to connect to Database");
        }
    }

    static public function getConnect()
    {
        if (!Database::$connect) {
            Database::$connect = new Database();
        }

        return Database::$connect;
    }

    public function getConnection() {

        return $this->connection;
    }

}
