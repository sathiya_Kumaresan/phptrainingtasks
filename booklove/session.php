<?php
session_start();
if (!isset($_SESSION['email'])) {
    header('location: /authentication/login');
}

$time = $_SERVER['REQUEST_TIME'];
$timeout = 1800;

if (isset($_SESSION['LAST_ACTIVITY']) && ($time - $_SESSION['LAST_ACTIVITY']) > $timeout ) {
    session_unset();
    session_destroy();
    header('location: /views/login.php');
}
