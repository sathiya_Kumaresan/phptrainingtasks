<?php
require_once 'booklove/../connection.php';
use PHPUnit\Framework\TestCase;
$_SESSION['email'] = 'kishore@gmail.com';


class UserTest extends TestCase
{
    public $user;
    public $connect;

    protected function setUp(): void
    {
        $this->user = new models\EditProfileModel;
        $_FILES = array(
             "fileToUpload" =>array("name" => 'phpunittest/1.jpg', "tmp_name" => 'phpunittest/1.jpg', "size" => 499),
        );
    }

    public function testFirstName()
    {
        $this->user->setFirstName('Kumaresan');
        $this->assertEquals($this->user->getFirstName(), 'Kumaresan');
        $this->assertNotEquals($this->user->getFirstName(), null);
        $this->assertNotEquals($this->user->getFirstName(), 'dsds');
    }

    public function testLastName()
    {

        $this->user->setLastName('sathiya');
        $this->assertEquals($this->user->getLastName(), 'sathiya');
        $this->assertNotEquals($this->user->getLastName(), '');
        $this->assertNotEquals($this->user->getLastName(), 'saro');
    }

    public function testBirthDate()
    {

        $this->user->setBirthDate('12-08-1998');
        $this->assertEquals($this->user->getBirthDate(), '12-08-1998');
        $this->assertNotEquals($this->user->getBirthDate(), '555');
        $this->assertNotEquals($this->user->getBirthDate(), '');
    }

    public function testAge()
    {

        $this->user->setAge(21);
        $this->assertEquals($this->user->getAge(), 21);
        $this->assertNotEquals($this->user->getAge(), 22);
        $this->assertNotEquals($this->user->getAge(), null);
    }

    public function testPhone()
    {

        $this->user->setPhone(9922334455);
        $this->assertEquals($this->user->getPhone(), 9922334455);
        $this->assertNotEquals($this->user->getPhone(), '');
    }

    public function testGender()
    {

        $this->user->setGender('female');
        $this->assertEquals($this->user->getGender(), 'female');
        $this->assertNotEquals($this->user->getGender(), 'male');
    }

    public function testEmail()
    {

        $this->user->setEmail('sathiya@gmail.com');
        $this->assertEquals($this->user->getEmail(), 'sathiya@gmail.com');
        $this->assertNotEquals($this->user->getEmail(), 'sathiya.com');
    }

    public function testCity()
    {

        $this->user->setCity('Hosur');
        $this->assertEquals($this->user->getCity(), 'Hosur');
        $this->assertNotEquals($this->user->getCity(), '');
    }

    public function testValidate()
    {   $this->user->setFirstName('Sathiya');
        $this->user->setLastName('Kumaresan');
        $this->user->setPhone(9876543210);
        $this->user->setAge(12);
        $this->user->setCity('Hosur');
        $this->assertEquals($this->user->validateForm(), null);
        $this->assertNotEquals($this->user->validateForm(), ['name' => 'name cannot be empty']);
    }

    public function testUpdateStudent()
    {
          $this->user->setFirstName('Sathiya');
          $this->user->setLastName('Kumaresan');
          $this->user->setPhone(9876543210);
          $this->user->setAge(12);
          $this->user->setCity('Hosur');
          $this->assertEquals($this->user->validateForm(), null);
          $this->assertTrue($this->user->updateStudent());
    }

    public function testGetData()
    {
          $expectedArray = ['id' => '9',
                           'first_name' => 'Sathiya',
                            'last_name' => 'Kumaresan',
                            'birth_date' => '1998-12-08',
                            'age' => '12',
                            'phone' => '9876543210',
                            'gender' => 'female',
                            'email' => 'kishore@gmail.com',
                            'city' => 'Hosur',
                            'password' => '827ccb0eea8a706c4c34a16891f84e7b',
                            'image_name' => 'uploads/default.png',
                            'status' => '1'
                         ];
          $this->user->setFirstName('Sathiya');
          $this->user->setLastName('Kumaresan');
          $this->user->setPhone(9876543210);
          $this->user->setAge(12);
          $this->user->setCity('Hosur');
          $this->assertEquals($this->user->validateForm(), null);
          $this->assertEquals($this->user->getData(), $expectedArray);
    }

    public function testGetHobbies()
    {
         $this->assertEquals($this->user->getHobbies(9), ['Volleyball' => 'Volleyball', 'Shuttle' => 'Shuttle']);
         $this->assertArrayHasKey('Volleyball', $this->user->getHobbies(9), 'Not Equal');
    }

    public function testAddHobbies()
    {
         $input = ['Volleyball', 'Shuttle'];
         $this->assertTrue($this->user->addHobbies(9, $input));
    }

    public function testUploadProfileImage()
    {
         $this->assertEquals($this->user->uploadProfile(['id' => 9]), 'Error Uploading');
    }
}
