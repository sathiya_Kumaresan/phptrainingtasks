**Theme :** Book Ordering System

**Description :**
Simple system where user can login and can order books.

**Functionalities:**

**SIGNUP PAGE:**

1. User can signup by giving their details.
2. Each field is validated using regular expression.
3. If expected input is given they will be redirected to login page.

**LOGIN PAGE:**

1. if user enters the correct user name and password they will be redirected to the admin page or the user page according to the rights stored in the database.

Note: here I have stored only one user as admin

2. If the username or password is incorrect it will show an error message saying Enter the correct username or Password.

**FORGOT PASSWORD:**

1. Incase the user has forgotten the password they can reset it by clicking the forgot password.

    step 1: -should enter the email in this page.
            -user will be intimated that they would have received a mail.
            -if the mail is not a registered mail an alert message will be given saying that the mail is not registered

   step 2: -User can click the reset link in their mail to reset their password.

   step 3: -The token match check would be done and the password would be reset.

**ADMIN LOGIN:**

**USER DETAILS:**
      1. Admin can view the registered user details in this page and can delete the user.

**ADD/UPDATE BOOK:**
      1. can enter the details of the book such as book name, author name, category, Quantity and price
      2. Admin can also add category. if exixting category is added than the alert will be shown.

      Note: IF the book name, author name, category are already present it will update the quantity of the book alone
            ELSE new record would be created

**VIEW BOOKS:**
      1. Admin can view the books present in the database.

**DASHBOARD**
     **DASHBOARD** 
       1. Dashboard displays the top 5 sold books and the top 5 users who bought books.
     **REPORT** 
       1. Admin could download csv file for the following
           -Books
           -Books sold
           -Books sold by category(by selecting category)
      **SUMMARY** 
       1. Admin could download csv file as a summary from date to to date
      **REVENUE** 
       1. Admin could see the revenue details of each book sold and the total revenue.
       2. This could also be downloaded as a csv file.


**USER LOGIN:**

**MY PROFILE:**
    1. user can view thier details.
**UPDATE PROFILE:**
    1. Can edit thier profile details.
    2. Can upload their Profile picture.
    3. can select their hobbies.

**VIEW BOOKS:**
    1. They can view all the books.
    2. on clicking the ORDER, book will be ordered and a pop message will be shown.
    3. If the book is not in stock, user will be intimated by the red button "NOT IN STOCK"
    4. user can search by category using dropdown button.

**MYORDERS:**
    1. User can view the books the user has orderd with the price


**TECHNICAL SNIPPETS:**

1. Design pattern:
   -singleton patten (connection.php, controllers/Controller.php)
   -Adaptor design pattern models/(LoginModel, LoginModelAdaptor)
2. Mysql table design
   - created Entity Relationship Diagram using mysql workbench
     ER Diagram - ERD.png
3. Unit testing
   - unit-testing-book.com.xlsx

**STEPS TO RUN APPLICATION:**

**DATABASE SETTING:**

   [dump without data]
   1. create database "student".
   2. Dump the file "studentNodata.sql".

   [dump with data]
   1. create database student.
   2. Dump the file "student.sql".

**SERVER SETTING:**
   1. create a virtula host bookstore.com.conf /etc/apache2/sites-available. refer: virtualHost.png.
   2. Ensure your server allows file uploads - read, write. permissions to be checked.
   3. Ensure rewrite mode is enabled - if not enable it by [sudo a2enmod rewrite].

**RUN APPLICATION USING:**
   1. In the browser search for the url : bookstore.com.
   2. LOGIN CREDENTIALS FOR ADMIN:
         ADMIN LOGIN: sathiyakumaresan128@gmail.com
         PASSWORD: 12345

         USER LOGIN: dinesh.16cs@kct.ac.in
         PASSWORD: 12345
