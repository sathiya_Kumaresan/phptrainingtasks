<?php

class Index
{
   public $routes;

   function router()
   {
       $url = $_GET['url'];
       $url = rtrim($url, '/');
       $url = explode('/', $url);

       foreach ($url as $key => $value) {
           $this->routes[$key] = $value;
       }

       if (!isset($this->routes['0']) || !isset($this->routes['1'])) {
           $this->routes['0'] = 'authentication';
           $this->routes['1'] = 'login';
       }
       $folder = $this->routes['0'];
       if (isset($this->routes['1'])) {
           $method = $this->routes['1'];
           if(file_exists('controllers/'.$folder.'/Application.php')) {
               require_once ('controllers/'.$folder.'/Application.php');
               if (method_exists('Application', $method)) {
                  $app = new Application();
                  $app->$method();
               } else {
                  include_once 'error.php';
               }
           } else {
              include_once 'error.php';
           }
       }
   }
}

$route = new Index();
$route->router();
