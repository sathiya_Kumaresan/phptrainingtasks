<?php
include('registerValidate.php');
include('connection.php');
include('session.php');

$email = $_SESSION['email'];
$sql = "select * from signup where email = '$email'";
$query = mysqli_query($connection, $sql);
$data = mysqli_fetch_assoc($query);

$sql = "select * from student_details where email = '$email'";
$query = mysqli_query($connection, $sql);
$studentDetails = mysqli_fetch_assoc($query);

$result = $_POST['data'];
if (isset($result['submit'])) {
    $object = new validate($result);
    $error = $object->validateForm();
    if (empty($error)) {
      $object->addStudent();
    }
}

?>


<html>
     <head>
       <title> Form validation</title>
       <link rel = "stylesheet" type = "text/css" href = "stylesheet.css">
     </head>
     <body>
       <div class = 'navigation'>
         <a href = "logout.php">LOGOUT</a>
         <a href = "myOrders.php">MY ORDERS</a>
         <a href = "buyBooks.php">VIEW BOOKS</a>
         <a href = "hobbiesnew.php">HOBBIES</a>
         <a href = "registerUser.php">EDIT PROFILE</a>
         <a href = "viewProfile.php">MY PROFILE</a>
       </div>
       <h1> STUDENT PROFILE </h1>
       <form action = "registerUser.php" method = "POST">
       <fieldset>
        <div>
         <div>
         <label for = "firstName">FIRST NAME</label>
          <input type = "text" name = "data[firstName]" value = <?php echo $studentDetails['first_name']?>>
          <span> <?php echo $error['firstName']; ?></span>
         </div>

          <div>
          <label for = "lastName">LAST NAME</label>
          <input type = "text" name = "data[lastName]" value = <?php echo $studentDetails['last_name']?> >
          <span> <?php echo $error['lastName']; ?></span>
          </div>

          <div>
          <label for = "birthDate">DATE OF BIRTH</label>
          <input type = "date" name = "data[birthDate]" value = <?php echo $studentDetails['birth_date']?>>
          <span> <?php echo $error['birthDate']; ?></span>
          </div>

          <div>
          <label for = "age">AGE</label>
          <input type = "number" name = "data[age]" value = <?php echo $studentDetails['age']?>>
          <span> <?php echo $error['age']; ?></span>
         </div>

          <div>
          <label for = "phone">PHONE NUMBER</label>
          <input type = "phone" name = "data[phone]" value = <?php echo $studentDetails['phone']?>>
          <span> <?php echo $error['phone']; ?></span>
         </div>

          <div>
            <label for = "gender">GENDER</label>
            <div>
              <input type = "radio" name = "data[gender]" value = "female"
              <?php if ($studentDetails[gender] == "female") { echo "checked";
              } ?>>FEMALE
           </div>
           <div>
              <input type = "radio" name = "data[gender]" value = "male"
              <?php if ($studentDetails[gender] == "male") { echo "checked";
              } ?>>MALE
            </div>
              <span> <?php echo $error['gender']; ?></span>
         </div>

          <div>
          <label for = "email">EMAIL</label>
          <input type = "email" name = "data[email]" value = <?php echo $data['email']?> disabled>
          <span> <?php echo $error['email']; ?></span>
          </div>

         <div>
         <label for = "city">CITY</label>
          <select name = "data[city]">
            <option value = "--selectcity--">--selectcity--</option>
            <option value = "Coimbatore" <?php if ($studentDetails[city] == "Coimbatore") {echo "selected";
        } ?>>Coimbatore</option>
            <option value = "Chennai" <?php if ($studentDetails[city] == "Chennai") {echo "selected";
        } ?>>Chennai</option>
            <option value = "Salem" <?php if ($studentDetails[city] == "Salem") {echo "selected";
        } ?>>Salem</option>
            <option value = "Hosur" <?php if ($studentDetails[city] == "Hosur") {echo "selected";
        } ?>>Hosur</option>
          </select>
          <span> <?php echo $error['city']; ?></span>
         </div>

          <div>
          <input type = "submit" name = "data[submit]">
          </div>
        </div>
        </fieldset>
       </form>
     </body>
</html>
