<?php
include ('connection.php');
include ('session.php');

?>
<html>
 <head>
   <title>BUY BOOKS</title>
   <link rel = "stylesheet" type = "text/css" href = "stylesheet.css">
 </head>
 <body>
   <div class = 'navigation'>
     <a href = "logout.php">LOGOUT</a>
     <a href = "buyBooks.php">VIEW BOOKS</a>
     <a href = "hobbiesnew.php">HOBBIES</a>
     <a href = "registerUser.php">EDIT PROFILE</a>
     <a href = "viewProfile.php">MY PROFILE</a>
   </div>
   <div class = 'search'>
     <form action = "search.php" method = "POST">
     <input type = "text" placeholder="SEARCH BY BOOKNAME" name = 'searchText'>
     <input type = "submit" value = "search" name = "search">
   </form>
   </div>
   <h1> BOOK DETAILS  </h1>
   <table>
     <tr>
       <th>BOOK ID</th>
       <th>BOOK NAME</th>
       <th>AUTHOR</th>
       <th>BUY BOOK</th>
     </tr>
     <?php
     if (isset($_POST['searchByCategory'])) {
       $searchText = $_POST['category'];
       $query = mysqli_query($connection, "select * from category where category_name like '%$searchText%'");
       $value = mysqli_fetch_assoc($query);
       $categoryId = $value['category_id'];
       $query = mysqli_query($connection, "select * from books where category_id = $categoryId");
       $count = mysqli_num_rows($query);
       if ($count == 0) {
         echo "No search results";
       } else {
         while ($data = mysqli_fetch_assoc($query)) {
           echo "<tr>",
           "<td>",$data['book_id'],"</td>",
           "<td>",$data['book_name'],"</td>",
           "<td>",$data['author'],"</td>",
           "<td><a href='buy.php?id=$id'>Buy</a></td></tr>";
         }
       }
     }
      ?>
    </table>
    </body>
  </html>
