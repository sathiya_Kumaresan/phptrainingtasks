<?php
session_start();
include ('connection.php');
include ('session.php');
?>

 <html>
  <head>
    <title>view Details</title>
    <link rel = "stylesheet" type = "text/css" href = "stylesheet.css">
  </head>
  <body>
    <div class = 'navigation'>
      <a href = "logout.php">LOGOUT</a>
      <a href = "booksBought.php">PURCHASED</a>
      <a href = "viewBooks.php">VIEW BOOKS</a>
      <a href = "books.php">ADD/UPDATE BOOKS</a>
      <a href = "select.php">STUDENT DETAILS</a>
    </div>
    <h1> STUDENT DETAILS </h1>
    <table>
      <tr>
        <th>ID</th>
        <th>FIRST NAME</th>
        <th>LAST NAME</th>
        <th>DATE OF BIRTH</th>
        <th>AGE</th>
        <th>PHONE</th>
        <th>GENDER</th>
        <th>EMAIL</th>
        <th>CITY</th>
        <th>DELETE</th>
        <th>UPDATE</th>
      </tr>

      <?php
      try {
          $result = mysqli_query($connection, "select * from student_details");
          while ($data = mysqli_fetch_assoc($result)) {
            $id = $data['id'];
            echo "<tr><td>",
            $data['id'],"</td><td>",$data['first_name'],"</td><td>",$data['last_name'],"</td><td>",$data['birth_date'],"</td>",
            "<td>",$data['age'],"</td><td>",$data['phone'],"</td><td>",$data['gender'],"</td>",
            "<td>",$data['email'],"</td>",
            "<td>",$data['city'],"</td>",
            "<td><a href='delete.php?id=$id'>Delete</a></td>",
            "<td><a href='updateStudent.php?id=$id'>Update</a></td>",
            "</tr>";

          }

      } catch (Exception $e) {
          echo $e;
      }
?>
    </table>
  </body>
</html>
