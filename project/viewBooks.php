<?php
session_start();
include ('connection.php');
include ('session.php');

?>

 <html>
  <head>
    <title>VIEW BOOK DETAILS</title>
    <link rel = "stylesheet" type = "text/css" href = "stylesheet.css">
  </head>
  <body>
    <div class = 'navigation'>
      <a href = "logout.php">LOGOUT</a>
      <a href = "booksBought.php">PURCHASED</a>
      <a href = "viewBooks.php">VIEW BOOKS</a>
      <a href = "books.php">ADD/UPDATE BOOKS</a>
      <a href = "select.php">STUDENT DETAILS</a>
    </div>
    <h1> BOOK DETAILS  </h1>
    <table>
      <tr>
        <th>BOOK ID</th>
        <th>BOOK NAME</th>
        <th>AUTHOR</th>
        <th>PRICE</th>
        <th>QUANTITY</th>
      </tr>

      <?php
      try {

          $result = mysqli_query($connection, "select * from books ");
          //total number of records in the page
          $total_record = mysqli_num_rows($result);
          $result_per_page = 5;
          $number_of_page = ceil($total_record/$result_per_page);
          if (!isset($_GET['page'])) {
              $page = 1;
          } else {
              $page = $_GET['page'];
          }
          $starting_record = ($page - 1) * $result_per_page;
          $result = mysqli_query($connection, "select * from books limit $starting_record, $result_per_page");
          while ($data = mysqli_fetch_assoc($result)) {
            $id = $data['id'];
            echo "<tr>",
            "<td>",$data['book_id'],"</td>",
            "<td>",$data['book_name'],"</td>",
            "<td>",$data['author'],"</td>",
            "<td>",$data['price'],"</td>",
            "<td>",$data['quantity'],"</td>",
            "</tr>";

          }

      } catch (Exception $e) {
          echo $e;
      }
?>
    </table>
    <div class = 'pagination'>
    <?php
    for ($page = 1; $page <= $number_of_page; $page++) {
       echo "<a href = 'viewBooks.php?page=$page'>$page</a>";
    } ?>
    <div>
  </body>
</html>
