<?php
include ('connection.php');

class validate
{
    public $data;
    public $error;
    public $firstName;
    public $lastName;
    public $birthDate;
    public $age;
    public $phone;
    public $gender;
    public $email;
    public $city;

    public function __construct($data) {
        $this->data = $data;
        $this->firstName = trim($this->data['firstName']);
        $this->lastName = trim($this->data['lastName']);
        $this->birthDate = $this->data['birthDate'];
        $this->age = trim($this->data['age']);
        $this->phone = trim($this->data['phone']);
        $this->gender = $this->data['gender'];
        $this->email = $this->data['email'];
        $this->city = $this->data['city'];
    }

    public function validateForm() {

        if (empty($this->firstName)){
            $this->error['firstName'] = "First name cannot be empty";
        } else {
            if (!preg_match('/^[a-z]*$/i', $this->firstName)) {
            $this->error['firstName'] = "First name cannot contain numerics or special characters";
            }
        }

        if (empty($this->lastName)) {
            $this->error['lastName'] = "Last name cannot be empty";
        } else {
            if (!preg_match('/^[a-z]*$/i', $this->lastName)) {
                $this->error['lastName'] = "Last name cannot contain numerics or special characters";
            }
        }

        if (empty($this->phone)) {
            $this->error['phone'] = "Phone cannot be empty";
        } else {
            if (!preg_match('/^[9,8,7,6][0-9]{9}$/', $this->phone)) {
                $this->error['phone'] = "Invalid phone number";
            }
        }

        if (empty($this->birthDate)) {
            $this->error['birthDate'] = "Birthdate should be selected";
        }

        if (empty($this->age)) {
            $this->error['age'] = "Age cannot be empty";
        } else if ($this->age < 0) {
            $this->error['age'] = "Age cannot be negative and cannot be greater than 25";
        }


        if (empty($this->data['gender'])) {
           $this->error['gender'] = "Please select the gender";
        }

        if ($this->city == "--selectcity--") {
            $this->error['city'] = "Must select a city";
        }

        return $this->error;
  }

  public function addStudent() {
          include('connection.php');
          $userEmail = $_SESSION['email'];
          $sql = "select * from student_details where email = '$userEmail'";
          $result = mysqli_query($connection, $sql);
          $data = mysqli_fetch_assoc($result);
          $row = mysqli_num_rows($result);
          echo $row;
          if ($row > 0) {
              echo $this->birthDate;
              $update = "update student_details set first_name = '$this->firstName',
              last_name = '$this->lastName', phone = '$this->phone', birth_date = '$this->birthDate',
              age = '$this->age', gender = '$this->gender', city = '$this->city'
              where email = '$userEmail'";
              if (mysqli_query($connection, $update)) {
                echo "<script>alert('Updated!!');</script>";
                header('location: hobbiesnew.php');
              } else {
                echo "<script>alert('Error Updation!!');</script>";
              }
          } else {
            $sql = " insert into student_details(first_name, last_name, birth_date, age, phone,
                   gender, email, city) values('$this->firstName', '$this->lastName',
                   '$this->birthDate', '$this->age', '$this->phone', '$this->gender',
                   '$userEmail', '$this->city')";
            if (mysqli_query($connection, $sql)){
                header('location: hobbiesnew.php');
            } else{
                echo "<script>alert('Error!!');</script>";
            }
          }

  }
}
