<?php
session_start();
include ('connection.php');
$id = $_GET['id'];
        $result = mysqli_query($connection,"select * from student_details where id = $id");
        $data = mysqli_fetch_array($result);
?>

<html>
   <head>
     <title> update </title>
       <link rel = "stylesheet" type = "text/css" href = "stylesheet.css">
   </head>
   <body>
     <div class = 'navigation'>
       <a href = "logout.php">LOGOUT</a>
       <a href = "viewBooks.php">VIEW BOOKS</a>
       <a href = "books.php">ADD/UPDATE BOOKS</a>
       <a href = "select.php">STUDENT DETAILS</a>
     </div>
     <h1> UPDATE THE DETAILS </h1>
     <form name = "updateForm" action = "update.php" method = "POST" onsubmit = "updateValidation()">
       <fieldset>
     <div>
       <div>
       <label for = "firstName">FIRST NAME</label>
        <input type = "text" name = "updateData[firstName]" value = <?php echo $data[first_name] ?>>
      </div>

       <div>
        <label for = "lastName">LAST NAME</label>
        <input type = "text" name = "updateData[lastName]" value = <?php echo $data[last_name] ?>>
      </div>

       <div>
        <label for = "birthDate">DATE OF BIRTH</label>
        <input type = "date" name = "updateData[birthDate]" value = <?php echo $data[birth_date] ?>>
      </div>

      <div>
        <label for = "age">AGE</label>
        <input type = "number" name = "updateData[age]" value = <?php echo $data[age] ?>>
      </div>

      <div>
        <label for = "phone">PHONE NUMBER</label>
        <input type = "phone" name = "updateData[phone]" value = <?php echo $data[phone] ?>>
      </div>

      <div>
        <div><label for = "gender">GENDER</label></div>
        <div><input type = "radio" name = "updateData[gender]" value = "female"
        <?php if ($data[gender] == female) {
                   echo "checked";
        } ?>>  <label for = "female">FEMALE</label><br></div>
        <div><input type = "radio" name = "updateData[gender]" value = "male"
        <?php if ($data[gender] == "male") { echo "checked";
        } ?> >  <label for = "gender">MALE</label></div>
      </div>

       <div>
        <label for = "email">EMAIL</label>
        <input type = "email" name = "updateData[email]" value = <?php echo $data[email] ?>>
      </div>

      <div>
       <label for = "city">CITY</label>
        <select name = "updateData[city]">
          <option value = "--selectcity--"></option>
          <option value = "Coimbatore" <?php if ($data[city] == "Coimbatore") {echo "selected";
        } ?>>Coimbatore</option>
          <option value = "Chennai" <?php if ($data[city] == "Chennai") {echo "selected";
        } ?>>Chennai</option>
          <option value = "Salem" <?php if ($data[city] == "Salem") {echo "selected";
        } ?>>Salem</option>
          <option value = "Hosur" <?php if ($data[city] == "Hosur") {echo "selected";
        } ?>>Hosur</option>
      </select>
     </div>

     <div>
        <button name = "updateData[update]" value = <?php echo $id; ?>>UPDATE</button>
     </div>

   </div>
      </fieldset>
     </form>
   </body>
</html>
