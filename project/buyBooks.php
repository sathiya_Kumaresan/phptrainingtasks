<?php
include ('connection.php');
include ('session.php');
?>
 <html>
  <head>
    <title>BUY BOOKS</title>
    <link rel = "stylesheet" type = "text/css" href = "stylesheet.css">
  </head>
  <body>
    <div class = 'navigation'>
      <a href = "logout.php">LOGOUT</a>
      <a href = "myOrders.php">MY ORDERS</a>
      <a href = "buyBooks.php">VIEW BOOKS</a>
      <a href = "hobbiesnew.php">HOBBIES</a>
      <a href = "registerUser.php">EDIT PROFILE</a>
      <a href = "viewProfile.php">MY PROFILE</a>
    </div>

    <div class = "search">

      <form action = "search.php" method = "POST">
      <input type = "text" placeholder="SEARCH BOOK BY NAME" name = 'searchText'>
      <input type = "submit" value = "search"  name = "search">
      </form>

      <form action = "searchByCategory.php" method = "POST">
      <input type = "text" placeholder="SEARCH BOOK BY CATEGORY" name = 'category'>
      <input type = "submit" value = "search"  name = "searchByCategory">
      </form>
    </div>

    <h1> BOOK DETAILS  </h1>
    <table>
      <tr>
        <th>BOOK ID</th>
        <th>BOOK NAME</th>
        <th>AUTHOR</th>
        <th>Price</th>
        <th>BUY BOOK</th>
      </tr>
      <?php
      try {
          $result = mysqli_query($connection, "select * from books ");
          //total number of records in the page
          $total_record = mysqli_num_rows($result);
          $result_per_page = 5;
          $number_of_page = ceil($total_record/$result_per_page);
          if (!isset($_GET['page'])) {
              $page = 1;
          } else {
              $page = $_GET['page'];
          }

          $starting_record = ($page - 1) * $result_per_page;
          //limiting the query to select
          $query = mysqli_query($connection, "select * from books limit $starting_record, $result_per_page");
          while ($data = mysqli_fetch_assoc($query)) {
            $id = $data['book_id'];
            echo "<tr>",
            "<td>",$data['book_id'],"</td>",
            "<td>",$data['book_name'],"</td>",
            "<td>",$data['author'],"</td>",
            "<td>",$data['price'],"</td>",
            "<td><a href='buy.php?id=$id'>Buy</a></td></tr>";
          }

      } catch (Exception $e) {
          echo $e;
      }
?>
    </table>
    <div class = 'pagination'>
    <?php
    for ($page = 1; $page <= $number_of_page; $page++) {
       echo "<a href = 'buyBooks.php?page=$page'>$page</a>";
    } ?>
    <div>
  </body>
</html>
