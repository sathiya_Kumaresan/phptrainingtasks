<?php
include ('connection.php');
include ('session.php');
$email = $_SESSION['email'];

$sql = mysqli_query($connection, "select * from student_details where email = '$email'");
$selectId = mysqli_fetch_assoc($sql);
$id = $selectId['id'];
$value = $_POST['hobbies'];

$i = 0;
$sql = "select hobbies from hobbies where id = $id";
$result = mysqli_query($connection, $sql);

while ($data = mysqli_fetch_assoc($result))  {
  $compare[$i++] = $data["hobbies"];
}

if (isset($_POST['submit'])) {
  if (empty($_POST['hobbies'])) {
     echo "<html><script>alert('you haven't checked anything');</script></html>";
  } else {
     if (!empty($compare)) {
       $intersect_array = array_intersect($value, $compare);
       $toInsert = array_diff($value, $compare);
       $toDelete = array_diff($compare, $value);

       if (empty($toInsert)) {
         echo "<script>alert('already Updated');</script>";
       } else {
           foreach ($toInsert as $hobby) {
              $sql = "insert into hobbies(id, hobbies) values ($id, '$hobby')";
              if (mysqli_query($connection, $sql)) {
                  echo "<script>alert('Updated'); windows.location('hobbiesnew.php');</script>";
              } else {
                  echo "error";
              }
           }
           if (isset($toDelete)) {
             foreach ($toDelete as $hobby) {
                $sql = "delete from hobbies where hobbies = '$hobby'";
                if (mysqli_query($connection, $sql)) {
                    echo "<script>alert('Updated');</script>";
                } else {
                    echo "error";
                }
             }
           }
       }
     } else {
        foreach ($value as $hobby) {
           $sql = "insert into hobbies(id, hobbies) values ($id, '$hobby')";
           if (mysqli_query($connection, $sql)) {
               echo "<script>alert('Updated');</script>";
           } else {
               echo "error";
           }
        }
     }

  }
}
?>

<html>
<head>
  <title>HOBBIES</title>
  <link rel = "stylesheet" type = "text/css" href = "stylesheet.css">
</head>
<body>
  <div class = 'navigation'>
    <a href = "logout.php">LOGOUT</a>
    <a href = "myOrders.php">MY ORDERS</a>
    <a href = "buyBooks.php">VIEW BOOKS</a>
    <a href = "hobbiesnew.php">HOBBIES</a>
    <a href = "registerUser.php">EDIT PROFILE</a>
    <a href = "viewProfile.php">MY PROFILE</a>
  </div>
  <h1>HOBBIES</h1>
  <form action = 'hobbiesnew.php' method = "POST">
  <fieldset>
      <div>
        <input type = "checkbox" id = "cricket" name = hobbies[cricket]
        value = "Cricket" <?php if(in_array("Cricket",$compare)) {echo "checked";} ?>>Cricket
      </div>
      <div>
        <input type = "checkbox" id = "volleyball" name = hobbies[volleyball]
         value = "Volleyball" <?php if(in_array("Volleyball",$compare)) {echo "checked";} ?>>Volleyball
      </div>
      <div>
        <input type = "checkbox" id = "basketball" name = hobbies[basketball]
         value = "Basketball" <?php if(in_array("Basketball",$compare)) {echo "checked";} ?>>Basketball
      </div>
      <div>
        <input type = "checkbox" id = "Shuttle" name = hobbies[shuttle]
         value = "Shuttle" <?php if(in_array("Shuttle",$compare)) {echo "checked";} ?>>Shuttle
      </div>
      <div>
        <input type = "checkbox" id = "hockey" name = hobbies[hockey]
         value = "Hockey" <?php if(in_array("Hockey",$compare)) {echo "checked";} ?>>Hockey
      </div>
      <div>
        <input type = "submit" id = "submit" name = 'submit' value = "Submit">
      </div>
    </fieldset>
  </form>
</body>
</html>
