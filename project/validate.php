<?php
session_start();
class validate
{
    public $data;
    public $error;
    public $firstName;
    public $lastName;
    public $phone;
    public $email;
    public $password;
    public $confirmPassword;
    public function __construct($data) {
        $this->data = $data;
        $this->firstName = trim($this->data['firstName']);
        $this->lastName = trim($this->data['lastName']);
        $this->phone = trim($this->data['phone']);
        $this->email = $this->data['email'];
        $this->password = $this->data['password'];
        $this->confirmPassword = $this->data['confirmPassword'];
    }

    public function validateForm() {
        if (empty($this->firstName)){
            $this->error['firstName'] = "First name cannot be empty";
        } else {
            if (!preg_match('/^[a-z]*$/i', $this->firstName)) {
            $this->error['firstName'] = "First name cannot contain numerics or special characters";
            }
        }

        if (empty($this->lastName)) {
            $this->error['lastName'] = "Last name cannot be empty";
        } else {
            if (!preg_match('/^[a-z]*$/i', $this->lastName)) {
                $this->error['lastName'] = "Last name cannot contain numerics or special characters";
            }
        }

        if (empty($this->phone)) {
            $this->error['phone'] = "Phone cannot be empty";
        } else {
            if (!preg_match('/^[9,8,7,6][0-9]{9}$/', $this->phone)) {
                $this->error['phone'] = "Invalid phone number";
            }
        }

        if (empty($this->email)) {
            $this->error['email'] = "Please fill the email";
        } else {
            if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
                $this->error['email'] = "Invalid Email";
            }
        }

      if (empty($this->password)) {
          $this->error['password'] = "Please set your Password";

      }

      if (empty($this->confirmPassword)) {
          $this->error['confirmPassword'] = "Please repeat your Password";
      } else {
          if ($this->password != $this->confirmPassword) {
            $this->error['confirmPassword'] = "Password Doesn't match";
          }
      }

      return $this->error;
  }

  public function addStudent() {
          include('connection.php');

          $this->password = md5($this->data['password']);
          $this->confirmPassword = md5($this->data['confirmPassword']);
          $sql = " insert into signup(phone, email, password) values
                 ('$this->phone', '$this->email', '$this->password')";
          if (mysqli_query($connection, $sql)){
            header('location: about.php');
          } else{
              return $this->error['email'] = "Email already Exists";
          }

          $sql = "insert into student_details(first_name, last_name, email)
          values('$this->firstName', '$this->lastName', '$this->email')";
          if (mysqli_query($connection, $sql)) {
            echo "inserted";
          } else {
            echo "error";
          }
          exit;
  }
}
