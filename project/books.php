<?php
include ('connection.php');
include ('session.php');

if (isset($_POST['submit'])) {
    $books = $_POST['books'];

    $bookName = $books['bookName'];
    $author = $books['author'];
    $quantity = $books['quantity'];
    $category = $books['category'];
    $price = $books['price'];
    
    $compare = "select * from books where book_name = '$bookName' and author = '$author'";
    $result = mysqli_query($connection, $compare);
    $value = mysqli_fetch_assoc($result);
    $numofrows = mysqli_num_rows($result);

    if (mysqli_num_rows($result) > 0) {
      $updateQuantity = $value['quantity'] + $quantity;
      $sql = "update books set quantity = $updateQuantity
      where book_name = '$bookName' and author = '$author'";
      if (mysqli_query($connection, $sql)) {
          echo "<script>alert('Quantity updated');</script>";
      } else {
          echo "<script>alert('Cannot update the books');</script>";
      }

    } else {
        $category = mysqli_query($connection, "select category_id from
        category where category_name = '$category'");
        $result = mysqli_fetch_assoc($category);
        $category_id = $result['category_id'];
        $sql = " insert into books(book_name, author, category_id, quantity, price) values
               ('$bookName', '$author', $category_id, $quantity, $price)";
        if (mysqli_query($connection, $sql)){
            echo "<script>alert('Book Added');</script>";
        }
    }


}
 ?>
 <html>
 <head>
   <title>ADDBOOKS</title>
   <link rel = "stylesheet" type = "text/css" href = "stylesheet.css">
 </head>
 <body>
   <div class = 'navigation'>
     <a href = "logout.php">LOGOUT</a>
     <a href = "booksBought.php">PURCHASED</a>
     <a href = "viewBooks.php">VIEW BOOKS</a>
     <a href = "books.php">ADD/UPDATE BOOKS</a>
     <a href = "select.php">STUDENT DETAILS</a>
   </div>
   <h1>ADD BOOKS OR UPDATE BOOKS</h1>
   <form action = "books.php" method = "POST">
     <fieldset>
    <div>
     <div>
       <input type = "text" name ="books[bookName]" placeholder="BOOKNAME" required/>
     </div>
     <div>
       <input type = "text" name ="books[author]" placeholder="AUTHOR" required/>
     </div>
     <div>
          <select name = "books[category]">
            <option value = "--selectcategory--">--selectcategory--</option>
            <option value = "Computer Programming" <?php if ($result[city] == "Computer Programming") {echo "selected";
        } ?>>Computer Programming</option>
            <option value = "Poems" <?php if ($result[city] == "Poems") {echo "selected";
        } ?>>Poems</option>
            <option value = "Novel" <?php if ($result[city] == "Novel") {echo "selected";
        } ?>>Novel</option>
            <option value = "Genreal Knowledge" <?php if ($result[city] == "Genreal Knowledge") {echo "selected";
        } ?>>Genreal Knowledge</option>
          </select>
     </div>
     <div>
       <input type = "number" name ="books[quantity]" placeholder="QUANTITY" required/>
     </div>
     <div>
       <input type = "number" name ="books[price]" placeholder="PRICE" required/>
     </div>
     <div>
       <input type = "submit" name ="submit" value = "ADD / UPDATE"/>
     </div>
   </div>
   </fieldset>
   </form>
 </body>
</html>
