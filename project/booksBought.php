<?php
include ('connection.php');
include ('session.php');
 ?>
 <html>
 <head>
   <title>BUYER DETAILS</title>
     <link rel = "stylesheet" type = "text/css" href = "stylesheet.css">
 </head>
 <body>
   <div class = 'navigation'>
     <a href = "logout.php">LOGOUT</a>
     <a href = "booksBought.php">PURCHASED</a>
     <a href = "viewBooks.php">VIEW BOOKS</a>
     <a href = "books.php">ADD/UPDATE BOOKS</a>
     <a href = "select.php">STUDENT DETAILS</a>
   </div>
   <div>
     <h1> BOOKS PURCHASED </h1>
   </div>
   <div>
    <table>
      <tr>
        <th>EMAIL</th>
        <th>BOOK_ID</th>
        <th>BOOK NAME</th>
        <th>AUTHOR</th>
        <th>DATE OF PURCHASE</th>
      </tr>
      <?php

      $sql =  "select email, buyer_details.book_id, book_name,
              author, date_of_purchase from books right join buyer_details
              on books.book_id = buyer_details.book_id";
      $result = mysqli_query($connection, $sql);
      $number_of_record = mysqli_num_rows($result);
      $record_per_page = 5;
      $number_of_page = ceil($number_of_record/$record_per_page);

      if (!isset($_GET['page'])) {
          $page = 1;
      } else {
          $page = $_GET['page'];
      }
      
      $starting_record = ($page - 1) * $record_per_page;

      $sql =  "select email, buyer_details.book_id, book_name,
              author, date_of_purchase from books right join buyer_details
              on books.book_id = buyer_details.book_id limit $starting_record, $record_per_page";

      $result = mysqli_query($connection, $sql);
      while ($details = mysqli_fetch_assoc($result)) {
         echo "<tr>",
              "<td>",$details['email'],"</td>",
              "<td>",$details['book_id'],"</td>",
              "<td>",$details['book_name'],"</td>",
              "<td>",$details['author'],"</td>",
              "<td>",$details['date_of_purchase'],"</td></tr>";
      }
      ?>
    </table>
   </div>

   <div class = "pagination">
     <?php
     for ($page = 1; $page <= $number_of_page; $page++) {
          echo "<a href = 'booksBought.php?page=$page' >$page<a>";
     }
     ?>
   </div>
 </body>
</html>
