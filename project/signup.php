<?php
include('validate.php');
$result = $_POST['data'];
session_start();
$_SESSION['username'] = $result['firstName'];

if (isset($result['submit'])) {

    $validation = new validate($result);
    $error = $validation->validateForm();
    if (empty($error)) {
      $error['email'] = $validation->addStudent();
    }
}
?>
<html>
     <head>
       <title> Form validation</title>

      <link rel = "stylesheet" type = "text/css" href = "stylesheet.css">
     </head>
     <body>
       <div class = 'navigation'>
         <a href = "signup.php">SIGNUP</a>
         <a href = "login.php">LOGIN</a>
         <a href = "about.php">ABOUT</a>
       </div>
       <h1>Student Registration</h1>
       <form method = "POST" action = "signup.php">
         <fieldset>
          <div>
          <div>
          <label for = "firstName">FIRST NAME</label>
          <input type = "text" name = "data[firstName]" value = <?php echo $result['firstName']?>><br>
          <span> <?php echo $error['firstName']; ?></span>
          </div>

          <div>
          <label for = "lastName">LAST NAME</label>
          <input type = "text" name = "data[lastName]" value = <?php echo $result['lastName']?>><br>
          <span> <?php echo $error['lastName']; ?></span>
          </div>

          <div>
          <label for = "phone">PHONE NUMBER</label>
          <input type = "phone" name = "data[phone]" value = <?php echo $result['phone']?>><br>
          <span> <?php echo $error['phone']; ?></span>
          </div>

          <div>
          <label for = "email">EMAIL</label>
          <input type = "email" name = "data[email]" value = <?php echo $result['email']?>><br>
          <span> <?php echo $error['email']; ?></span>
          </div>

          <div>
          <label for = "password">PASSWORD</label>
          <input type = "password" name = "data[password]">
          <span> <?php echo $error['password']; ?></span>
         </div>

          <div>
          <label for = "password">CONFIRM PASSWORD</label>
          <input type = "Password" name = "data[confirmPassword]"><br>
          <span> <?php echo $error['confirmPassword']; ?></span><br>
          </div>

          <div>
          <input type = "submit" value = "SIGN UP" name = "data[submit]">
          </div>
        </div>
        </fieldset>
        </form>
      </body>
</html>
