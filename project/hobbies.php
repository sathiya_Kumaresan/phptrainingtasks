<?php
include ('session.php');
include ('connection.php');

$email = $_SESSION['email'];
$result = $_POST['data'];
if (isset($result['submit'])) {
    $sports = $result['sports'];
    $club = $result['club'];
    $events = $result['events'];

    if ($sports == "--selectsports--") {
      $error['sports'] = "Sports cannot be empty";
    }

    if ($club == "--selectclub--") {
      $error['club'] = "Club cannot be empty";
    }

    if ($events == "--selectevents--") {
      $error['events'] = "Events cannot be empty";
    }
    updateHobbies($email, $sports, $club, $events);
}

function updateHobbies($email, $sports, $club, $events){
    include('connection.php');
    $sql = "select * from student_details where email = '$email'";
    $result = mysqli_query($connection, $sql);
    $data = mysqli_fetch_assoc($result);
    $id = $data['id'];
    $sql = "insert into hobbies values($id, '$sports', '$club', '$events')";
    if (mysqli_query($connection, $sql)){
        echo "<script>alert('Inserted Successfully!!');</script>";
    } else{
        echo "<script>alert('Cannot update data contact admin!!');</script>";
    }
}

?>

<html>
     <head>
       <title>Hobbies</title>

      <link rel = "stylesheet" type = "text/css" href = "stylesheet.css">
     </head>
     <body>
       <h1>Hobbies</h1>
        <a href = "viewProfile.php"><input type = "submit" name = "logout" value = "VIEWPROFILE"/ > </a>
       <a href = "about.php"><input type = "submit" name = "logout" value = "LOGOUT"/ > </a>
       <form method = "POST" action = "hobbies.php">
         <fieldset>

           <label for = "sports">SPORTS</label>
            <select name = "data[sports]">
              <option value = "--selectsports--">--selectsports--</option>
              <option value = "Cricket" <?php if ($data[sports] == "Cricket") {echo "selected";
            } ?>>Cricket</option>
              <option value = "Shuttle" <?php if ($data[sports] == "Shuttle") {echo "selected";
            } ?>>Shuttle</option>
              <option value = "Volleyball" <?php if ($data[sports] == "Volleyball") {echo "selected";
            } ?>>Volleyball</option>
              <option value = "Basketball" <?php if ($data[sports] == "Basketball") {echo "selected";
            } ?>>Basketball</option>
          </select><br>
          <span> <?php echo $error['sports']; ?></span><br>

          <label for = "club">CLUB</label>
           <select name = "data[club]">
             <option value = "--selectclub--">--selectclub--</option>
             <option value = "Soil" <?php if ($data[club] == "Soil") {echo "selected";
           } ?>>Soil</option>
             <option value = "Varnam" <?php if ($data[club] == "Varnam") {echo "selected";
           } ?>>Varnam</option>
             <option value = "Sahitya" <?php if ($data[club] == "Sahitya") {echo "selected";
           } ?>>Sahitya</option>
             <option value = "YRC" <?php if ($data[club] == "YRC") {echo "selected";
           } ?>>YRC</option>
         </select><br>
         <span> <?php echo $error['club']; ?></span><br>

         <label for = "events">EVENTS</label>
          <select name = "data[events]">
            <option value = "--selectevents--">--selectevents--</option>
            <option value = "Code2Duo" <?php if ($data[events] == "Code2Duo") {echo "selected";
          } ?>>Code2Duo</option>
            <option value = "Codemania" <?php if ($data[events] == "Codemania") {echo "selected";
          } ?>>Codemania</option>
            <option value = "Codethan" <?php if ($data[events] == "Codethan") {echo "selected";
          } ?>>codethan</option>
            <option value = "Enigma" <?php if ($data[events] == "Enigma") {echo "selected";
          } ?>>Enigma</option>
        </select><br>
        <span> <?php echo $error['events']; ?></span><br>

          <input type = "submit" value = "add" name = "data[submit]">
        </fieldset>
        </form>
      </body>
</html>
