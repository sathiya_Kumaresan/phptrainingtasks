<?php

function divide($divident, $divisor) 
{
    if($divisor == 0) {
        throw new Exception('cannot divide by zero');
    } else {
        return $divident/$divisor;
    }
}


try {
    echo divide(100, 5),"<br>";
    echo divide(10, 0);
} catch (Exception $except) {
    echo 'Exception: '.$except->getMessage();
}

finally {
    echo "<br>This is finally block.will be executed irreespective of the try catch execution.";
}
