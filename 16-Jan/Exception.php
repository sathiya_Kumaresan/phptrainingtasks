<?php

class MyException extends Exception
{
    public function __construct($message, $code = 0, Exception $previous = null)
    {
         parent::__construct($message, $code, $previous);
         
    }

    public function __toString() 
    {
        return __CLASS__.$this->code." ".$this->message;
    }

    public function customFunction() 
    {
         echo "Custom Function.";
    }

}

class Test
{
    const value = 0;
    const number = 1;
    function __construct($case = self::value)
    {
        switch ($case) {
            case 0:
                throw new MyException('this is custom myexception');
                break;
            case 1:
                throw new Exception('this is Default Exception');
                break;
         }
     }
            
}

try {
    $object = new Test(Test::value);
} catch (MyException $e) {
      echo "Exception Caught :",$e;
      $e->customFunction();
} catch (Exception $e) {
      echo 'caught', $e;
}
