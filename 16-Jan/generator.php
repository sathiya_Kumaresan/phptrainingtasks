<?php

function ranger($start, $end, $step = 1)
{
    for($i = $start; $i<=$end; $i += $step)
    {
        yield $i;
    }
}


foreach(ranger(2, 7) as $number) {
    echo $number."\n";
}

