<?php
$array = array(1, 2, 3);
$first = $array;
$first[0]++;
echo "Array: <br>";
var_dump($array);
echo "<br>First:<br> ";
var_dump($first);


class returnbyreferance
{
    public $value = 45;

    public function &add()
    {
        return $this->value;
    }
}

$object = new returnbyreferance;
$myvalue = &$object->add();
echo "<br> Myvalue: ",$myvalue,"<br>";
$object->value = 5;
$obj = &$object->add();
echo "valuechanged: ",$obj,"<br>";
$myvalue = 8;
echo "value changes: ",$object->add();




function &arrayvalue()
{
    static $collection = [1,2,3,4];
    return $collection;
}

$array = &arrayvalue();//must be assigned to a variable using referance operator to use it 
$array[4] = 5;

echo "<br>";
var_dump($array);


array_push(arrayvalue(),'sathiya');//in call function doesnt need any referance operator
echo "<br>";
var_dump($array);


$value = 5;
$copy = &$value;
echo "<br>value: ",$value,"  copy: ",$copy;
unset($copy);
echo "  Unset Copy: ",$copy;
$copy = 10;
echo "<br>Assigning value tocopy: ",$copy ,"<br>";


function get_content() 
{
    file_get_contents("https://google.com");
    var_dump($http_response_header);
}
get_content();
var_dump($http_response_header);

echo "<br>";
var_dump($argc);
echo "<br>";
var_dump($argv);

