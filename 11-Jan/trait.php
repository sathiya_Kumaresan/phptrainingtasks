<?php 
include 'include.php'; 
include 'interface.php';

class Student implements Activity
{
    use subject;

    public $name;
    public $age;
    public $gender;
    public $phone;
    
    public function __construct($name, $age, $gender, $phone) 
    {
        $this->name = $name;
        $this->age = $age;
        $this->gender = $gender;
        $this->phone = $phone;
    }
    
    public function display() 
    {   
        echo "<b>Student Details </b>:<br>";
        echo "Name : ".$this->name."<br>";
        echo "Age : ".$this->age."<br>";
        echo "Gender : ".$this->gender."<br>";
        echo "Phone : ".$this->phone."<br><br>";
    } 
    
    public function game() 
    {
        $game = "Cricket";
        $play = "Football";
        $team = "Shuttle";
        echo "<br><b>Game Details:</b> <br>";
        echo $game."<br>".$play."<br>".$team."<br>";
    } 
    
}

class Details extends Student 
{
    public function __construct(
        $name,
        $age,
        $gender,
        $phone,
        $bloodGroup,
        $nationality
    ) {
        parent::__construct($name, $age, $gender, $phone);
        $this->bloodGroup = $bloodGroup;
        $this->nationality = $nationality;
    }
    
    public function display()
    { 
        parent::display();
        echo "<b>Personal Details :</b> <br>";
        echo "Blood Group : ".$this->bloodGroup."<br>";
        echo "Nationality : ".$this->nationality."<br><br>";
    }  
     
    
}


$first = new Details("varshni", 21, "female", 9834567213, "O+ve", "INDIAN");
$first->display();
$first->marks(90, 89, 79);
$first->game();



