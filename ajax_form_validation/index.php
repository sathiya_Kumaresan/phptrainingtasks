<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>AJAX FORM VALIDATION</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="style.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  </head>
  <body>
    <form action = "validate.php" method = "post">
      <label>
        <p class="label-txt">ENTER YOUR EMAIL</p>
        <input type="text" class="input" id = "email">
        <div class="line-box">
          <div class="line"></div>
        </div>
      </label>
      <label>
        <p class="label-txt">ENTER YOUR NAME</p>
        <input type="text" class="input" id = "name">
        <div class="line-box">
          <div class="line"></div>
        </div>
      </label>
      <label>
        <p class="label-txt">ENTER YOUR PASSWORD</p>
        <input type="password" class="input" id = "password">
        <div class="line-box">
          <div class="line"></div>
        </div>
      </label>
      <input type = "submit" id = "submit" type="submit" value = "submit">
      <p id = "form-message"></p>
    </form>
  </body>
</html>
<script>
    $(document).ready(function(){
        $("form").submit(function(event) {
            event.preventDefault();
            let email = $("#email").val();
            let name = $("#name").val();
            let password = $("#password").val();
            let data = {'email': email, 'name': name, 'password': password};
            console.log(data);

            $("#form-message").load("validate.php", {
               'data': data,
            });
        });
    });
</script>
