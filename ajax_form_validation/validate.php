<?php

if (isset($_POST['data'])) {
      
      $data = $_POST['data'];
      $email = $data['email'];
      $name = $data['name'];
      $password = $data['password'];

      $errorEmpty = false;
      $errorEmail = false;

      if (empty($name) || empty($email) || empty($password)) {
          echo "<span class= 'form-error'>Fill in all the fields</span>";
          $errorEmpty = true;
      } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
          echo "<span class= 'form-error'>Enter a valid Email</span>";
          $errorEmail = true;
      } else {
         echo "<span class = 'form-success'>Form validation Success!!</form>";
      }
} else {
   echo "There was error submiting the form";
}

?>

<script type="text/javascript">
      $("#email").removeClass("input-error");
      $("#name").removeClass("input-error");
      $("#password").removeClass("input-error");
      var errorEmpty = "<?php if (isset($errorEmpty)) {echo $errorEmpty; }?>";
      var errorEmail = "<?php if (isset($errorEmail)) {echo $errorEmail; }?>";

      if (errorEmpty == true) {
          $("#email").addClass("input-error");
          $("#name").addClass("input-error");
          $("#password").addClass("input-error");
      }

      if (errorEmail == true) {
          $("#email").addClass("input-error");
      }

      if (errorEmpty == false && errorEmail == false) {
          $("#email").val(" ");
          $("#name").val(" ");
          $("#password").val("");
      }
</script>
