<?php

function compareTriplets($alice, $bob) 
{
   $result  = array();
   for ($value = 0, $bobCount = 0, $aliceCount = 0; $value < count($alice); $value++) {
       if ($alice[$value] != $bob[$value]) {
           ($alice[$value] > $bob[$value]) ? ($aliceCount++) :($bobCount++);
       }
   }
   array_push($result, $aliceCount,$bobCount);
   return $result; 
}

$value = implode(' ', compareTriplets(array(5, 6, 7),array(3, 6, 10)));
echo $value;
