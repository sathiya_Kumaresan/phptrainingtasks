<?php

$array = [[1, 2, 3, 4], [7, 8, 9, 10], [13, 14, 15, 16], [19, 20, 21, 22], [25, 26, 27, 28]];

$totalRow = count($array);
$totalColumn = count(current($array));
$rotation = 7;

$totalRow--;
$totalColumn--;

for ($row = 0; $row <= $totalRow; $row++)
{
    for($column = 0; $column <= $totalColumn; $column++)
    {
       $minimumRow = min($row, $totalRow - $row);
       $minimumColumn = min($column, $totalColumn - $column);

            $ring = min($minimumRow, $minimumColumn);
            $maxRow = $totalRow - $ring;
            $maxCol = $totalColumn - $ring;

            $resultRow = $row;
            $resultColumn = $column;
            for ($i = 0; $i < $rotation; $i++) {
                if ($resultColumn == $ring && $resultRow < $maxRow) {
                    $resultRow++;
                }
                else if ($resultRow == $maxRow && $resultColumn < $maxCol) {
                    $resultColumn++;
                }
                else if ($resultRow > $ring && $resultColumn == $maxCol) {
                    $resultRow--;
                }
                else if ($resultRow == $ring && $resultColumn > $ring) {
                    $resultColumn--;
                }
            }
            $result[$resultRow][$resultColumn] = $array[$row][$column];
    }
}

    for ($row = 0; $row <= $totalRow; $row++) {
        for ($column = 0; $column <= $totalColumn; $column++) {
           echo $result[$row][$column] ,"\t\t\t";
        }
        echo "<br>";
    }
