<?php

declare(ticks = 1);

function tickHandler()
{
    echo "Hi ";
}

register_tick_function('tickHandler');

$value = 1;

if ($value > 0) {
    $value += 2;
    print($value);
    echo "<br>";
}
