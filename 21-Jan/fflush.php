<?php

//fflush
$flushfile = fopen("test.txt","w+");
fwrite($flushfile, "hi");
rewind($flushfile);
fflush($flushfile);
fclose($flushfile);

//symbolic link
$target = "/var/www/html/comment.php";
$link = 'test';

symlink($target, $link);
echo readlink($link);

echo "filesize: ",filesize("test.txt");

$handle = fopen("text.txt", 'r+');
ftruncate($handle, 1);
echo "truncated filesize: ",filesize("test.txt");

fclose($handle);
