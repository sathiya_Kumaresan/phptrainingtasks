<?php
require_once 'models/signup_model.php';
class Controller
{
     public function redirect($url)
     {
       if ($url == "about_view.php") {
             require 'views/about_view.php';
       } else if ($url == "signup_view.php") {
             require 'views/signup_view.php';
       } else if ($url == "signup_model.php") {
         include_once 'models/signup_model.php';
         if (isset($_POST['data'])) {
             $data = $_POST['data'];
             $validation = new validate($_POST['data']);
             $error = $validation->validateForm();
             if (empty($error)) {
               $error['email'] = $validation->addStudent();
             }
             require 'views/signup_view.php';
         }
       } else if ($url == 'login_model.php') {
              include 'views/login_view.php';
              include 'models/login_model.php';
              $login = new LoginModel();
       } else if ($url == 'login_view.php') {
             include 'views/login_view.php';
       } else if ($url == 'stylesheet.css') {
             require_once 'stylesheet.css';
       }


     }
}

 ?>
