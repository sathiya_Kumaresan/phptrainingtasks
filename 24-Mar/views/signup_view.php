<?php
include_once 'models/signup_model.php';
?>
<html>
     <head>
       <title> Form validation</title>
       <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel = "stylesheet" type = "text/css" href = "stylesheet.css">
     </head>
     <body>
       <div class = 'navigation'>
         <a href = "signup_view.php">SIGNUP</a>
         <a href = "login_view.php">LOGIN</a>
         <a href = "about_view.php">ABOUT</a>
       </div>
       <h1>Student Registration</h1>
       <form method = "POST" action = "models/signup_model.php">
         <fieldset>
          <div>
          <div>
          <label for = "firstName">FIRST NAME</label>
          <input type = "text" name = "data[firstName]" value = <?php echo $data['firstName']?>><br>
          <span> <?php echo $error['firstName']; ?></span>
          </div>

          <div>
          <label for = "lastName">LAST NAME</label>
          <input type = "text" name = "data[lastName]" value = <?php echo $data['lastName']?>><br>
          <span> <?php echo $error['lastName']; ?></span>
          </div>

          <div>
          <label for = "phone">PHONE NUMBER</label>
          <input type = "phone" name = "data[phone]" value = <?php echo $data['phone']?>><br>
          <span> <?php echo $error['phone']; ?></span>
          </div>

          <div>
          <label for = "email">EMAIL</label>
          <input type = "email" name = "data[email]" value = <?php echo $data['email']?>><br>
          <span> <?php echo $error['email']; ?></span>
          </div>

          <div>
          <label for = "password">PASSWORD</label>
          <input type = "password" name = "data[password]">
          <span> <?php echo $error['password']; ?></span>
         </div>

          <div>
          <label for = "password">CONFIRM PASSWORD</label>
          <input type = "Password" name = "data[confirmPassword]"><br>
          <span> <?php echo $error['confirmPassword']; ?></span><br>
          </div>

          <div>
          <input type = "submit" value = "SIGN UP" name = "data[submit]">
          </div>
        </div>
        </fieldset>
        </form>
      </body>
</html>
