<?php
include_once 'models/Model.php';

class Controller
{
    public $model;

    public function __construct()
    {
         include 'views/login.php';
         $this->model = new Model();
    }

    public function invoke()
    {
        $result = $this->model->getLogin();
        if ($result == 'login') {
            return true;
        } else {
            echo $result;
        }
    }

    public function sample()
    {     echo "controller hi";
          header('location: views/sample.php');
    }

}
