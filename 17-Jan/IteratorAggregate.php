<?php

class MyData implements IteratorAggregate
{
    public $value;
    public function __construct($value)
    {
        $this->value = $value;
    }
   
    public function getIterator()
    {
        return new ArrayIterator($this->value);   
    }
        
}
$value = array(1,2,3,4);
$object = new MyData($value);

foreach($object as $key => $valued) {
    echo "Key: ",$key," Value: ",$valued,"\n";
}
