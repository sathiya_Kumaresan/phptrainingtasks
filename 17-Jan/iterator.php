<?php

class myData implements IteratorAggregate
{
    public $value = array(1, 2, 3, 4, 5);
   
    public function getIterator()
    {
        return new ArrayIterator($this->value);   
    }
        
}


$object = new myData();
foreach ($object as $key =>$value) {
    echo $key," ",$value,"\n";
}
