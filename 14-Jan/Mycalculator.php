<?php 
class Mycalculator
{
    public $value;
    public $number;
    public function __construct($value, $number)
    {
        $this->value = $value;
        $this->number = $number;
    }
    public function add() 
    {
        return $this->value + $this->number;
    }
    public function subtract() 
    {
        return $this->value - $this->number;
    }
    public function multiply() 
    {
        return $this->value * $this->number;
    }
    public function divide() 
    {
        return intdiv($this->value, $this->number);
    }
    
}

$object = new Mycalculator(12, 6);
echo "Addition : ".$object->add(),"<br>";
echo "Subtraction : ".$object->subtract(),"<br>";
echo "Multiplication : ".$object->multiply(),"<br>";
echo "Division : ".$object->divide(),"<br>";


