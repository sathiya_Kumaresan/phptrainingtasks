<?php
function generator($number, $value)
{
    echo "Addition: ";
    yield $number + $value;
    echo "<br>";
    echo "Subtraction: ";
    yield $number - $value;
    echo "<br>";
    echo "Multiplication: ";
    yield $number * $value;
    echo "<br>";
    echo "Division: ";
    yield $number / $value;
}

$value = generator(12, 3);
foreach($value as $string) {
    echo $string;
}
